﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;

namespace RRT_OneDimension
{
    class Program
    {
        double fail_start;
        double fail_rate;
       
        double R = 0.75;
        int randomseed;
        ArrayList al = new ArrayList();
        public Program(int seed,double failure_rate) 
        {
            randomseed = seed;
            this.fail_rate = failure_rate;
        }
        public int run()
        {
            ///////////////
            //StreamWriter sw = File.CreateText("results/第"+((randomseed/15)-2 )+"次.txt");
            ////////////////////
            Random random = new Random(randomseed);
            int count = 0;
            int EMcount = 0;
            fail_start = random.NextDouble()*(1-fail_rate);
           // Console.Out.WriteLine("fail:(" + fail_start + "," + (fail_start + fail_rate) + ")");
           // sw.WriteLine("fail:(" + fail_start + "," + (fail_start + fail_rate) + ")");
            //MyTimer timer = new MyTimer();
            //timer.Start();
            double p = random.NextDouble();
            while (count<1500) 
            {
                if (test(p) == false)
                {
                    EMcount++;
                }
                //sw.WriteLine(p);
              //  Console.Out.WriteLine("p"+count+":"+p);
                count++;
                al.Add(p);
                /////////////////
                //下面产生下一个测试用例
                bool flag = true;
                while (flag)
                {
                    flag = false;
                    p = random.NextDouble();
                    for (int i = 0; i < al.Count; i++)
                    {
                        if ((p > ((double)al[i] - R / (2 * al.Count))) && (p < ((double)al[i] + R / (2 * al.Count))))
                        {
                            flag = true;
                        }
                    }
                }
               
            }
            //sw.WriteLine(p);
            //sw.Close();
           // timer.Stop();
           // Console.Out.WriteLine("time   " + timer.Duration );
            return EMcount;
        }
        public bool test(double p)
        {
            bool flag;
            if (p > fail_start && p < (fail_start + fail_rate))
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
            return flag;
        }
        static void Main(string[] args)
        {
            double[] fail_rate_arrays = { 0.0005 };
            for (int theta = 0; theta < fail_rate_arrays.Length; theta++)
            {

                int cishu = 1000;
                long sumOfF = 0;
                MyTimer timer = new MyTimer();
                timer.Start();
                for (int i = 0; i < cishu; i++)
                {
                    Program program = new Program((i + 3) * 15,fail_rate_arrays[theta]);
                    int f_measure = program.run();
                    sumOfF += f_measure;
                }
                timer.Stop();
                //输出
                //StreamWriter sw = File.CreateText("results/FM_" + (fail_rate_arrays[theta]) + ".txt");
                //sw.WriteLine((double)((double)sumOfF / (double)cishu)+"");
                //sw.WriteLine((timer.Duration / (double)cishu) + "");
                //sw.Close();
              //  Console.Out.WriteLine("F-measure平均值：" + (double)((double)sumOfF / (double)cishu) + " time:" + timer.Duration / cishu + "s");
                Console.Out.WriteLine("E-measure平均值：" + (double)((double)sumOfF / (double)cishu) + " time:" + timer.Duration / cishu + "s");

            }
                Console.ReadLine();
        }
    }
}
