// RNG.h

#pragma once

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */
/* Tempering parameters */   
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

using namespace System;

namespace RNG {

	public ref class mtrand
	{
		
		// TODO: 在此处添加此类的方法。
		public:
        static unsigned long lgenrand();  //integer
        static double i0i1genrand(); //real number to include 0 and include 1 
        static double i0e1genrand(); //real number to include 0 but exclude 1 
        static double e0e1genrand(); //real number to exclude 0 and exclude 1 
        static void sgenrand(unsigned long seed); //change seed
        static void lsgenrand(unsigned long seed_array[]); //change a series of seeds

    private:
        static unsigned long genrand(); 
	};
}
