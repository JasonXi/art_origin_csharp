﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;


namespace ARTLib_x3
{
    class MyTimer
    {
        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(out long IpPerformanceCount);
        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(out long IpFrequency);
        private long startTime, stopTime;
        private long freq;
        public MyTimer()
        {
            startTime = 0;
            stopTime = 0;
            if (QueryPerformanceFrequency(out freq) == false)
            {
                throw new Exception("not supported");
            }
        }
        public void Start()
        {
            Thread.Sleep(0);
            QueryPerformanceCounter(out startTime);
        }
        public void Stop()
        {
            QueryPerformanceCounter(out stopTime);
        }
        public double Duration
        {
            get
            {
                return (double)(stopTime - startTime) / (double)freq;
            }
        }
    }
}
