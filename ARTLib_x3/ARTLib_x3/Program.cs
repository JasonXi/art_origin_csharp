﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RNG;
using System.Collections;

namespace ARTLib_x3
{
    class Program
    {

        public Program(int seed)
        {
            mtrand.sgenrand((uint)seed);
        }
        /// <summary>
        /// 计算Fm 
        /// </summary>
        public uint doARTX2_NDs(uint dim, double[] frs, SingleFault[] faults, uint noOftcs, ref double maxDiscrep)
        {
            bool fail = false;
            SingleTClist[] ordered_tcOfDs = new SingleTClist[dim];
            ArrayList[] orig_tcOfDs = new ArrayList[dim];
            Fx[] FxOfDs = new Fx[dim];
            double[] init_posOfDs = new double[dim];
            TCNode[] curtcs = new TCNode[dim];
            double[] cur_vals = new double[dim];
            TCNode curtc = null;
            double cur_val = -1;

            uint fcount = 0;

            if (faults == null)
            {
                for (int i = 0; i < dim; i++)
                {
                    init_posOfDs[i] = (1.0 - frs[i]) * mtrand.e0e1genrand();
                    //Console.Out.WriteLine("("+init_posOfDs[i]+","+(init_posOfDs[i]+frs[i])+")");
                }
            }
            else
            {

            }
            for (int i = 0; i < dim; i++)
            {
                if (ordered_tcOfDs[i] == null)
                {
                    ordered_tcOfDs[i] = new SingleTClist((uint)i);
                    orig_tcOfDs[i] = new ArrayList();
                }
                else
                {
                    ordered_tcOfDs[i].tclist.Clear();
                    orig_tcOfDs[i].Clear();
                }
                if (FxOfDs[i] == null)
                {
                    FxOfDs[i] = new Fx("a(x-x1)(x-x2)");
                }
            }

            for (int i = 0; i < dim; i++)
            {
                ordered_tcOfDs[i].tclist.Add(new TCNode(1, 1, 1));
            }
            int flag = 0;
            int inserPos = -1;
            while (!fail)
            {
                for (int i = 0; i < dim; i++)
                {
                    curtc = genNextX2(ordered_tcOfDs[i], fcount, FxOfDs[i], ref inserPos);//目前是针对一维的
                    cur_val = (double)curtc.t_value;

                    if (Double.IsNaN(cur_val) || cur_val >= 1 || cur_val <= 0)
                    {
                        i--;//原地不动
                        continue;
                    }//防止误差引起的问题
                    curtcs[i] = curtc;
                    cur_vals[i] = cur_val;
                    orig_tcOfDs[i].Add(cur_val);

                }
                fcount++;
                if (faults == null)
                {
                    flag = 0;
                    for (int i = 0; i < dim; i++)
                    {
                        if (cur_vals[i] >= double.Parse(init_posOfDs[i].ToString()) && cur_vals[i] <= (double.Parse(init_posOfDs[i].ToString()) + frs[i]))
                        {
                            flag++;
                        }
                    }

                    if (flag == dim && noOftcs == 0)
                    {
                        fail = true;
                        continue;
                    }
                    flag = 0;
                }
                else
                {
                    //fail = executeProg(dim, cur_vals, faults);
                    //if (fail && noOftcs == 0)
                    //{
                    //    continue;
                    //}
                }


                for (int i = 0; i < dim; i++)
                {
                    insertTCLX2_V2(ordered_tcOfDs[i], curtcs[i], inserPos);
                    computeFxX2_V3(ordered_tcOfDs[i], FxOfDs[i], inserPos);
                }
                if (noOftcs > 0 && fcount == noOftcs)
                {
                    break;
                }
            }
            if (noOftcs > 0)
            {
                //computeDiscrepancy(ordered_tcOfDs, dim, noOftcs, 0.001,ref maxDiscrep);
                computeDispersion(ordered_tcOfDs, dim, noOftcs, ref maxDiscrep);
                // computeDiscrepancy_V1(ordered_tcOfDs, dim, noOftcs, ref maxDiscrep);
            }
            // excel.Quit();
            return fcount;

        }
        public uint doARTX2_OneDimension(double frs, SingleFault[] faults, uint noOftcs)
        {
            bool fail = false;
            SingleTClist ordered_tcOfDs = null;
            ArrayList orig_tcOfDs = new ArrayList();
            Fx FxOfDs = null;
            double init_posOfDs = -1;
            TCNode curtcs;
            double cur_vals;
            TCNode curtc = null;
            double cur_val = -1;

            uint fcount = 0;

            if (faults == null)
            {

                init_posOfDs = (1.0 - frs) * mtrand.e0e1genrand();
               // Console.Out.WriteLine("(" + init_posOfDs + "," + (init_posOfDs + frs) + ")");
            }
            else
            {

            }
            if (ordered_tcOfDs == null)
            {
                ordered_tcOfDs = new SingleTClist((uint)1);
                orig_tcOfDs = new ArrayList();
            }
            else
            {
                ordered_tcOfDs.tclist.Clear();
                orig_tcOfDs.Clear();
            }
            if (FxOfDs == null)
            {
                FxOfDs = new Fx("a(x-x1)(x-x2)");
            }
            ordered_tcOfDs.tclist.Add(new TCNode(1, 1, 1));
            int flag = 0;
            int inserPos = -1;
            while (!fail)
            {

                curtc = genNextX2(ordered_tcOfDs, fcount, FxOfDs, ref inserPos);//目前是针对一维的
                cur_val = (double)curtc.t_value;
                curtcs = curtc;
                cur_vals = cur_val;
                orig_tcOfDs.Add(cur_val);
                fcount++;
                if (faults == null)
                {
                    flag = 0;

                    if (cur_vals >= double.Parse(init_posOfDs.ToString()) && cur_vals <= (double.Parse(init_posOfDs.ToString()) + frs))
                    {
                        flag++;
                    }
                    if (flag == 1)
                    {
                        fail = true;
                        continue;
                    }
                    flag = 0;
                }
                else
                {
                    
                }
                insertTCLX2_V2(ordered_tcOfDs, curtcs, inserPos);
                computeFxX2_V3(ordered_tcOfDs, FxOfDs, inserPos);
               
            }
            return fcount;
        }

        /// <summary>
        ///   产生下一个
        /// </summary>
        public TCNode genNextX2(SingleTClist tcs, uint fcount, Fx insofFx, ref int inspos)
        {
            //Console.Out.WriteLine("start genNextX2!!!");
            double tu = mtrand.e0e1genrand();//产生不包括0,1的随机数
            double tval = tu;
            double tval2 = tu;

            TCNode tc = null;
            bool success = false;
            if (insofFx == null)
            {
                //  insofFx = instanceOfFx;
            }
            if (fcount == 0)
            {
                tc = new TCNode(tval);
                inspos = 0;
                success = true;

            }
            else//按fn分布产生下一个test case
            {
                //int noOfTc = tcs.tclist.Count;
                double tc_val;
                TCNode tc_temp;
                fx2 fx_tmp;
                double coefficient = 0;
                double lr = 0;
                double rr = 1;
                double bcof;
                double[] roots;
                double[] roots2;
                int i;
                for (i = 0; i < fcount + 1; i++)
                {
                    tc_temp = (TCNode)tcs.tclist[i];
                    tc_val = double.Parse(tc_temp.t_value.ToString());
                    //  if (i == 0)
                    //Console.Out.WriteLine("!!!" + tc_temp.f_high + " " + tc_temp.f_low + " " + tu);
                    if (tu > tc_temp.f_high)
                    {
                        fx_tmp = (fx2)insofFx.fxs[i];
                        if (i == 0)
                        {
                            coefficient += (fx_tmp.a) * Math.Pow(fx_tmp.x2, 2.0) * (fx_tmp.x1 / 2.0 - fx_tmp.x2 / 6.0);

                        }
                        else
                        {
                            coefficient += ((-fx_tmp.a) * Math.Pow(fx_tmp.x2 - fx_tmp.x1, 3.0) / 6);
                        }
                        continue;
                    }
                    else if (tu >= tc_temp.f_low)
                    {

                        inspos = i;
                        fx_tmp = (fx2)insofFx.fxs[i];
                        bcof = (fx_tmp.a) * (fx_tmp.x1 - fx_tmp.x2) / 2;
                        if (i == 0)
                        {
                            // lr = 0 - fx_tmp.x2;
                            lr = -1;
                            rr = 0;
                            bcof = -bcof;
                            coefficient += (fx_tmp.a) * Math.Pow(fx_tmp.x2, 2.0) * (fx_tmp.x1 / 2.0 - fx_tmp.x2 / 6.0);
                        }
                        else
                        {
                            lr = 0;
                            rr = 1;
                        }
                        double x1_tmp = fx_tmp.x1;
                        string errmsg = ">" + x1_tmp.ToString() + "<" + fx_tmp.x2.ToString();
                        if (i == 0)
                        {
                            x1_tmp = fx_tmp.x2;
                            errmsg = "<" + x1_tmp.ToString();
                        }
                        double a_tmp = (fx_tmp.a) / 3;
                        roots = shengjinFormula(a_tmp, bcof, 0, coefficient - tu);

                        for (int rn = 0; rn < roots.Length; rn++)
                        {
                            if (i == 0)
                            {
                                tval = roots[rn] + fx_tmp.x2;
                                if (tval >= 0 && tval <= fx_tmp.x2)
                                {
                                    success = true;
                                    break;
                                }
                            }
                            else
                            {
                                tval = roots[rn] + fx_tmp.x1;
                                if (tval >= fx_tmp.x1 && tval <= fx_tmp.x2 && tval <= 1)//去掉不合理的根
                                {
                                    success = true;
                                    break;
                                }
                            }
                        }
                        if (success)
                        {
                            tc = new TCNode(tval);
                            tc.fx_val = (fx_tmp.a) * (tval - fx_tmp.x1) * (tval - fx_tmp.x2);
                        }
                        break;
                    }
                }
            }
            if (!success)
            {
                tval = -1;//无效值
                tc = new TCNode(tval);
            }
            //Console.Out.WriteLine(tc.t_value);
            return tc;
        }
        /// <summary>
        ///   计算Fx2
        /// </summary>
        public bool computeFxX2_V3(SingleTClist tcl, Fx insOfFx, int insPos)
        {
            //  Console.Out.WriteLine("start computeFxX2_V3!!!");
            int noOfTcs = tcl.tclist.Count;//最后一个是1，不是tc
            bool enExcel = true;
            string excelname = "pdfsX2_V3.xls";

            // Excel.Workbooks workbooks = null;
            //  object miss = Missing.Value;
            //   Excel.Sheets sheets = null;
            //   Excel._Worksheet sheet = null;
            if (noOfTcs > 120)
                enExcel = false;
            if (enExcel)
            {

                //workbooks = excel.Workbooks;
                //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
                //sheets = excel.Worksheets;
                //sheet = (Excel._Worksheet)sheets.get_Item(1);
                //excel.DisplayAlerts = false;
                //sheet.Cells[1, noOfTcs - 1] = noOfTcs;

            }

            if (insOfFx.fxs == null)
            {
                insOfFx.fxs = new ArrayList();
                // insOfFx.fxs.Add(new fx2(0, 1));
            }
            else
            {
                // insOfFx.fxs.Clear();

            }
            fx2 fx2tmp;
            TCNode tccur;
            double sumOfdenomi = 0;

            Random randOfroot = new Random();
            tccur = (TCNode)tcl.tclist[0];  //第一个测试用例
            double x1 = double.Parse(tccur.t_value.ToString());
            double r0 = -x1 * randOfroot.NextDouble();
            sumOfdenomi = Math.Pow(x1, 2.0) * (-x1 / 6.0 + r0 / 2.0);//0到x1 的积分

            tccur = (TCNode)tcl.tclist[noOfTcs - 2];  //最后一个测试用例
            double xn = double.Parse(tccur.t_value.ToString());
            double rn = (1 - xn) * randOfroot.NextDouble() + 1;
            sumOfdenomi += Math.Pow(1 - xn, 2.0) * ((1 - xn) / 3.0 + (xn - rn) / 2.0);

            double x2 = -1;

            if (noOfTcs == 2)
            {

                /*     for( int i = 1; i < noOfTcs - 1; i++ )
                     {
                         tccur = ( TCNode ) tcl.tclist[ i ];
                         x2 = double.Parse( tccur.t_value.ToString() );
                         sumOfdenomi += -Math.Pow( x2 - x1, 3 ) / 6.0;
                         x1 = x2;


                     }     */
            }
            else
            {
                double comput_denomi = insOfFx.sumsOfintervals;
                fx2 fx_tmp = null;
                if (insPos == 0)
                {
                    fx_tmp = (fx2)insOfFx.fxs[0];
                    comput_denomi -= Math.Pow(fx_tmp.x2, 2.0) * (-fx_tmp.x2 / 6.0 + fx_tmp.x1 / 2.0);
                    comput_denomi += Math.Pow(x1, 2.0) * (-x1 / 6.0 + r0 / 2.0);

                    tccur = (TCNode)tcl.tclist[1];
                    x2 = double.Parse(tccur.t_value.ToString());
                    comput_denomi += -Math.Pow(x2 - x1, 3) / 6.0;

                    fx_tmp = (fx2)insOfFx.fxs[noOfTcs - 2];
                    comput_denomi -= Math.Pow(1 - fx_tmp.x1, 2.0) * ((1 - fx_tmp.x1) / 3.0 + (fx_tmp.x1 - fx_tmp.x2) / 2.0);
                    comput_denomi += Math.Pow(1 - xn, 2.0) * ((1 - xn) / 3.0 + (xn - rn) / 2.0);


                }
                else if (insPos > 0 && insPos < noOfTcs - 2)
                {
                    fx_tmp = (fx2)insOfFx.fxs[0];
                    comput_denomi -= Math.Pow(fx_tmp.x2, 2.0) * (-fx_tmp.x2 / 6.0 + fx_tmp.x1 / 2.0);
                    comput_denomi += Math.Pow(x1, 2.0) * (-x1 / 6.0 + r0 / 2.0);

                    fx_tmp = (fx2)insOfFx.fxs[insPos];
                    comput_denomi -= -Math.Pow(fx_tmp.x2 - fx_tmp.x1, 3) / 6.0;
                    tccur = (TCNode)tcl.tclist[insPos];
                    x2 = double.Parse(tccur.t_value.ToString());
                    comput_denomi += -Math.Pow(x2 - fx_tmp.x1, 3) / 6.0 - Math.Pow(fx_tmp.x2 - x2, 3) / 6.0;

                    fx_tmp = (fx2)insOfFx.fxs[noOfTcs - 2];
                    comput_denomi -= Math.Pow(1 - fx_tmp.x1, 2.0) * ((1 - fx_tmp.x1) / 3.0 + (fx_tmp.x1 - fx_tmp.x2) / 2.0);
                    comput_denomi += Math.Pow(1 - xn, 2.0) * ((1 - xn) / 3.0 + (xn - rn) / 2.0);




                }
                else if (insPos == noOfTcs - 2)
                {
                    fx_tmp = (fx2)insOfFx.fxs[0];
                    comput_denomi -= Math.Pow(fx_tmp.x2, 2.0) * (-fx_tmp.x2 / 6.0 + fx_tmp.x1 / 2.0);
                    comput_denomi += Math.Pow(x1, 2.0) * (-x1 / 6.0 + r0 / 2.0);

                    fx_tmp = (fx2)insOfFx.fxs[noOfTcs - 2];
                    comput_denomi -= Math.Pow(1 - fx_tmp.x1, 2.0) * ((1 - fx_tmp.x1) / 3.0 + (fx_tmp.x1 - fx_tmp.x2) / 2.0);

                    tccur = (TCNode)tcl.tclist[insPos];
                    x2 = double.Parse(tccur.t_value.ToString());
                    comput_denomi += -Math.Pow(x2 - fx_tmp.x1, 3) / 6.0;
                    comput_denomi += Math.Pow(1 - xn, 2.0) * ((1 - xn) / 3.0 + (xn - rn) / 2.0);


                }
                sumOfdenomi = comput_denomi;

            }

            //tccur = (TCNode)tcl.tclist[noOfTcs - 2];
            //double xn = double.Parse(tccur.t_value.ToString());
            //double rn = (1 - xn) * randOfroot.NextDouble() + 1;
            //sumOfdenomi += Math.Pow(1 - xn, 2.0) * ((1 - xn) / 3.0 + (xn - rn) / 2.0);



            double acof = 1.0 / sumOfdenomi;//二次项系数
            double ss = -1;
            insOfFx.sumsOfintervals = sumOfdenomi;


            if (insOfFx.fxs == null)
            {
                insOfFx.fxs = new ArrayList();
                // insOfFx.fxs.Add(new fx2(0, 1));
            }
            else
            {
                insOfFx.fxs.Clear();

            }
            double flow = 0;
            //double fhig = 0;
            x1 = 0;
            for (int i = 0; i < noOfTcs; i++)
            {
                tccur = (TCNode)tcl.tclist[i];

                x2 = double.Parse(tccur.t_value.ToString());
                if (i == 0)
                {
                    fx2tmp = new fx2(r0, x2);
                    fx2tmp.a = acof;
                    fx2tmp.s = acof * Math.Pow(x2, 2.0) * (-x2 / 6.0 + r0 / 2.0);
                }
                else if (i == noOfTcs - 1)
                {
                    fx2tmp = new fx2(x1, rn);
                    fx2tmp.a = acof;
                    fx2tmp.s = acof * Math.Pow(1 - x1, 2.0) * ((1 - x1) / 3.0 + (x1 - rn) / 2.0);
                }
                else
                {
                    fx2tmp = new fx2(x1, x2);
                    fx2tmp.a = acof;
                    //下面这句话原本放在括号外面，应该是个错误
                    fx2tmp.s = -acof / 6.0 * Math.Pow(x2 - x1, 3);
                }


                //这句话原本在这个位置
                //fx2tmp.s = -acof / 6.0 * Math.Pow(x2 - x1, 3);
                insOfFx.fxs.Add(fx2tmp);

                tccur.f_low = flow;
                tccur.f_high = flow + fx2tmp.s;
                flow = tccur.f_high;
                x1 = x2;


            }
            tccur = (TCNode)tcl.tclist[noOfTcs - 1];
            tccur.f_high = 1;

            if (enExcel)
            {
                //double lrv = 0, rrv = 1;
                //for (int i = 0; i < insOfFx.fxs.Count; i++)
                //{
                //    fx2tmp = (fx2)insOfFx.fxs[i];

                //    lrv = fx2tmp.x1;
                //    rrv = fx2tmp.x2;

                //    sheet.Cells[i + 2, noOfTcs - 1] = "(" + lrv.ToString() + "," + rrv.ToString() + ");" + fx2tmp.a.ToString() + "*(x-(" + fx2tmp.x1.ToString() + "))*(x-(" + fx2tmp.x2.ToString() + "))";


                //}
                //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

                //excel.Workbooks.Close();
                //excel.Quit();

            }


            return true;


        }
        public double[] shengjinFormula(double acof, double bcof, double cof, double dof)
        {
            double A = bcof * bcof - 3 * acof * cof;//A=b^2-3ac
            double B = bcof * cof - 9 * acof * dof;// B=bc-9ad
            double C = cof * cof - 3 * bcof * dof;//C=c^2-3bd
            double delta = B * B - 4 * A * C;
            double root = 0;
            double r1 = 0;
            double r2 = 0;
            double[] roots = new double[3];
            if (delta > 0)
            {
                double Y1 = A * bcof + 3.0 * acof * (-B + Math.Sqrt(B * B - 4 * A * C)) / 2.0;
                double Y2 = A * bcof + 3.0 * acof * (-B - Math.Sqrt(B * B - 4 * A * C)) / 2.0;
                double powY1;
                double powY2;
                if (Y1 < 0)
                {
                    powY1 = -Math.Pow(-Y1, 1.0 / 3.0);
                }
                else
                {
                    powY1 = Math.Pow(Y1, 1.0 / 3.0);
                }
                if (Y2 < 0)
                {
                    powY2 = -Math.Pow(-Y2, 1.0 / 3.0);
                }
                else
                {
                    powY2 = Math.Pow(Y2, 1.0 / 3.0);
                }
                root = (-bcof - powY1 - powY2) / (3.0 * acof);
                r1 = root;
                r2 = root;
            }
            else if (delta == 0)
            {
                root = -bcof / acof + B / A;
                r1 = -B / (2 * A);
                r2 = r1;

            }
            else if (delta < 0)
            {
                double T = (2 * A * bcof - 3 * acof * B) / (2 * Math.Pow(A, 3.0 / 2.0));
                double theta = Math.Acos(T);
                root = (-bcof - 2 * Math.Sqrt(A) * Math.Cos(theta / 3)) / (3.0 * acof);
                r1 = (-bcof + Math.Sqrt(A) * (Math.Cos(theta / 3) + Math.Sqrt(3) * Math.Sin(theta / 3))) / (3 * acof);
                r2 = (-bcof + Math.Sqrt(A) * (Math.Cos(theta / 3) - Math.Sqrt(3) * Math.Sin(theta / 3))) / (3 * acof);
            }
            roots[0] = root;
            roots[1] = r1;
            roots[2] = r2;
            return roots;
        }
        public void insertTCLX2_V2(SingleTClist tcl, TCNode tc, int inspos)
        {
            tcl.tclist.Insert(inspos, tc);
        }
        /* public bool executeProg(uint dim, double[] vals, SingleFault[] faults)
         {
             SingleFault fault;
             bool fail = false;
             double fr_sval;
             int flag;
             for (int i = 0; i < faults.Length; i++)
             {
                 fault = (SingleFault)faults[i];

                 flag = 0;
                 for (int j = 0; j < dim; j++)
                 {
                     fr_sval = double.Parse((fault.init_posOfNDs[j]).ToString());
                     if (vals[j] >= fr_sval && vals[j] <= (fr_sval + (double)(fault.fr_dimensions[j])))
                     {
                         flag++;

                     }
                 }
                 if (flag == dim)
                 {
                     fail = true;

                     break;
                 }

             }
             return fail;
         } */
        public void computeDiscrepancy(SingleTClist[] ordered_tcOfDs, uint dim, uint noOftcs, double interval, ref double max_Discrep)
        {

            int noOfintervals = (int)(1.0 / interval);
            int[] freqs1 = null;
            int[,] freqs2 = null;
            int[, ,] freqs3 = null;
            TCNode tc = null;
            double maxDiscrep = -1;
            if (dim == 1)
            {
                freqs1 = new int[noOfintervals];
                for (int i = 0; i < noOfintervals; i++)
                    freqs1[i] = 0;
                int xIndex = -1;
                for (int i = 0; i < noOftcs; i++)
                {
                    tc = (TCNode)ordered_tcOfDs[0].tclist[i];
                    xIndex = (int)(double.Parse(tc.t_value.ToString()) / interval);
                    freqs1[xIndex]++;

                }
                double diff = 0;
                for (int i = 0; i < noOfintervals; i++)
                {
                    diff = Math.Abs(double.Parse(freqs1[i].ToString()) / noOftcs - interval);
                    if (diff > maxDiscrep)
                    {
                        maxDiscrep = diff;
                    }
                }
            }
            else if (dim == 2)
            {
                freqs2 = new int[noOfintervals, noOfintervals];
                for (int i = 0; i < noOfintervals; i++)
                {
                    for (int j = 0; j < noOfintervals; j++)
                    {
                        freqs2[i, j] = 0;
                    }
                }
                int xIndex = -1;
                int yIndex = -1;
                for (int i = 0; i < noOftcs; i++)
                {
                    tc = (TCNode)ordered_tcOfDs[0].tclist[i];
                    xIndex = (int)(double.Parse(tc.t_value.ToString()) / interval);
                    tc = (TCNode)ordered_tcOfDs[1].tclist[i];
                    yIndex = (int)(double.Parse(tc.t_value.ToString()) / interval);
                    freqs2[xIndex, yIndex] = freqs2[xIndex, yIndex] + 1;

                }
                double diff = 0;
                for (int i = 0; i < noOfintervals; i++)
                {
                    for (int j = 0; j < noOfintervals; j++)
                    {
                        diff = Math.Abs(double.Parse(freqs2[i, j].ToString()) / noOftcs - interval * interval);
                        if (diff > maxDiscrep)
                        {
                            maxDiscrep = diff;
                        }

                    }
                }
            }
            else if (dim == 3)
            {
                freqs3 = new int[noOfintervals, noOfintervals, noOfintervals];
                for (int i = 0; i < noOfintervals; i++)
                {
                    for (int j = 0; j < noOfintervals; j++)
                    {
                        for (int k = 0; k < noOfintervals; k++)
                        {
                            freqs3[i, j, k] = 0;
                        }
                    }
                }
                int xIndex = -1;
                int yIndex = -1;
                int zIndex = -1;
                for (int i = 0; i < noOftcs; i++)
                {
                    tc = (TCNode)ordered_tcOfDs[0].tclist[i];
                    xIndex = (int)(double.Parse(tc.t_value.ToString()) / interval);
                    tc = (TCNode)ordered_tcOfDs[1].tclist[i];
                    yIndex = (int)(double.Parse(tc.t_value.ToString()) / interval);
                    tc = (TCNode)ordered_tcOfDs[2].tclist[i];
                    zIndex = (int)(double.Parse(tc.t_value.ToString()) / interval);
                    freqs3[xIndex, yIndex, zIndex] = freqs3[xIndex, yIndex, zIndex] + 1;

                }
                double diff = 0;
                for (int i = 0; i < noOfintervals; i++)
                {
                    for (int j = 0; j < noOfintervals; j++)
                    {
                        for (int k = 0; k < noOfintervals; k++)
                        {
                            diff = Math.Abs(double.Parse(freqs3[i, j, k].ToString()) / noOftcs - interval * interval * interval);
                            if (diff > maxDiscrep)
                            {
                                maxDiscrep = diff;
                            }
                        }

                    }
                }


            }
            max_Discrep = maxDiscrep;
            // Console.WriteLine("discrep=" + maxDiscrep.ToString() + ";noOftcs=" + noOftcs.ToString());



        }
        public void computeDispersion(SingleTClist[] ordered_tclist, uint dim, uint noOftcs, ref double disperion)
        {
            double maxdisper = -1;

            double minDist = -1;
            TCNode[] curtcOfNds = new TCNode[dim];
            TCNode[] tmptcOfNds = new TCNode[dim];
            double[] curOfvals = new double[dim];
            double[] tmpOfvals = new double[dim];
            double tmpDist = 0;
            for (int i = 0; i < noOftcs; i++)
            {
                minDist = -1;
                for (int k = 0; k < dim; k++)
                {
                    curtcOfNds[k] = (TCNode)ordered_tclist[k].tclist[i];
                    curOfvals[k] = double.Parse((curtcOfNds[k].t_value).ToString());

                }
                for (int j = 0; j < noOftcs; j++)
                {
                    if (j == i)
                        continue;
                    else
                    {
                        tmpDist = 0;
                        for (int k = 0; k < dim; k++)
                        {
                            tmptcOfNds[k] = (TCNode)ordered_tclist[k].tclist[j];
                            tmpOfvals[k] = double.Parse((tmptcOfNds[k].t_value).ToString());
                            tmpDist += (tmpOfvals[k] - curOfvals[k]) * (tmpOfvals[k] - curOfvals[k]);

                        }
                        if (minDist == -1 || tmpDist < minDist)
                        {
                            minDist = tmpDist;
                        }

                    }
                }
                if (minDist > maxdisper)
                {
                    maxdisper = minDist;
                }




            }
            disperion = Math.Sqrt(maxdisper);
        }

        static void Main(string[] args)
        {
            Program p2 = new Program(9);
            int countsum2 = 0;
            MyTimer timer2 = new MyTimer();
            timer2.Start();
            int cishu = 3000;
            for (int k = 0; k < cishu; k++)
            {
                uint fm = p2.doARTX2_OneDimension(0.0005, null, (uint)0);
                countsum2 += (int)fm;
            }
            timer2.Stop();
            Console.Out.WriteLine(countsum2 / (double)cishu + " " + timer2.Duration / (double)cishu);
        }
    }
    public class TCNode
    {
        public object t_value;
        public double f_low;
        public double f_high;
        public double fx_val;
        public int fx_index;//位于第几个区间
        public TCNode(object val)
        {
            t_value = val;
            f_low = -1;
            f_high = -1;
        }
        public TCNode(object val, double fl, double fh)
        {
            t_value = val;
            f_low = fl;
            f_high = fh;
        }
    }
    public class SingleTClist
    {
        uint dimension;
        public ArrayList tclist;
        public SingleTClist(uint dim)
        {
            dimension = dim;
            tclist = new ArrayList();

        }

    }
    public class fx2//二次概率密度函数=a(x-x1)(x-x2)
    {
        public double a;//二次项系数
        public double x1;//第一个根
        public double x2;//第二个根
        public double s;//该fx所对应的积分值
        public fx2(double ax1, double ax2)
        {

            x1 = ax1;
            x2 = ax2;
        }
    }
    /// <summary>
    /// 用来做折线y=kx+b
    /// </summary>
    public class fx1
    {
    }
    public class fcos//余弦pdf=Acos(ax+b)
    {
        public double A;//振幅
        public double acof;
        public double bcof;
        public double lr;//定义域(lr,rr)
        public double rr;
        public double s;//该fx所对应的积分值
        public fcos(double llr, double rrr)
        {
            lr = llr;
            rr = rrr;

        }
    }
    public class Fx//概率分布函数
    {
        string fexp;
        public ArrayList fxs;
        public double sumsOfintervals = 0;
        public Fx(string expname)
        {
            fexp = expname;

        }

    }
    public class SingleFault
    {
        public uint dim;
        public double failr;
        public ArrayList init_posOfNDs;
        public ArrayList fr_dimensions;
        public double coordinate_pros = 1;//默认为正方形
        public SingleFault(uint ddim, double fr_arg)
        {
            dim = ddim;
            failr = fr_arg;
            //init_posOfNDs = new ArrayList();
            fr_dimensions = new ArrayList(Int32.Parse(dim.ToString()));//各维的fr
            for (int i = 0; i < dim; i++)
            {
                if (coordinate_pros == 1)
                {

                    fr_dimensions.Add(Math.Pow(failr, 1.0 / (double)dim));
                }
                else
                {

                }
            }
        }

    }
}
