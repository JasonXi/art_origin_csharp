﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Diagnostics;
using System.Text;

namespace ProbabilityRandomTesting
{
    public class TreeNode<T> : IComparable, IComparable<TreeNode<T>>
    {

        #region .ctor 串联构造函数

        public TreeNode()
            : this( default( T ), null, null, 0 )
        {
        }

        public TreeNode( T value )
            : this( value, null, null, 1 )
        {
        }

        public TreeNode( T value, TreeNode<T> leftNode, TreeNode<T> rightNode )
            : this( value, leftNode, rightNode, 1 )
        {
        }

        public TreeNode( T value, TreeNode<T> leftNode, TreeNode<T> rightNode, int deepness )
        {
            Value = value;
            LeftNode = leftNode;
            RightNode = rightNode;
            Deepness = deepness;
            IsLeaf = true;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        #endregion

        #region Interface Members

        int IComparable.CompareTo( Object item )
        {
            TreeNode<T> node = item as TreeNode<T>;
            if( node == null )
                throw new ArgumentException( "Argument for comparing is not a object of TreeNode Type !!" );
            else
            {
                if( this == item )
                    return 0;
                else
                {
                    Comparer comparer = new Comparer( CultureInfo.CurrentCulture );
                    return comparer.Compare( this.Value, node.Value );
                }
            }
        }

        int IComparable<TreeNode<T>>.CompareTo( TreeNode<T> item )
        {
            if( item == null )
            {
                throw new ArgumentException( "Argument for comparing is not a object of TreeNode Type !!" );
            }
            else
            {
                if( this == item )
                    return 0;
                else
                {
                    Comparer comparer = new Comparer( CultureInfo.CurrentCulture );
                    return comparer.Compare( this.Value, item.Value );
                }
            }
        }
        #endregion

        #region Properties

        public TreeNode<T> LeftNode
        {
            get;
            set;
        }

        public TreeNode<T> RightNode
        {
            get;
            set;
        }

        public TreeNode<T> FatherNode
        {
            get;
            set;
        }

        //这个属性是指数的层数，也就是深度，根的深度为1
        public int Deepness
        {
            get;
            set;
        }

        //这个属性指示这个节点是不是叶子节点
        public bool IsLeaf
        {
            get;
            set;
        }

        //这个属性返回或者设置该节点的值
        public T Value
        {
            get;
            set;
        }

        #endregion
    }

    public class BinaryTree<T>
    {
        #region .ctor

        static BinaryTree()
        {
            _comparer = new Comparer( CultureInfo.CurrentCulture );
        }

        public BinaryTree()
        {
            this.Root = null;
            this.Count = 0;
            this._deepness = 0;
        }

        public BinaryTree( TreeNode<T> root )
        {
            if( root == null )
            {
                throw new ArgumentException( "Root can not be null !!" );
            }
            this.Root = root;
            this.Count++;
            this._deepness = 1;
        }
        #endregion

        #region Public Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemValue"></param>
        public void Add( T itemValue )
        {
            if( Root == null )
            {
                TreeNode<T> root = new TreeNode<T>( itemValue );
                this.Root = root;
                this.Count++;
            }
            else
            {
                TreeNode<T> _iterator = this.Root;
                bool okFlag = true;
                ;
                int deepness = 2;
                int result = _comparer.Compare( itemValue, _iterator.Value );
                ;

                while( okFlag )
                {
                    Debug.WriteLine( "Comaper Result is :" + result.ToString() );
                    Debug.WriteLine( "---------------------------------------" );
                    if( result > 0 )
                    {
                        if( _iterator.RightNode != null )
                        {
                            _iterator = _iterator.RightNode;
                            result = _comparer.Compare( itemValue, _iterator.Value );
                            deepness++;
                        }
                        else
                        {
                            //在添加节点的时候就设置该节点的深度，而在计算整棵树的深度的时候，其实只要对所有叶节点的深度的值进行排序就可以得到了
                            _iterator.RightNode = new TreeNode<T>( itemValue, null, null, deepness );
                            _iterator.IsLeaf = false;
                            _iterator.RightNode.FatherNode = _iterator;
                            Count++;
                            okFlag = false;
                        }
                    }
                    else if( result < 0 )
                    {
                        if( _iterator.LeftNode != null )
                        {
                            _iterator = _iterator.LeftNode;
                            result = _comparer.Compare( itemValue, _iterator.Value );
                            deepness++;
                        }
                        else
                        {
                            _iterator.LeftNode = new TreeNode<T>( itemValue, null, null, deepness );
                            _iterator.IsLeaf = false;
                            _iterator.LeftNode.FatherNode = _iterator;
                            Count++;
                            okFlag = false;
                        }
                    }
                    else
                        okFlag = false;
                }
            }
        }

        /// <summary>
        /// 从树中移出一个节点,要考虑很多情况，比如要删除的节点没有子节点，有一个子节点，有两个子节点 PS:杩欎釜鏂规硶搴旇繘琛屾祴璇?
        /// </summary>
        /** 
        *删除指定的节点实现 
        *  
        * 算法思想： 
        *  
        *1、若待删除的节点p是叶子节点，则直接删除该节点； 
        *  
        * 2、若待删除的节点p只有一个子节点，则将p的子节点与p的父节点直接连接，然后删除节点p； 
         * 为什么只有一个子节点时可以直接接到删除节点的父节点下面呢?因为只有一个子节点，直接接上 
         * 去不会影响排序子节点本身的排序，当然更不会影响另外一个子树（因为另一子树跟本不存在!）；
         * 
        * 3、若待删除节点p有两个子节点时，应该使用中序遍历方式得到的直接前置节点S或直接后继节点s 
         *的值来代替点s的值，然后删除节点s，（注：节点s肯定属于上述1、2情况之一）而不是直接删除  
         *p，这样可以将该删除问题转换成上面1、2问题； 
        */
        public void Remove( T key )
        {
            TreeNode<T> node;
            bool isExit = IsExit( key, out node );
            if( !isExit )
            {
                return;
            }
            else
            {
                if( IsLeafNode( node ) )
                {
                    if( node.FatherNode.LeftNode == node )
                        node.FatherNode.LeftNode = null;
                    else
                        node.FatherNode.RightNode = null;
                    if( node != null )
                        node = null;
                }
                else
                {
                    if( !HasTwoLeafNodes( node ) )
                    {
                        TreeNode<T> child = GetUniqueChild( node );
                        if( node.FatherNode.LeftNode == node )
                        {
                            node.FatherNode.LeftNode = child;
                        }
                        else
                        {
                            if( node.LeftNode != null )
                                node.FatherNode.RightNode = child;
                        }
                        if( node != null )
                            node = null;
                    }
                    else
                    {
                        //首先找到后继结点
                        TreeNode<T> successor = GetSuccessor( node );
                        //调整节点值，这个时候有一个值得注意的地方，比如你的树是这样的情况：
                        /*           45                                     50
                         *             \                                    /
                         *             52                                  25
                         *             / \                                /  \
                         *            /   \                             15    28
                         *          49     57                          /  \   / 
                         *         /  \    / \                        7    8 26
                         *        46   50 55  58
                         *                  \
                         *                   56
                        */
                        //树A中节点52相应的后继结点应该是55，那么57的左子节点应调整成56，52就要调整成55
                        node.Value = successor.Value;
                        Remove( successor );
                    }
                }
                this.Count--;
            }
        }


        /** 
         * 删除指定的节点实现 
         *  
         * 算法思想： 
         *  
         * 1、若待删除的节点p是叶子节点，则直接删除该节点； 
         *  
         * 2、若待删除的节点p只有一个子节点，则将p的子节点与p的父节点直接连接，然后删除节点p； 
         * 为什么只有一个子节点时可以直接接到删除节点的父节点下面呢?因为只有一个子节点，直接接上 
         *  去不会影响排序子节点本身的排序，当然更不会影响另外一个子树（因为另一子树跟本不存在!）； 
         *  
         3、若待删除节点p有两个子节点时，应该使用中序遍历方式得到的直接前置节点S或直接后继节点s 
          * 的值来代替点s的值，然后删除节点s，（注：节点s肯定属于上述1、2情况之一）而不是直接删除 
         * p，这样可以将该删除问题转换成上面1、2问题； 
         */
        public void Remove( TreeNode<T> node )
        {
            if( IsLeafNode( node ) )
            {
                if( node.FatherNode.LeftNode == node )
                    node.FatherNode.LeftNode = null;
                else
                    node.FatherNode.RightNode = null;
                if( node != null )
                    node = null;
            }
            else
            {
                if( !HasTwoLeafNodes( node ) )
                {
                    TreeNode<T> child = GetUniqueChild( node );
                    if( node.FatherNode.LeftNode == node )
                    {
                        node.FatherNode.LeftNode = child;
                    }
                    else
                    {
                        node.FatherNode.RightNode = child;
                    }
                    if( node != null )
                        node = null;
                }
                else
                {
                    //要删除的节点有两个子节点
                    TreeNode<T> successor = GetSuccessor( node );
                    node.Value = successor.Value;
                    Remove( successor ); //删除相应的后继结点
                    if( successor != null )
                        successor = null;
                }
            }
            this.Count--;
        }

        /// <summary>
        ///  返回某一节点唯一的一个节点
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private TreeNode<T> GetUniqueChild( TreeNode<T> node )
        {
            TreeNode<T> child;
            if( node.LeftNode != null )
                child = node.LeftNode;
            else
                child = node.RightNode;
            return child;
        }

        /// <summary>
        /// 根据中根遍历来得到相应的后继结点，仍然还有BUG存在！
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public TreeNode<T> GetSuccessor( T value )
        {
            throw new NotImplementedException( "This function is not complete yet !" );
            IComparable icomparable = Root as IComparable;
            TreeNode<T> wanted = new TreeNode<T>( value );
            if( icomparable.CompareTo( wanted ) == 0 )
            {
                return Root;
            }
            else
            {
                TreeNode<T> node;
                if( IsExit( value, out node ) )
                {
                    if( IsLeafNode( node ) )
                    {
                        //如果是叶子节点，那么应该做么做才能返回正确的后继节点？
                        return null;
                    }
                    else
                        return GetSuccessor( node );  //如果是非叶子节点，则调用相应的方法返回非叶子节点的后继结点

                }
                else
                    return null;
            }
        }

        /// <summary>
        /// 中根遍历树
        /// </summary>
        /// <param name="root"></param>
        public void InOrder( TreeNode<T> root )
        {
            if( root != null )
            {
                InOrder( root.LeftNode );
                Console.WriteLine( string.Format( "This node's value is {0} and it's deepness is {1},leaf:{2}", root.ToString(), root.Deepness.ToString(), root.IsLeaf.ToString() ) );
                InOrder( root.RightNode );
            }
        }

        /// <summary>
        ///  先根遍历树
        /// </summary>
        /// <param name="root"></param>
        public void PreOrder( TreeNode<T> root )
        {
            if( root != null )
            {
                Console.WriteLine( string.Format( "This node's value is {0} and it's deepness is {1},leaf:{2}", root.ToString(), root.Deepness.ToString(), root.IsLeaf.ToString() ) );
                PreOrder( root.LeftNode );
                PreOrder( root.RightNode );
            }
        }

        /// <summary>
        /// 后根遍历树
        /// </summary>
        /// <param name="root"></param>
        public void PostOrder( TreeNode<T> root )
        {
            if( root != null )
            {
                PostOrder( root.LeftNode );
                PostOrder( root.RightNode );
                string.Format( "This node's value is {0} and it's deepness is {1},leaf:{2}", root.ToString(), root.Deepness.ToString(), root.IsLeaf.ToString() );
            }
        }
        /// <summary>
        ///  寻找到离给定的点最近的一个节点
        /// </summary>
        /// <returns></returns>
        double min = double.MaxValue;
        TreeNode<T> closeNode;
        public TreeNode<T> getCloseToX(TreeNode<T> root, double x )
        {
            if( root != null )
            {
                double temp= Math.Abs((double)x-double.Parse(root.Value.ToString()));
                if( temp < min )
                {
                    min = temp;
                    closeNode = root;
                }
               // Console.Out.WriteLine("|x-"+root.ToString()+"|="+(temp));
                getCloseToX( root.LeftNode,x );
                getCloseToX( root.RightNode,x );
            }
            return closeNode;
        }
        /// <summary>
        ///  返回距离最近的两个节点
        /// </summary>
        /// <returns></returns>
        ArrayList al=new ArrayList();
        /// <summary>
        /// 中根遍历树2
        /// </summary>
        /// <param name="root"></param>
        public void InOrder2( TreeNode<T> root )
        {
            if( root != null )
            {
                InOrder2( root.LeftNode );
               // Console.WriteLine( string.Format( "This node's value is {0} and it's deepness is {1},leaf:{2}", root.ToString(), root.Deepness.ToString(), root.IsLeaf.ToString() ) );
                al.Add(root);
                InOrder2( root.RightNode );
            }
        }
        public TreeNode<T>[] getCloseTwo(TreeNode<T> root, TreeNode<T> t )
        {
            al.Clear();
            InOrder2(root);
            int atemp=al.IndexOf(t);
            
           TreeNode<T>[] a=new TreeNode<T>[2];
           a[ 0 ] = new TreeNode<T>();
           a[ 1 ] = new TreeNode<T>();
           if( atemp - 1 >= 0 && atemp + 1 < this.Count )
           {
               a[ 0 ] = ( TreeNode<T> ) al[ atemp - 1 ];
               a[ 1 ] = ( TreeNode<T> ) al[ atemp + 1 ];
               Console.Out.WriteLine( "not not" + " atemp:" + atemp + " count:" + Count );
           }
           else if( atemp - 1 < 0 && atemp + 1 < this.Count )
           {
               
               a[ 0 ] = null;
               a[ 1 ] = ( TreeNode<T> ) al[ atemp + 1 ];
               Console.Out.WriteLine( "not yes" + " atemp:" + atemp + " count:" + Count );
           }
           else if( atemp + 1 >= this.Count && atemp - 1 >= 0 )
           {
               a[ 0 ] = ( TreeNode<T> ) al[atemp-1];
               a[ 1 ] = null;
               Console.Out.WriteLine( "yes not" +" atemp:"+atemp+" count:"+Count);
           }
           else if( atemp + 1 >= this.Count && atemp - 1 < 0 )
           {
               a[ 1 ] = null;
               a[ 0 ] = null;
               Console.Out.WriteLine( "yes yes" + " atemp:" + atemp + " count:" + Count );
           }
           return a;
        }
        /// <summary>
        ///  返回具有最大节点值的树节点
        /// </summary>
        /// <returns></returns>
        public TreeNode<T> GetMaxNode()
        {
            TreeNode<T> _iterator = Root;
            while( _iterator.RightNode != null )
            {
                _iterator = _iterator.RightNode;
            }
            return _iterator;
        }

        /// <summary>
        ///  返回最大的值
        /// </summary>
        /// <returns></returns>
        public T GetMaxValue()
        {
            return GetMaxNode().Value;
        }

        /// <summary>
        ///  返回具有最小节点值得节点
        /// </summary>
        /// <returns></returns>
        public TreeNode<T> GetMinNode()
        {
            TreeNode<T> _iterator = Root;
            while( _iterator.LeftNode != null )
            {
                _iterator = _iterator.LeftNode;
            }
            return _iterator;
        }

        /// <summary>
        /// 返回最小值
        /// </summary>
        /// <returns></returns>
        public T GetMinValue()
        {
            return GetMinNode().Value;
        }

        public bool IsExit( T key, out TreeNode<T> node )
        {
            node = null;
            TreeNode<T> _iterator = Root;
            int result = _comparer.Compare( key, _iterator.Value );
            bool okFlag = true;
            while( okFlag )
            {
                if( result == 0 )
                {
                    okFlag = false;
                    node = _iterator;//如果找到这个叶子结点，那么得到叶子节点的指针
                    return true;
                }
                else if( result > 0 )
                {
                    _iterator = _iterator.RightNode;
                    if( _iterator != null )
                        result = _comparer.Compare( key, _iterator.Value );
                    else
                    {
                        okFlag = false;
                        return false;
                    }
                }
                else
                {
                    _iterator = _iterator.LeftNode;
                    if( _iterator != null )
                        result = _comparer.Compare( key, _iterator.Value );
                    else
                    {
                        okFlag = false;
                        return false;
                    }
                }
            }
            return false;
        }

        public override string ToString()
        {
            string rtnString = string.Format( "This is a  kind of binary tree that impleted by Master, and it has {0} nodes.", Count.ToString() );
            return rtnString;
        }

        public TreeNode<T> Root
        {
            get;
            set;
        }

        public int Count
        {
            get;
            private set;
        }

        //返回树的深度
        public int Deepness
        {
            get
            {
                if( Count == 1 )
                    return 1;
                else
                {
                    int[] _deepnessSortArray = new int[ Count ];
                    int pointer = Count - 1;
                    for( int i = 0; i < Count; i++ )
                        _deepnessSortArray[ i ] = 0;
                    InOrder( Root, ref pointer, ref _deepnessSortArray );
                    Array.Sort<int>( _deepnessSortArray ); //对_deepnessSortArray进行排序，得出其中最大的数值就是该树的深度
                    return _deepnessSortArray[ Count - 1 ];
                }
            }
        }

        #endregion

        #region Private Members

        private static Comparer _comparer;

        private int _deepness;

        /// <summary>
        /// 遍历树节点，然后给深度数组_deepnessSortArray[i]赋值，之后对这个数组进行排序，得到的最大值就是该树的深度
        /// </summary>
        /// <param name="root"></param>
        /// <param name="pointer"></param>
        /// <param name="deepnessSortArray"></param>
        private void InOrder( TreeNode<T> root, ref int pointer, ref int[] deepnessSortArray )
        {
            if( root != null )
            {
                InOrder( root.LeftNode, ref pointer, ref deepnessSortArray );
                deepnessSortArray[ pointer ] = root.Deepness;
                pointer--;
                InOrder( root.RightNode, ref pointer, ref deepnessSortArray );
            }
        }

        /// <summary>
        ///  基于中根遍历的算法来得到某一个非叶子节点的后继结点
        /// </summary>
        /// <param name="wannaDelNode"></param>
        private TreeNode<T> GetSuccessor( TreeNode<T> wannaDelNode )
        {
            TreeNode<T> _iterator = wannaDelNode.RightNode;
            TreeNode<T> successorFather = null;
            TreeNode<T> successor = null;

            //Debug.WriteLine(string.Format("_iterator's value is {0}", _iterator.Value.ToString()));
            //Debug.WriteLine(string.Format("successor's value is {0}", successor.Value.ToString()));
            //首先应该要判断是不是叶子节点，如果是叶子节点，情况就完全不一样了
            while( _iterator != null )
            {
                successorFather = _iterator.FatherNode;
                successor = _iterator;
                _iterator = _iterator.LeftNode;
            }
            return successor;
        }

        private bool IsLeafNode( TreeNode<T> node )
        {
            return ( node.LeftNode == null && node.RightNode == null ) ? true : false;
        }

        private bool HasTwoLeafNodes( TreeNode<T> node )
        {
            return ( node.LeftNode != null && node.RightNode != null ) ? true : false;
        }

        #endregion
    }
    public class MAIN
    {
        //public static void Main( string[] args )
        //{
        //    BinaryTree<double> bt = new BinaryTree<double>();
        //    bt.Add( 2.0 );
        //    bt.Add( 1.0 );
        //    bt.Add( 1.5 );
        //    bt.Add( 3.0 );
        //    bt.Add( 2.5 );
        //    TreeNode<double> b = bt.Root;
        //    TreeNode<double> a = bt.getCloseToX( b, 0.9 );
        //    // int c = bt.Count;
        //    Console.Out.WriteLine( "node:" + a.ToString() );

        //    TreeNode<double>[] a1 = bt.getCloseTwo( a );
        //    if( a1[ 0 ] != null && a1[ 1 ] != null )
        //    {
        //        Console.Out.WriteLine( "node:" + a1[ 0 ].ToString() );
        //        Console.Out.WriteLine( "node:" + a1[ 1 ].ToString() );
        //    }
        //    else if( a1[ 0 ] == null && a1[ 1 ] != null )
        //    {
        //        Console.Out.WriteLine( "node:" + "start" );
        //        Console.Out.WriteLine( "node:" + a1[ 1 ].ToString() );
        //    }
        //    else if( a1[ 0 ] != null && a1[ 1 ] == null )
        //    {
        //        Console.Out.WriteLine( "node:" + a1[ 0 ].ToString() );
        //        Console.Out.WriteLine( "node:" + "end");
        //    }
        //}
    }
}
