﻿using System;
using System.Collections;
using System.Globalization;
using System.Text;

namespace BTree
{

    public class TreeNode
    {
        public object data;
        public TreeNode leftNode;
        public TreeNode rightNode;

        public TreeNode( object item )
        {
            data = item;
            leftNode = rightNode = null;
        }

        public void PrintNode()
        {
            string result = "";
            if( this == null )
                return;
            result = "node.data is :" + this.data.ToString();
            Console.WriteLine( result );
        }
    } // end of TreeNode class

    public class BTree
    {
        public TreeNode root;

        // create an empty BTree
        public BTree()
        {
            root = null;
        }

        public void Destroy( TreeNode node )
        {
            if( root != null )
            {
                Destroy( root.leftNode );
                Destroy( root.rightNode );
                root = null;
            }
        }

        public void CreateBTree( int n )
        {
            int i = 0;
            for( ; i < n; i++ )
            {
                object temp = Console.ReadLine();
                insert( temp );
            }
        }

        public void Inorder( TreeNode root )
        {
            if( root == null )
                return;
            Inorder( root.leftNode );
            root.PrintNode();
            Inorder( root.rightNode );
        }

        public void Firstorder( TreeNode root )
        {
            if( root == null )
                return;
            root.PrintNode();
            Firstorder( root.leftNode );
            Firstorder( root.rightNode );
        }

        public void Backorder( TreeNode root )
        {
            if( root == null )
                return;
            Backorder( root.leftNode );
            Backorder( root.rightNode );
            root.PrintNode();
        }


        public TreeNode search( object item )
        {
            TreeNode temp = null;
            if( root.data.Equals( item ) )
                temp = root;
            else
                temp = search( root, item );
            return temp;
        }

        public TreeNode search( TreeNode root, object item )
        {
            TreeNode tmp = null;
            if( root.data.Equals( item ) )
            {
                tmp = root;
                return tmp;
            }
            int result = Comparer.Default.Compare( root.data, item );
            if( result > 0 )
            {
                if( root.leftNode != null )
                    tmp = search( root.leftNode, item );
            }
            else
            {
                if( root.rightNode != null )
                    tmp = search( root.rightNode, item );
            }
            return tmp;
        }

        public void insert( object item )
        {
            if( root == null )
            {
                root = new TreeNode( item );
            }
            else
                insert( root, item );
        }

        public void insert( TreeNode node, object item )
        {
            int result = Comparer.Default.Compare( node.data, item );
            if( result >= 0 )
            {
                if( node.leftNode == null )
                {
                    TreeNode temp = new TreeNode( item );
                    node.leftNode = temp;
                }
                else
                    insert( node.leftNode, item );
            }
            else
            {
                if( node.rightNode == null )
                {
                    TreeNode temp = new TreeNode( item );
                    node.rightNode = temp;
                }
                else
                    insert( node.rightNode, item );
            }
        } //end of insert


        public TreeNode MinRight( TreeNode node )
        {
            if( node.leftNode == null )
                return node;
            else
                return MinRight( node.leftNode );
        }

        public TreeNode searchParent( TreeNode head, TreeNode p )
        {
            if( head.leftNode == p || head.rightNode == p || head == null )
                return head;
            int result = Comparer.Default.Compare( p.data, head.data );
            if( result <= 0 )
                return searchParent( head.leftNode, p );
            else
                return searchParent( head.rightNode, p );
        }

        public void delete( object item )
        {
            TreeNode p = null;

            p = search( root, item );

            if( p == null ) // p is not the item of the BTree
            {
                Console.WriteLine( "don't find the item" );
                return;
            }

            if( p == root ) // p is the root node
            {
                if( root.leftNode != null || root.rightNode != null )
                {
                    Console.WriteLine( "BTree root node can't be delete yet" );
                    return;
                }
                else
                {
                    root = null;
                    Console.WriteLine( "BTree root node has been deleted" );
                    return;
                }
            }

            else // p is not the root node
            {
                TreeNode parent = null;
                parent = searchParent( root, p );
                if( p.leftNode == null && p.rightNode == null ) // p is leaf node
                {
                    if( parent.leftNode == p )
                        parent.leftNode = null;
                    else
                        parent.rightNode = null;
                }
                else //p is not leaf node
                {
                    if( p.rightNode == null ) // p has no right child node
                    {
                        if( parent.leftNode == p )
                            parent.leftNode = p.leftNode;
                        else
                            parent.rightNode = p.leftNode;
                    }
                    if( p.leftNode == null ) // p has no left child node
                    {
                        if( p.leftNode == p )
                            parent.leftNode = p.rightNode;
                        else
                            parent.rightNode = p.rightNode;
                    }
                    else if( p.leftNode != null && p.rightNode != null ) // p has both left node and right node
                    {
                        TreeNode rightMinSon, rightMinParent;
                        rightMinSon = MinRight( p.rightNode );
                        rightMinParent = searchParent( p.rightNode, rightMinSon );

                        rightMinParent.leftNode = rightMinSon.rightNode;

                        if( p.rightNode == rightMinSon )
                        {
                            p.rightNode = rightMinSon.rightNode;
                        }
                        p.data = rightMinSon.data;

                    }

                }
            }
        } // end of delete

    } // end of BTree class



    public class MainEntry
    {
       /* public static void Main()
        {
            BTree tree = new BTree();

            Console.Write( " *********************************************** " );
            Console.WriteLine( "Create BTree" );
            Console.WriteLine( "please input the numbers of the BTree which you want to create" );
            int n = Convert.ToInt32( Console.ReadLine() );
            Console.WriteLine( "please input the items:" );
            tree.CreateBTree( n );
            Console.WriteLine( "***********************************************" );

            Console.Write( " *********************************************** " );
            Console.WriteLine( "BTree first search" );
            tree.Firstorder( tree.root );
            Console.WriteLine( "***********************************************" );

            Console.Write( " *********************************************** " );
            Console.WriteLine( "BTree inorder search" );
            tree.Inorder( tree.root );
            Console.WriteLine( "***********************************************" );

            Console.Write( " *********************************************** " );
            Console.WriteLine( "BTree backorder search" );
            tree.Backorder( tree.root );
            Console.WriteLine( "***********************************************" );

            Console.Write( " *********************************************** " );
            Console.WriteLine( "search item in BTree" );
            Console.WriteLine( "please input the item you want to search" );

            object obj = Console.ReadLine();
            TreeNode result = tree.search( obj );
            if( result != null )
                Console.WriteLine( "has find the item {0}", result.data );
            else
                Console.WriteLine( "has not find the item {0}", obj );

#if DEBUG
            for( int i = 1; i < n + 1; i++ )
            {
                object item = null;
                TreeNode temp = null;
                item = Console.ReadLine();
                temp = tree.search( item );
                if( temp != null )
                    Console.WriteLine( "has find the item {0}", temp.data );
                else
                    Console.WriteLine( "has not find the item {0}", item );
            }
#endif
            Console.WriteLine( "***********************************************" );

            Console.Write( " *********************************************** " );
            Console.WriteLine( "insert item in BTree" );
            Console.WriteLine( "please input the item you want to insert" );
            obj = Console.ReadLine();
            tree.insert( obj );
            tree.Firstorder( tree.root );
            Console.WriteLine( "***********************************************" );


            Console.Write( " *********************************************** " );
            Console.WriteLine( "delete item in BTree" );
            Console.WriteLine( "please input the item you want to delete" );
            object obj1 = Console.ReadLine();
            tree.delete( obj1 );
            if( tree.root != null )
                tree.Firstorder( tree.root );
            else
                Console.WriteLine( "the BTree is empty" );
#if DEBUG
            object test = null;
            for( int i = 1; i <= n + 2; i++ )
            {
                test = Console.ReadLine();
                tree.delete( test );
                if( tree.root != null )
                    tree.Firstorder( tree.root );
                else
                    Console.WriteLine( "the BTree is empty" );
            }
#endif

            Console.WriteLine( "***********************************************" );
            Console.ReadKey();
           
        } */
    }
} // end of namespace BTree