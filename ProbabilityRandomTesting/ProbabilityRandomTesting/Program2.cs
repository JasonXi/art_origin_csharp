﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProbabilityRandomTesting
{
    class Program2
    {
        double fail_start;
        double fail_rate = 0.01;
        double R = 0.75;
        int randomseed;
        BinaryTree<double> bstree = new BinaryTree<double>();
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="seed"></param>
        public Program2(int seed)
        {
            randomseed = seed;

        }
        public int run()
        {
            Random random = new Random( randomseed );
            int count = 0;
            fail_start = ( 1.0 - fail_rate ) * random.NextDouble();
            // Console.Out.WriteLine("failure_start:"+fail_start);
            double p = random.NextDouble();
            while( count<30 )
            {
                count++;
                bstree.Add(p);
                Console.Out.WriteLine( "-----------------------" );
                Console.Out.WriteLine("p:"+p);
                double S = random.NextDouble();
                Console.Out.WriteLine( "S:" + S );
                TreeNode<double> T = bstree.getCloseToX(bstree.Root,S);
                Console.Out.WriteLine( "T:" +T.Value );
                TreeNode<double>[] side = bstree.getCloseTwo( bstree.Root,T );
                double start, end;
               
                if( side[ 0 ] == null && side[1]!=null)
                {
                    start = 0.0;
                    end = (side[ 1 ].Value+T.Value)/2;
                  //  Console.Out.WriteLine( "1"  );
                }
                else if( side[ 0 ] != null && side[ 1 ] == null )
                {
                    start = (side[ 0 ].Value + T.Value)/2;
                    end = 1.0;
                  //  Console.Out.WriteLine( "2" );
                }
                else if( side[ 0 ] == null && side[ 1 ] == null )
                {
                    start = 0.0;
                    end = 1.0;
                  //  Console.Out.WriteLine( "3" );
                }
                else
                {
                    start = ( side[ 0 ].Value + T.Value ) / 2;
                    end = ( side[ 1 ].Value + T.Value ) / 2;
                  //  Console.Out.WriteLine( "4" );
                }
                Console.Out.WriteLine("start:"+start+" end:"+end);
                 ////////////利用算法
                int ktemp = 0;
                start = Math.Pow(start-T.Value,3);
                end = Math.Pow( end - T.Value, 3 );
                double temp = random.NextDouble() * ( end - start ) + start;
                if( temp < 0 )
                {
                    ktemp = 1;
                    temp = Math.Abs( temp );
                }
                temp = Math.Pow( temp, 1.0 / 3.0 );
                if( ktemp == 1 )
                    temp = -temp;
                p = temp + T.Value;
            }
            return count;
        }
        public bool test( double p )
        {
            bool flag;
            if( p > fail_start && p < ( fail_start + fail_rate ) )
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
            return flag;
        }
        //static void Main( string[] args )
        //{
        //    int cishu = 1;
        //    long sumOfF = 0;
        //    // MyTimer timer = new MyTimer();
        //    //timer.Start();
        //    for( int i = 0; i < cishu; i++ )
        //    {
        //        Program2 program2 = new Program2( ( i + 3 ) * 15 );
        //        int f_measure = program2.run();
        //        // Console.Out.WriteLine( "F-measure：" + f_measure );
        //        sumOfF += f_measure;
        //    }
        //    // timer.Stop();
        //    Console.Out.WriteLine( "F-measure平均值：" + ( sumOfF / ( double ) cishu ) );
        //    Console.ReadLine();
        //}
    }
}
