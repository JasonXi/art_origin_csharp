﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ProbabilityRandomTesting
{
    class Program
    {
        double fail_start;
        double fail_rate = 0.0001;
        double R = 0.75;
        int randomseed;
        ArrayList al = new ArrayList();

        public Program(int seed)
        {
            randomseed = seed;

        }
        public int run()
        {
            Random random = new Random( randomseed );
            int count = 0;
            fail_start = ( 1.0 - fail_rate ) * random.NextDouble();
           // Console.Out.WriteLine("failure_start:"+fail_start);
            double p = random.NextDouble();
            while( test(p))
            {
                count++;
              //  Console.Out.WriteLine( "--------------------------------" );
              //  Console.Out.WriteLine("p:"+p);
                if (al.Count == 0) { al.Add(p); }
                else
                {
                    //对al进行排序
                    //////////////////////////////////////////////////
                    int low = 0, high = al.Count - 1, mid = -1;
                    while (low <= high)
                    {
                        mid = (low + high) / 2;
                        if (p > (double)al[mid])
                        {
                            low = mid + 1;
                        }
                        else
                        {
                            high = mid - 1;
                        }
                    }
                    if (p < (double)al[mid]) { mid = mid - 1; }
                    //  al.Add(p);
                    // ordered_al.Add(p);
                    al.Insert(mid + 1, p);
                }
                ///////////////////////////////////////
                
                double S = random.NextDouble();
              
                double length = double.MaxValue;
                int k=-1;
                for( int i = 0; i < al.Count; i++ )
                {
                    if( Math.Abs( ( double ) al[ i ] - S ) < length )
                    {
                        k = i;
                        length = Math.Abs( ( double ) al[ i ] - S );
                    }
                }
               // Console.Out.WriteLine( "S:" + S+" k:"+k );
                double left=-1.0, right=-1.0;
                if( k == 0 && k==(al.Count-1))
                {
                    left = 0.0;
                    right = 1.0;
                }
                if( k == 0 && k != ( al.Count - 1 ) )
                {
                    left = 0.0;
                    right = ((double)al[k]+(double)al[k+1])/2;
                }
                if( k != 0 && k == ( al.Count - 1 ) )
                {
                    left = ( ( double ) al[ k-1 ] + ( double ) al[ k  ] ) / 2;
                    right = 1.0;
                }
                if( k != 0 && k != ( al.Count - 1 ) )
                {
                    left = ( ( double ) al[ k-1 ] + ( double ) al[ k  ] ) / 2;
                    right = ( ( double ) al[ k ] + ( double ) al[ k + 1 ] ) / 2;
                }
             //   Console.Out.WriteLine("left:"+left+" right:"+right );
                left = Math.Pow( ( left - ( double ) al[ k ] ), 3.0 );
                right = Math.Pow( ( right - ( double ) al[ k ] ), 3.0 );
              
                double temp = random.NextDouble() * ( right - left ) + left;
            
                int ktemp = 0;
                if( temp < 0 )
                {
                    ktemp = 1;
                    temp = Math.Abs(temp);
                }
                temp = Math.Pow(temp,1.0/3.0);
                if( ktemp == 1 )
                    temp = -temp;
               // Console.Out.WriteLine( "temp2:" + temp );
                p=temp+(double)al[k];
            }
           // Console.Out.WriteLine( "p:" + p );
            return count;
        }
        public bool test( double p )
        {
            bool flag;
            if( p > fail_start && p < ( fail_start + fail_rate ) )
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
            return flag;
        }
        static void Main( string[] args )
        {
            int cishu = 1000;
            long sumOfF = 0;
            // MyTimer timer = new MyTimer();
            //timer.Start();
            for( int i = 0; i < cishu; i++ )
            {
                Program program = new Program( ( i + 3 ) * 15 );
                int f_measure = program.run();
                // Console.Out.WriteLine( "F-measure：" + f_measure );
                sumOfF += f_measure;
            }
            // timer.Stop();
            Console.Out.WriteLine( "F-measure平均值：" + ( sumOfF / ( double ) cishu ) );
            Console.ReadLine();
        }
    }
}
