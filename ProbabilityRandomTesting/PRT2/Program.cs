﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRT2
{
    class Program
    {
        double fail_start;
        double fail_rate = 0.0001;
        //double R = 0.75;
        BSTree bstree = new BSTree();
        public int run(int seed)
        {
            Random random = new Random(seed);
            fail_start=random.NextDouble()*(1-fail_rate);
            int count = 0;
            double p = random.NextDouble();
            while(test(p))
            {
                count++;
                bstree.insert(p);
                //Console.Out.WriteLine( "--------------------" );
                //Console.Out.WriteLine("p:"+p);
                double S = random.NextDouble();
                double low=0.0, high=0.0;
                Node[] Sides = bstree.GetSides(S);
                Node node = bstree.getCloseNode(S);
                if( Sides[ 0 ] != null )
                {
                    low = ( node.value + Sides[ 0 ].value ) / 2.0;
                }
                else
                {
                    low = 0.0;
                }
                if( Sides[ 1 ] != null )
                {
                    high = ( node.value + Sides[ 1 ].value ) / 2.0;
                }
                else
                {
                    high = 1.0;
                }
               // Console.Out.WriteLine( "S:" + S+",node:"+node.value );
          
               // Console.Out.WriteLine( "low:"+low+",high:"+high );
                
                int ktemp = 0;
                low = Math.Pow( low -node.value, 3 );
                high = Math.Pow( high - node.value, 3 );
                double temp = random.NextDouble() * ( high - low ) + low;
                if( temp < 0 )
                {
                    ktemp = 1;
                    temp = Math.Abs( temp );
                }
                temp = Math.Pow( temp, 1.0 / 3.0 );
                if( ktemp == 1 )
                    temp = -temp;
                p = temp + node.value;
            }
            return count;
        }
        public bool test( double p )
        {
            bool flag;
            if( p > fail_start && p < ( fail_start + fail_rate ) )
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
            return flag;
        }
        static void Main( string[] args )
        {
            long sumcount = 0;
            MyTimer timer = new MyTimer();
            int times=1000;
            timer.Start();
            for( int i = 0; i < times; i++ )
            {
                Program program = new Program();
                int count = program.run( (i+3)*15 );
                sumcount += count;
            }
            timer.Stop();
            Console.Out.WriteLine( "average f_measure:"+(sumcount/(double)times)+",time:"+(timer.Duration/(double)times ));
            Console.Read();
        }
    }
}
