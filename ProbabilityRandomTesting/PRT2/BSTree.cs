﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PRT2
{
    //二叉树的节点定义
    class Node
    {
       public  double value;
       public  Node Left;
       public  Node Right;
      // public string ltag, rtag;
    }
    class BSTree
    {
      // public ArrayList<Node> bstree=new ArrayList<Node>();
     //  public Node root=null;
       public Node root = null;
       public void insert(double nodeValue)
       {
           Node parent;
           Node newNode = new Node();
           newNode.value = nodeValue;
           if( root == null )
           {
               root = newNode;
           }
           else
           {
               Node Current = root;
               while( true )
               {
                   parent = Current;
                   if( newNode.value < Current.value )
                   {
                       Current = Current.Left;
                       if( Current == null )
                       {
                           parent.Left = newNode;
                           break;
                       }
                   }
                   else
                   {
                       Current = Current.Right;
                       if( Current == null )
                       {
                           parent.Right = newNode;
                           break;
                       }
                   }
               }
           }
       }
       public void LDR( Node root )
       {
           if( root != null )
           {
               LDR(root.Left);
               Console.Out.WriteLine("value:"+root.value);
               LDR( root.Right );
           }
       }
        //得到一个节点
       public Node getNode(double value)
       {
           Node Current=this.root;
           while( true )
           {
               if( Current != null )
               {
                   if( Current.value != value )
                   {
                       if( Current.value > value )
                       {
                           Current = Current.Left;
                       }
                       else
                       {
                           Current = Current.Right;
                       }
                   }
                   else
                   {
                       return Current;
                   }
               }
               else
               {
                   return null;
               }
           }
       }
        //  得到最小节点，最左边的节点
       public Node getLeftMin(Node s)
       {
           while( s.Left!=null )
           {
               s = s.Left;
           }
           return s;
       }
       public Node getRightMax( Node s )
       {
           while( s.Right != null )
           {
               s = s.Right;
           }
           return s;
       }
        //得到中序的下一个节点
        public Node getNextInoder(Node pnode)
       {
           if( this.root == null || pnode == null )
               return null;
           if( pnode.Right != null )
           {
               return getLeftMin(pnode.Right);
           }
           Node succ = null;
           Node root=this.root;
           while( root != null )
           {
               if( pnode.value < root.value )
               {
                   succ = root;
                   root = root.Left;
               }
               else if( pnode.value > root.value )
                   root = root.Right;
               else
                   break;
           }
           return succ;
       }
        //得到中序的上一个节点
        public Node getPreInorder( Node pnode )
        {
            if( this.root == null || pnode == null )
                return null;
            if( pnode.Left  != null )
            {
                return getRightMax( pnode.Left );
            }
            Node succ = null;
            Node root = this.root;
            while( root != null )
            {
                if( pnode.value > root.value )
                {
                    succ = root;
                    root = root.Right;
                }
                else if( pnode.value < root.value )
                    root = root.Left;
                else
                    break;
            }
            return succ;
        }
        
        public Node getCloseNode(double S )
        {
            ArrayList al = new ArrayList();
            double min=double.MaxValue;
            Node temp=null;
            Node root=this.root;
            if( root == null )
                return null;
            while( root != null || al.Count != 0 )
            {
                while( root != null )
                {
                    al.Add(root);
                    root = root.Left;
                }
                /* ddd */
                root=(Node)al[al.Count-1];
                al.RemoveAt( al.Count - 1 );
                if( Math.Abs( root.value - S ) < min )
                {
                    temp = root;
                    min = Math.Abs( root.value - S );
                   // Console.Out.WriteLine("get:"+temp.value);
                }
                root = root.Right;
            }
            return temp;
        }
        public Node[] GetSides(double S)
       {
            Node[] node=new Node[2];
            Node current=getCloseNode(S);
            node[ 0 ] = getPreInorder(current);
            node[ 1 ] = getNextInoder(current);
            return node;
       }
    }
    class TestProgram
    {
        //public static void Main( string[] args )
        //{
        //    BSTree bstree = new BSTree();
        //    bstree.insert( 0.69 );
        //    double S = 0.8649;
        //    Node a = bstree.getCloseNode( S );
        //    Console.Out.WriteLine( a.value );
        //    Node[] aa = bstree.GetSides( S );
        //    bstree.LDR( bstree.root );
        //    //Console.Out.WriteLine( ( aa[ 0 ].value + a.value ) / 2.0 );
        //   // Console.Out.WriteLine( ( aa[ 1 ].value + a.value ) / 2.0 );
        //    bstree.insert( 0.05 );
        //    S = 0.56049;
        //    a = bstree.getCloseNode( S );
        //    Console.Out.WriteLine( a.value );
        //    aa = bstree.GetSides( S );
        //    bstree.LDR( bstree.root );
        //    //Console.Out.WriteLine( ( aa[ 0 ].value + a.value ) / 2.0 );
        //    //Console.Out.WriteLine( ( aa[ 1 ].value + a.value ) / 2.0 );
        //    bstree.insert( 0.416 );
        //    S = 0.250197;
        //    a = bstree.getCloseNode( S );
        //    Console.Out.WriteLine( a.value );
        //    aa = bstree.GetSides( S );
        //    bstree.LDR( bstree.root );
        //    //Console.Out.WriteLine( ( aa[ 0 ].value + a.value ) / 2.0 );
        //    //Console.Out.WriteLine( ( aa[ 1 ].value + a.value ) / 2.0 );
        //    //bstree.LDR( bstree.root );
           
           
        //}
    }
}
