﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RT_OneDimension
{
    class Program
    {
        double fail_start;
        double fail_rate=0.005;
        int randomseed;
        public Program(int seed) 
        {
            randomseed = seed;
        }
        public int  run() { 
            Random random=new Random(randomseed);
            int count=0;
            fail_start = random.NextDouble()*(1-fail_rate);
            Console.Out.WriteLine("fail:(" + fail_start + "," + (fail_start+fail_rate) + ")");
            double p = random.NextDouble();
            while (test(p)) {
                count++;
                Console.Out.WriteLine("p"+count+":"+p);
                p = random.NextDouble();
            }
            count++;
            Console.Out.WriteLine("p" + count + ":" + p);
            return count;
        }
        public bool test(double p) {
            bool flag;
            if (p > fail_start && p < (fail_start + fail_rate))
            {
                flag = false;
            }
            else {
                flag = true;
            }
            return flag;
        }
        static void Main(string[] args)
        {
            int cishu = 2000;
            long sumOfF = 0;
            MyTimer timer = new MyTimer();
            timer.Start();
            for (int i = 0; i < cishu; i++)
            {
                Program program = new Program((i+3)*17);
                int f_measure=program.run();
                sumOfF += f_measure;
            }
            timer.Stop();
            Console.Out.WriteLine("F-measure平均值："+(sumOfF/cishu)+" timer:"+(timer.Duration/cishu));
            Console.ReadLine();
        }
    }
}
