﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;


namespace ARTLib
{
   public class Input
    {
       public  int dimension;
       public  ArrayList item_list;
        public Input(int dim)
        {
            dimension = dim;
            item_list = new ArrayList(dim);

        }
       public override string ToString()
       {
           StringBuilder sb = new StringBuilder("(");
           for (int i = 0; i < dimension; i++)
           {
               sb.Append(item_list[i].ToString() + ",");
               
           }
           sb.Remove(sb.Length - 1, 1);
           sb.Append(")");
           return sb.ToString();
           //return base.ToString();
       }


   }
}
