﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ARTLib
{
   public class HilbertCurve
    {

     /// <summary>
        ///0.[00001000]numbits为8，key为8
     /// </summary>
     /// <param name="key"></param>
     /// <param name="numbits"></param>
     /// <param name="xx"></param>
     /// <param name="yy"></param>
       public void convert_hilbert_key(uint key, int numbits, ref uint xx,ref uint yy){

          uint[,] abcd = new uint[4, 2] { { 0, 0 }, { 0, 1 }, { 1, 1 }, { 1, 0 } };//({0, 0}, {0, 1}, {1, 1}, {1, 0}); /* unit square */
          uint[,] tmp = new uint[4, 2];
       while(key>1) /* should be > 0, but this is safer for (invalid) odd numbers */
{
 //uint[,] tmp=new uint[4,2]; /* save abcd here */
       byte subcell;// unsigned char subcell;
       for (int j = 0; j < 1; j++)
       {
           for (int k = 0; k < 4; k++)
           {
               tmp[k,j] = abcd[k,j];
           }
       }

//memcpy(tmp, abcd, sizeof tmp); /* save our 4 points with the new ones */

numbits -= 2; /* each subdivision decision takes 2 bits */
           uint u_subcell=(key >> numbits) & 3;
subcell = byte.Parse(u_subcell.ToString()); /* namely these two (taken from the top) */
key &= (uint)((1<<numbits) - 1); /* update key by removing the bits we used */

/** Add the two points with indices u and v (in tmp) and store the result at
* index dst in abcd (for both x(0) and y(1)).
**/
//#define ADD(dst, u, v) (abcd[(dst)][0] = tmp[(u)][0] + tmp[(v)][0],
//abcd[(dst)][1] = tmp[(u)][1] + tmp[(v)][1])

switch (subcell)
{ /* divide into subcells */
case 0:
/* h(key, numbits, a << 1, a + d, a + c, a + b); */
    ADD(0, 0, 0, abcd, tmp); ADD(1, 0, 3, abcd, tmp); ADD(2, 0, 2, abcd, tmp); ADD(3, 0, 1, abcd, tmp);
break;
case 1:
/* h(key, numbits, b + a, b << 1, b + c, b + d); */
    ADD(0, 1, 0, abcd, tmp); ADD(1, 1, 1, abcd, tmp); ADD(2, 1, 2, abcd, tmp); ADD(3, 1, 3, abcd, tmp);
break;
case 2:
/* h(key, numbits, c + a, c + b, c << 1, c + d); */
    ADD(0, 2, 0, abcd, tmp); ADD(1, 2, 1, abcd, tmp); ADD(2, 2, 2, abcd, tmp); ADD(3, 2, 3, abcd, tmp);
break;
case 3:
/* h(key, numbits, d + c, d + b, d + a, d << 1); */
    ADD(0, 3, 2, abcd, tmp); ADD(1, 3, 1, abcd, tmp); ADD(2, 3, 0, abcd, tmp); ADD(3, 3, 3, abcd, tmp);
break;
}

//#undef ADD
}
/* final result is the midpoint of the cell, i.e. (a + b + c + d) / 4 */
xx = (abcd[0,0] + abcd[1,0] + abcd[2,0] + abcd[3,0] + 1) >> 2;
yy = (abcd[0,1] + abcd[1,1] + abcd[2,1] + abcd[3,1] + 1) >> 2;
Console.WriteLine("二维点为x="+xx.ToString()+";y="+yy.ToString());
/*	printf(”x: %u y: %un”, *xx, *yy);*/

}
       public void ADD(int dst,int u, int v,uint[,] abcd,uint[,] tmp){
           abcd[(dst),0] = tmp[(u),0] + tmp[v,0];
abcd[(dst),1] = tmp[(u),1] + tmp[(v),1];
       }
       /// <summary>
       /// 0.[00001000]numbits为8，key为8
       /// </summary>
       /// <param name="key"></param>
       /// <param name="numbits"></param>
       /// <param name="xx"></param>
       /// <param name="yy"></param>
        public ArrayList convert_hilbert_key_V1(uint key, int numbits, ref double xx,ref double yy){

          double[,] abcd = new double[4, 2] { { 0, 0 }, { 0, 1 }, { 1, 1 }, { 1, 0 } };//({0, 0}, {0, 1}, {1, 1}, {1, 0}); /* unit square */
          double[,] tmp = new double[4, 2];
          ArrayList nds = new ArrayList();
         
       while(numbits>1) /* should be > 0, but this is safer for (invalid) odd numbers */
{
 //uint[,] tmp=new uint[4,2]; /* save abcd here */
       byte subcell;// unsigned char subcell;
       for (int j = 0; j < 2; j++)
       {
           for (int k = 0; k < 4; k++)
           {
               tmp[k,j] = abcd[k,j];
           }
       }

//memcpy(tmp, abcd, sizeof tmp); /* save our 4 points with the new ones */

numbits -= 2; /* each subdivision decision takes 2 bits */
           uint u_subcell=(key >> numbits) & 3;
subcell = byte.Parse(u_subcell.ToString()); /* namely these two (taken from the top) */
//key &= (uint)((1<<numbits) - 1); /* update key by removing the bits we used */

/** Add the two points with indices u and v (in tmp) and store the result at
* index dst in abcd (for both x(0) and y(1)).
**/
//#define ADD(dst, u, v) (abcd[(dst)][0] = tmp[(u)][0] + tmp[(v)][0],
//abcd[(dst)][1] = tmp[(u)][1] + tmp[(v)][1])

switch (subcell)
{ /* divide into subcells */
case 0:
/* h(key, numbits, a << 1, a + d, a + c, a + b); */
    ADD(0, 0, 0, abcd, tmp); ADD(1, 0, 3, abcd, tmp); ADD(2, 0, 2, abcd, tmp); ADD(3, 0, 1, abcd, tmp);
break;
case 1:
/* h(key, numbits, b + a, b << 1, b + c, b + d); */
    ADD(0, 1, 0, abcd, tmp); ADD(1, 1, 1, abcd, tmp); ADD(2, 1, 2, abcd, tmp); ADD(3, 1, 3, abcd, tmp);
break;
case 2:
/* h(key, numbits, c + a, c + b, c << 1, c + d); */
    ADD(0, 2, 0, abcd, tmp); ADD(1, 2, 1, abcd, tmp); ADD(2, 2, 2, abcd, tmp); ADD(3, 2, 3, abcd, tmp);
break;
case 3:
/* h(key, numbits, d + c, d + b, d + a, d << 1); */
    ADD(0, 3, 2, abcd, tmp); ADD(1, 3, 1, abcd, tmp); ADD(2, 3, 0, abcd, tmp); ADD(3, 3, 3, abcd, tmp);
break;
}

//#undef ADD
}
/* final result is the midpoint of the cell, i.e. (a + b + c + d) / 4 */
xx = (abcd[0,0] + abcd[1,0] + abcd[2,0] + abcd[3,0] ) /4.0;
yy = (abcd[0,1] + abcd[1,1] + abcd[2,1] + abcd[3,1] ) /4.0;
nds.Add(xx);
nds.Add(yy);
return nds;
//Console.WriteLine("二维点为x="+xx.ToString()+";y="+yy.ToString());
/*	printf(”x: %u y: %un”, *xx, *yy);*/

}
       public void ADD(int dst,int u, int v,double[,] abcd,double[,] tmp){
           abcd[(dst),0] = (tmp[(u),0] + tmp[v,0])/2.0;
abcd[(dst),1] = (tmp[(u),1] + tmp[(v),1])/2.0;
       }
       /// <summary>
       /// 将一维转换为多维
       /// </summary>
       /// <param name="hilbercode"></param>
       /// <param name="dim"></param>
       /// <returns></returns>
       public string[] HilbertCode2Coordinates(string hilbercode,uint dim)
       {
          const int Wordbits=32;
          const int OrderOfHilbert=32;
           int ndimbits=hilbercode.Length;
           int dim_int = Int32.Parse(dim.ToString());

           //填充补0使得hilbertcode为dim的整数倍位
           int paddings = -1;
           int intNdim = ndimbits / dim_int;
           if ((ndimbits % dim_int) != 0)
           {
               
               
                ndimbits = intNdim * dim_int + dim_int;
                paddings = ndimbits - hilbercode.Length;
               for (int kindex = 0; kindex < paddings; kindex++)
               {
                   hilbercode += "0";
               }
               
           }
           string[] point = new string[dim];
          

           uint	mask = (uint)1 << Wordbits - 1;//31个1
           uint element, temp1, temp2, A, W = 0, S, tS, T, tT, J, P = 0, xJ;
	    
	       int	i =0, j;
           for (int kindex = 0; kindex < dim; kindex++)
           {
               point[kindex] = "";
           }

	/*--- P ---*/
           string p_str = hilbercode.Substring(i, dim_int);
	       P=Convert.ToUInt32(p_str,2);
	/*--- xJ ---*/
	J = dim;
	for (j = 1; j < dim_int; j++)
		if ((P >> j & 1) == (P & 1))
			continue;
		else
			break;
    if (j != dim_int)
    {
        J=J-uint.Parse(j.ToString());
    }
	xJ = J - 1;

	/*--- S, tS, A ---*/
	A = S = tS = P ^( P / 2);//异或运算


	/*--- T ---*/
	if (P < 3)
		T = 0;
	else
		if (P % 2!=0)
			T = (P - 1) ^ (P - 1) / 2;
		else
			T = (P - 2) ^ (P - 2) / 2;

	/*--- tT ---*/
	tT = T;

	/*--- distrib bits to coords ---*/
	/*for (j = DIM - 1; P > 0; P >>=1, j--)
		if (P & 1)
			pt.hcode[j] |= mask;*/
    for (j = dim_int - 1; j>= 0; A >>= 1, j--)
    {
        if ((A & 1) != 0)
            point[j] = point[j] + "1";
        else
            point[j] = point[j] + "0";
    }


    int noOfshiftbits = 0;
	for (i=dim_int, mask >>= 1; i<ndimbits; i=i+dim_int, mask >>= 1)
	{
		/*--- P ---*/
        p_str=hilbercode.Substring(i,dim_int);
        P=Convert.ToUInt32(p_str,2);
		
		
		/*--- S ---*/
		S = P ^ (P / 2);

		/*--- tS ---*/
        noOfshiftbits =(int) (xJ % dim);
		if (xJ % dim != 0)
		{
			temp1 = S >> (noOfshiftbits);
			temp2 = S << (dim_int - noOfshiftbits);
			tS = temp1 | temp2;
			tS &= ((uint)1 << dim_int) - 1;
		}
		else
			tS = S;

		/*--- W ---*/
		W ^= tT;

		/*--- A ---*/
		A = W ^ tS;

		/*--- distrib bits to coords ---*/
        for (j = dim_int - 1; j>=0; A >>= 1, j--)
        {
            //if ((A & 1)!=0)
            //    point[j] |= mask;
            if ((A & 1) != 0)
                point[j] = point[j] + "1";
            else
                point[j] = point[j] + "0";
        }

		
			/*--- T ---*/
			if (P < 3)
				T = 0;
			else
				if (P % 2!=0)
					T = (P - 1) ^ (P - 1) / 2;
				else
					T = (P - 2) ^ (P - 2) / 2;

			/*--- tT ---*/
            noOfshiftbits =(int)( xJ % dim);
			if (xJ % dim != 0)
			{
				temp1 = T >> noOfshiftbits;
				temp2 = T << (dim_int - noOfshiftbits);
				tT = temp1 | temp2;
				tT &= ((uint)1 << dim_int) - 1;
			}
			else
				tT = T;

			/*--- xJ ---*/
			J = dim;
			for (j = 1; j < dim; j++)
				if ((P >> j & 1) == (P & 1))
					continue;
				else
					break;
			if (j != dim)
				J -= (uint)j;
			xJ += J - 1;
		
	}
    //for (int kindex = 0; kindex < dim_int; kindex++)
    //{
    //   // point[kindex] = point[kindex] >> 27;
    //    Console.WriteLine(kindex.ToString() + ":" + point[kindex].ToString());
    //}
    return point;



       }
       /// <summary>
       /// 小于1的小数转换为二进制
       /// </summary>
       /// <param name="val_double"></param>
       /// <returns></returns>
       public string Double2Bin(double val_double)
       {
           double ud = 2 * val_double;
           StringBuilder bin_sb = new StringBuilder("");
           double converVal =0;
           uint i=1;
           double eachbit=0;
           double resolution =1e-22;
           if ((ud - 1) == 0)
           {
               bin_sb.Append("1");

           }
           while ((ud - 1) != 0)
           {

               if (ud > 1)
               {
                   eachbit = 1;
                   bin_sb.Append("1");
               }
               else if(ud<1)
               {
                   eachbit = 0;
                   bin_sb.Append("0");
               }
               //eachbit = Convert.ToDouble(ud.ToString().Substring(0, 1));
               converVal += eachbit *( 1/Math.Pow(2, i));
               i = i + 1;

              //bin_sb.Append(eachbit.ToString().Substring(0,1));
               if (ud > 1)
               {
                   ud = (ud - 1) * 2;
               }
               else if(ud<1)
               {
                   ud = ud * 2;
               }
               if ((ud - 1) == 0)
               {
                   bin_sb.Append("1");
                   break;

               }

               
               if (Math.Abs(converVal - val_double) < resolution)//达到一定的精度
               {
                   
                   break;
               }

           }
           return "0."+bin_sb.ToString();
          // Convert.ToUInt32(bin_sb.ToString(), 2);
       }
       public double Bin2Double(string bin_str)
       {
          double sum = 0;
           double binbase=1;
           int len = bin_str.Length;
           for (int i = 0; i < len; i++)
           {
               binbase = binbase * 0.5;
               if (bin_str[i] == '1')
               {
                   sum += binbase;
               }
               

           }
           return sum;

       }

   }

}
