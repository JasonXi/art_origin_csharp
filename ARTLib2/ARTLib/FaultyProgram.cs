﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARTLib
{
    public class FaultyProgram
    {
        private string patterntype = "block";
        public FaultyProgram(string type)
        {
            patterntype = type;
        }
        
        public bool blockPattern(int x, int y)
        {
            if(x>=4&&x<=6&&y>=4&&y<=6)
            return false;
            else
            return true;
        }
       public bool stripPattern(int x, int y)
        {
            if ((x - y > 8) && (x - y == 9) && x >= 9 && x <= 17)
                return false;
            else return true;
            //if (y == 10 && x >= 9 && x <= 17)
            //    return false;
            //else return true;
            //if ((x - y == 4) && x >= 9 && x <= 17)
            //    return false;
            //else return true;
            //if (x == 35&& y >= 20 && y <= 28)
            //    return false;
            //else return true;
            
        }
       public bool pointPattern(int x, int y)
        {
            if (x % 12 == 0 && y % 12 == 0 && x > 12 && y > 12)
                return false;
            else
                return true;
            //if (x % 12 == 0&& x >=12 && x < 48&&(y>=23&&y<=25))
            //    return false;
            //else
            //    return true;
        }
        public bool callProg(int x, int y)
        {
            switch (patterntype)
            {
                case "block":
                   return blockPattern(x, y);
                   
                case "strip":
                    return stripPattern(x, y);
                case "point":
                    return pointPattern(x, y);
                default:
                    return false;
            }
        }
        public string PatternType
        {
            get
            {
                return patterntype;
            }
            set
            {
                patterntype = value;
            }
        }
    }
}
