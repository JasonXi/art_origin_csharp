﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ARTLib
{
   public class DHRT
    {
        int n;
        double a, b, h, eps;
        public ArrayList values;

        public DHRT(double aa, double bb, double hh, double es)
        {
            values = new ArrayList();
             
			    a = aa;  b = bb; 
			   h = hh; eps = es;
			  
		   

        }
       public void dh_root(double acof, double bcof, double cof)
        {     //执行对分法

            int js;
            double z, y, z1, y1, z0, y0;
            n = 0; z = a; y = func(z, acof, bcof, cof);
            while ((z <= b + h / 2.0) && (n<1))
            {
                if (Math.Abs(y) < eps)
                {
                    n = n + 1;
                    values.Add(z);// x[n - 1] = z;
                    z = z + h / 2.0; y = func(z,acof,bcof,cof);
                }
                else
                {
                    z1 = z + h; y1 = func(z1, acof, bcof, cof);
                    if (Math.Abs(y1) < eps)
                    {
                        n = n + 1;
                        values.Add(z1);// x[n - 1] = z1;
                        z = z1 + h / 2.0; y = func(z, acof, bcof, cof);
                    }
                    else if (y * y1 > 0.0)
                    { y = y1; z = z1; }
                    else
                    {
                        js = 0;
                        while (js == 0)
                        {
                            if (Math.Abs(z1 - z) < eps)
                            {
                                n = n + 1;
                                values.Add((z1 + z) / 2.0);// x[n - 1] = (z1 + z) / 2.0;
                                z = z1 + h / 2.0; y = func(z, acof, bcof, cof);
                                js = 1;
                            }
                            else
                            {
                                z0 = (z1 + z) / 2.0; y0 = func(z0, acof, bcof, cof);
                                if (Math.Abs(y0) < eps)
                                {
                                    values.Add(z0);// x[n] = z0; n = n + 1; js = 1;
                                    z = z0 + h / 2.0; y = func(z, acof, bcof, cof);
                                }
                                else if ((y * y0) < 0.0)
                                { z1 = z0; y1 = y0; }
                                else { z = z0; y = y0; }
                            }
                        }
                    }
                }
            }
        }
        public double func(double x,double acof,double bcof,double cof)
        { //计算方程左端函数f(x)值
            double y;
            y = acof*Math.Pow(x,3.0)+bcof*Math.Pow(x,2.0)+cof;
            return y;
        }
    }
}
