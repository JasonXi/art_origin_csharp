﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace ARTLib
{
   public class FailRegion
    {
        private float fail_rate;
        public ArrayList range_list;
        public ArrayList starpos_list;
       public ArrayList dim_rates;
        private int dimension;
        private int size;
       public Input startp;
        public FailRegion()
        {
            
        }
        public int Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }
        public float Fail_rate
        {
            get
            {
                return fail_rate;
            }
            set
            {
                fail_rate = value;
            }
        }
        public int Dimension
        {
            get
            {
                return dimension;
            }
            set
            {
                dimension = value;
            }
        }
       public string dimRatesTostring()
       {
           StringBuilder rates=new StringBuilder("");
           for (int i = 0; i < dimension; i++)
               rates.Append(dim_rates[i].ToString() + ":");
           rates.Remove(rates.Length - 1, 1);
           return rates.ToString();

       }


   }
}
