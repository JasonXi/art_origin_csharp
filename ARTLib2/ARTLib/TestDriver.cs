﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using RNG;
namespace ARTLib
{
   public  class TestDriver
    {
        //初始化一个ProgSimul，包括各维度的范围以及失效域，然后进行测试
        public void SingleSquareForRRT(int dim,int[] rangeMax,float fail_rate, float R, int Max)
        {
            ProgSimul prog = new ProgSimul(dim, fail_rate, rangeMax);
            prog.GenerateFailregion(fail_rate, null, true);
            ART art_instance = new ART();
            if (R > 0)
            {
                art_instance.RRT(R, prog, fail_rate, Max);
            }
            else
            {
                art_instance.RT(prog, fail_rate, Max);
            }

        }
       public void SingleRecForRRT(int dim, int[] rangeMax, int[] dim_rate,float fail_rate, float R, int Max)
       {
           ProgSimul prog = new ProgSimul(dim, fail_rate, rangeMax);
           prog.GenerateFailregion(fail_rate, dim_rate, false);
           ART art_instance = new ART();
           if (R > 0)
           {
               art_instance.RRT(R, prog, fail_rate, Max);
           }
           else
           {
               art_instance.RT(prog, fail_rate, Max);
           }

       }
       public void multiFailsForRRT(float[] size_rates, int dim, int[] rangeMax, float fail_rate, float R, int Max)
       {
           ProgSimul prog = new ProgSimul(dim, fail_rate, rangeMax);
           prog.generateMultiplefailregions(size_rates);
           ART art_instance = new ART();
           if (R > 0)
           {
               art_instance.RRT(R, prog, fail_rate, Max);
           }
           else
           {
               art_instance.RT(prog, fail_rate, Max);
           }
       }
       public void testRealProg(FaultyProgram fp,int dim, int[] rangeMax, int Max,float R)
       {
           ProgSimul prog = new ProgSimul(dim, -1, rangeMax);
           ART art_instance = new ART();
           if (R > 0)
           {
               art_instance.RRTforRealProg(R, Max, fp, prog);
           }
           else
           {
               art_instance.RTforRealProg(prog, Max, fp);
           }

       }
       public void generateSeq(FaultyProgram fp, int dim, int[] rangeMax, int Max, float R)
       {
           ProgSimul prog = new ProgSimul(dim, -1, rangeMax);
           ART art_instance = new ART();
           if (R > 0)
           {
               art_instance.RRTSequence(R, Max, fp, prog);
               
           }
           else
           {
               art_instance.RTSequence(prog, Max, fp);
              
           }

       }
       public double[] failure_rates(uint dim, double rval)
       {
           double[] frs = new double[dim];
           double oneVal=Math.Pow(rval,1.0/(double )dim);
           for (int i = 0; i < dim; i++)
           {
               frs[i] =oneVal;

           }
           return frs;
       }
       /// <summary>
       /// 获得strip的failure rate
       /// </summary>
       /// <param name="dim"></param>
       /// <param name="rval"></param>
       /// <param name="prop"></param>
       /// <returns></returns>
       public double[] failure_rates(uint dim, double rval, double prop)
       {
           double[] frs = new double[dim];
           double min =Math.Pow(rval / Math.Pow(prop,dim-1),1.0/dim);
           for (int i = 0; i < dim; i++)
           {
               frs[i] = min * (i > 0 ? prop : 1);
           }
           return frs;
       }
       /// <summary>
       /// 获得strip的长度比
       /// </summary>
       /// <param name="fr_total"></param>
       /// <param name="props"></param>
       /// <returns></returns>
       public double[] strip_pattern(double fr_total, double[] props)
       {
           double[] props_tmp;
           if (props == null)
           {
               props_tmp = new double[13];
               props_tmp[0] = 1;
               props_tmp[1] = 4;
               props_tmp[2] = 7;
               for (int i = 3; i < props_tmp.Length; i++)
               {
                   props_tmp[i] = (i - 2) * 10;

               }

           }
           else
           {
               props_tmp = props;
           }
           
           return props_tmp;
       }
       public long[] no_point_pattern(long[] no_arr)
       {
           long[] nos_tmp;
           if (no_arr == null)
           {
               nos_tmp = new long[13];
               nos_tmp[0] = 1;
               nos_tmp[1] = 4;
               nos_tmp[2] = 7;
               for (int i = 3; i < nos_tmp.Length; i++)
               {
                   nos_tmp[i] = (i - 2) * 10;

               }

           }
           else
           {
               nos_tmp = no_arr;
           }

           return nos_tmp;
       }
       public SingleFault[] point_pattern(uint dim,double fr_total, long nos, double[] props)
       {
           double min_fr;
           SingleFault[] faults = new SingleFault[nos];
          // double[] props_temp = new double[nos];

           if (props == null)
           {
               min_fr = fr_total / nos;

           }
           else
           {
               double sum = 0;
               for (int i = 0; i < props.Length; i++)
               {
                   sum += props[i];
               }
               min_fr = fr_total / sum;
           }
           for (long i = 0; i < nos; i++)
           {
               if (props == null)
               {
                   faults[i] = new SingleFault(dim, min_fr);
               }
               else
               {
                   faults[i] = new SingleFault(dim, min_fr * props[i]);
               }

           }
           return faults;

       }
       /// <summary>
       /// point pattern
       /// </summary>
       public void doExptOnPoint()
       {
           //Excel.Application excel;
           //excel = new Excel.Application();
           //Excel.Workbooks workbooks;
           //object miss = Missing.Value;
           //Excel.Sheets sheets;
           //Excel._Worksheet sheet = null;
           //string excelname = "resultPoint.xls";
           //workbooks = excel.Workbooks;
           //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           //sheets = excel.Worksheets;
           //sheet = (Excel._Worksheet)sheets.get_Item(1);
           //excel.DisplayAlerts = false;
           Random rtseed = new Random();

           Console.WriteLine("输入空间维度：");
           string sdim = Console.ReadLine();
           uint dim = UInt32.Parse(sdim);
           Console.WriteLine("输入failurerate:");
           double fr = double.Parse(Console.ReadLine());
           Console.WriteLine("生成算法：");
           string salg = Console.ReadLine();
           Console.WriteLine("自定义生成Testcase:");
           uint noOftcs=UInt32.Parse(Console.ReadLine());
           double init_val = 1.0;


           long[] noOfarr = no_point_pattern(null);
           SingleFault[] fault = null;


           executeGTC runalg = null;
           double fresult = -1;

           for (int i = 0; i < noOfarr.Length; i++)
           {
               ART_New art_ins = new ART_New((uint)(rtseed.Next()));
               if (salg == "rt")
               {
                   fresult = art_ins.ARTs(dim, null, point_pattern(dim,fr,noOfarr[i],null), art_ins.doRT_NDS,noOftcs);

               }
               else if (salg == "cos")
               {
                  // fresult = art_ins.ARTs(dim, null, point_pattern(dim,fr,noOfarr[i],null), art_ins.doARTCOS_NDs,noOftcs);
               }
               else if (salg == "x2")
               {
                   //fresult = art_ins.ARTs(dim, null, point_pattern(dim,fr,noOfarr[i],null), art_ins.doARTX2_NDs,noOftcs);
                 fresult = art_ins.ARTs(dim, null, point_pattern(dim, fr, noOfarr[i], null), art_ins.doARTX2_NDs_V1, noOftcs);
               }
               Console.WriteLine("执行no=" + noOfarr[i].ToString() + ";fcount=" + fresult.ToString());
               //sheet.Cells[i + 1, 1] = noOfarr[i];
               //sheet.Cells[i + 1, 2] = fresult;
               //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               art_ins = null;
           }
           // sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           //excel.Workbooks.Close();
           //excel.Quit();

       }
       /// <summary>
       /// block pattern
       /// </summary>
       public void doExponART()
       {

           //Excel.Application excel;
           //excel = new Excel.Application();
           //Excel.Workbooks workbooks;
           //object miss = Missing.Value;
           //Excel.Sheets sheets;
           //Excel._Worksheet sheet = null;
           //string excelname = "result1.xls";
           //workbooks = excel.Workbooks;
           //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           //sheets = excel.Worksheets;
           //sheet = (Excel._Worksheet)sheets.get_Item(1);
           //excel.DisplayAlerts = false;
           Random rtseed = new Random();

           Console.WriteLine("输入空间维度：");
           string sdim = Console.ReadLine();
           uint dim = UInt32.Parse(sdim);
           Console.WriteLine("生成算法：(输入“x2”)");
           string salg = Console.ReadLine();
          // Console.WriteLine("自定义生成Testcase:");
          // uint noOftcs = UInt32.Parse(Console.ReadLine());
           uint noOftcs = 0;
           double init_val = 1.0;
           double[] frs = new double[4];
           
           //for (int i = 0; i < 10; i++)
           //{
               
           //    frs[i] =init_val*(1- 0.5 *(i%2));
              
           //    if ((i+1) % 2 == 0)
           //    {
           //        init_val = 0.1 * init_val;
                   
           //    }
           //}
           frs[0] = 0.01;
           frs[1] = 0.005;
           frs[2] = 0.002;
           frs[3] = 0.001;
           executeGTC runalg = null;
           double fresult = -1;

           for (int i = 0; i < 4; i++)
           {
               ART_New art_ins = new ART_New((uint)(rtseed.Next()));
               if (salg == "rt")
               {
                   fresult = art_ins.ARTs(dim, failure_rates(dim, frs[i]),null, art_ins.doRT_NDS,noOftcs);

               }
               else if (salg == "cos")
               {
                 //  fresult = art_ins.ARTs(dim, failure_rates(dim, frs[i]), null,art_ins.doARTCOS_NDs,noOftcs);
               }
               else if (salg == "x2")
               {
                  // fresult = art_ins.ARTs(dim, failure_rates(dim, frs[i]), null, art_ins.doARTX2_NDs, noOftcs);//1维时调用art_ins.doARTX2_NDs
                  fresult = art_ins.ARTs(dim, failure_rates(dim, frs[i]), null, art_ins.doARTX2_NDs_V1, noOftcs);//2维时调用art_ins.doARTX2_NDs_V1
               }
               else
               {
                   Console.WriteLine("算法名称错误，请输入'x2'");
                   break;
               }
               Console.WriteLine("执行theta=" + frs[i].ToString() + ";fcount=" + fresult.ToString());
               //sheet.Cells[i + 1, 1] = frs[i];
               //sheet.Cells[i + 1, 2] = fresult;
               //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               art_ins = null;
           }
          // sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           //excel.Workbooks.Close();
           //excel.Quit();


       }
       /// <summary>
       /// 计算E-measure
       /// </summary>
       public void computeEmeasure()
       {
         //  Console.Out.WriteLine("start computeEmeasure!!!");
           //Excel.Application excel;
           //excel = new Excel.Application();
           //Excel.Workbooks workbooks;
           //object miss = Missing.Value;
           //Excel.Sheets sheets;
           //Excel._Worksheet sheet = null;
           //string excelname = "result1.xls";
           //workbooks = excel.Workbooks;
           //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           //sheets = excel.Worksheets;
           //sheet = (Excel._Worksheet)sheets.get_Item(1);
           //excel.DisplayAlerts = false;
           Random rtseed = new Random();

           Console.Out.WriteLine("输入空间维度：gggg");
           string sdim = Console.ReadLine();
           uint dim = UInt32.Parse(sdim);
           Console.Out.WriteLine("生成算法：(输入“x2”)+dddd");
           string salg = Console.ReadLine();
           Console.Out.WriteLine("start");
           // Console.WriteLine("自定义生成Testcase:");
           // uint noOftcs = UInt32.Parse(Console.ReadLine());
           uint noOftcs = 0;
           double init_val = 1.0;
           double[] frs = new double[4];

           //for (int i = 0; i < 10; i++)
           //{

           //    frs[i] =init_val*(1- 0.5 *(i%2));

           //    if ((i+1) % 2 == 0)
           //    {
           //        init_val = 0.1 * init_val;

           //    }
           //}
           frs[0] = 0.0005;
           frs[1] = 0.005;
           frs[2] = 0.002;
           frs[3] = 0.001;
           executeGTC runalg = null;
           double fresult = -1;
           Console.Out.WriteLine("start");
           for (int i = 0; i < 1; i++)
           {
               ART_New art_ins = new ART_New((uint)(rtseed.Next()));
               if (salg == "rt")
               {
                   
                   fresult= art_ins.computeE(dim, failure_rates(dim, frs[i]), null, art_ins.computeFails_RT, 1);

               }
               else if (salg == "cos")
               {
                   //  fresult = art_ins.ARTs(dim, failure_rates(dim, frs[i]), null,art_ins.doARTCOS_NDs,noOftcs);
               }
               else if (salg == "x2")
               {
                 // fresult= art_ins.computeE(dim, failure_rates(dim, frs[i]), null, art_ins.computeFails_1D, 100);
                   //fresult=art_ins.computeE(dim, failure_rates(dim, frs[i]), null, art_ins.computeFails_Hilbert, 1000);

                   double x = 0.3;
                   uint xx = 0;
                   uint suncount = 0;
                   for (int k = 0; k < 10; k++)
                   {
                       uint count = art_ins.doARTX2_NDs(dim, failure_rates(dim, frs[i]), null, xx, ref x);
                       suncount += count;
                   }
                   Console.Out.WriteLine((double)suncount / 10.0);

                   //double fm=art_ins.ARTs(1, failure_rates(dim, frs[i]),null,null,0);
                   //Console.Out.WriteLine(fm);
               }
               else
               {
                   Console.WriteLine("算法名称错误，请输入'x2'");
                   break;
               }
              // Console.WriteLine("执行theta=" + frs[i].ToString() + ";E=" + fresult.ToString());
               //sheet.Cells[i + 1, 1] = frs[i];
               //sheet.Cells[i + 1, 2] = fresult;
               //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               art_ins = null;
           }
           // sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           //excel.Workbooks.Close();
           //excel.Quit();


       }
       /// <summary>
       /// 计算P-measure
       /// </summary>
       public void computePmeasure()
       {

           //Excel.Application excel;
           //excel = new Excel.Application();
           //Excel.Workbooks workbooks;
           //object miss = Missing.Value;
           //Excel.Sheets sheets;
           //Excel._Worksheet sheet = null;
           //string excelname = "result1.xls";
           //workbooks = excel.Workbooks;
           //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           //sheets = excel.Worksheets;
           //sheet = (Excel._Worksheet)sheets.get_Item(1);
           //excel.DisplayAlerts = false;
           Random rtseed = new Random();

           Console.WriteLine("输入空间维度：");
           string sdim = Console.ReadLine();
           uint dim = UInt32.Parse(sdim);
           Console.WriteLine("生成算法：(输入“x2”)");
           string salg = Console.ReadLine();
           // Console.WriteLine("自定义生成Testcase:");
           // uint noOftcs = UInt32.Parse(Console.ReadLine());
           uint noOftcs = 0;
           double init_val = 1.0;
           double[] frs = new double[4];

           //for (int i = 0; i < 10; i++)
           //{

           //    frs[i] =init_val*(1- 0.5 *(i%2));

           //    if ((i+1) % 2 == 0)
           //    {
           //        init_val = 0.1 * init_val;

           //    }
           //}
           frs[0] = 0.01;
           frs[1] = 0.005;
           frs[2] = 0.002;
           frs[3] = 0.001;
           executeGTC runalg = null;
           double fresult = -1;

           for (int i = 0; i < 4; i++)
           {
               ART_New art_ins = new ART_New((uint)(rtseed.Next()));
               if (salg == "rt")
               {

                   fresult = art_ins.computeP(dim, failure_rates(dim, frs[i]), null, art_ins.computeFails_RT, 1000);

               }
               else if (salg == "cos")
               {
                   //  fresult = art_ins.ARTs(dim, failure_rates(dim, frs[i]), null,art_ins.doARTCOS_NDs,noOftcs);
               }
               else if (salg == "x2")
               {
                  // fresult = art_ins.computeP(dim, failure_rates(dim, frs[i]), null, art_ins.computeFails_1D, 100);
                     fresult=art_ins.computeP(dim, failure_rates(dim, frs[i]), null, art_ins.computeFails_Hilbert, 1000);

               }
               else
               {
                   Console.WriteLine("算法名称错误，请输入'x2'");
                   break;
               }
               Console.WriteLine("执行theta=" + frs[i].ToString() + ";P=" + fresult.ToString());
               //sheet.Cells[i + 1, 1] = frs[i];
               //sheet.Cells[i + 1, 2] = fresult;
               //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               art_ins = null;
           }
           // sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           //excel.Workbooks.Close();
           //excel.Quit();


       }
       /// <summary>
       /// strip pattern
       /// </summary>
       public void doART_strip()
       {
           //Excel.Application excel;
           //excel = new Excel.Application();
           //Excel.Workbooks workbooks;
           //object miss = Missing.Value;
           //Excel.Sheets sheets;
           //Excel._Worksheet sheet = null;
           //string excelname = "resultStrip001.xls";
           //workbooks = excel.Workbooks;
           //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           //sheets = excel.Worksheets;
           //sheet = (Excel._Worksheet)sheets.get_Item(1);
           //excel.DisplayAlerts = false;
           Random rtseed = new Random();

           Console.WriteLine("输入空间维度：");
           string sdim = Console.ReadLine();
           uint dim = UInt32.Parse(sdim);
           Console.WriteLine("输入failurerate:");
           double fr = double.Parse(Console.ReadLine());
           Console.WriteLine("生成算法：");
           string salg = Console.ReadLine();
           Console.WriteLine("自定义生成Testcase:");
           uint noOftcs = UInt32.Parse(Console.ReadLine());
           double init_val = 1.0;
           double[] frs;
          
           frs = strip_pattern(fr, null);

           
           executeGTC runalg = null;
           double fresult = -1;

           for (int i = 0; i < frs.Length; i++)
           {
               ART_New art_ins = new ART_New((uint)(rtseed.Next()));
               if (salg == "rt")
               {
                   fresult = art_ins.ARTs(dim, failure_rates(dim,fr,frs[i]), null,art_ins.doRT_NDS,noOftcs);

               }
               else if (salg == "cos")
               {
                  // fresult = art_ins.ARTs(dim, failure_rates(dim, fr,frs[i]), null,art_ins.doARTCOS_NDs,noOftcs);
               }
               else if (salg == "x2")
               {
                  // fresult = art_ins.ARTs(dim, failure_rates(dim, fr,frs[i]), null,art_ins.doARTX2_NDs,noOftcs);
                  fresult = art_ins.ARTs(dim, failure_rates(dim, fr, frs[i]), null, art_ins.doARTX2_NDs_V1, noOftcs);
               }
               Console.WriteLine("执行theta=" + frs[i].ToString() + ";fcount=" + fresult.ToString());
               //sheet.Cells[i + 1, 1] = frs[i];
               //sheet.Cells[i + 1, 2] = fresult;
               //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               art_ins = null;
           }
           // sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           //excel.Workbooks.Close();
           //excel.Quit();

       }
       /// <summary>
       /// 用于计算discrepancy和dispersion
       /// </summary>
       public void doOneArt()
       {
           Random rtseed = new Random();
           double fresult = -1;
           Console.WriteLine("输入空间维度：");
           string sdim = Console.ReadLine();
           uint dim = UInt32.Parse(sdim);
           Console.WriteLine("输入failurerate:");
           double fr = double.Parse(Console.ReadLine());
           Console.WriteLine("生成算法：");
           string salg = Console.ReadLine();
           uint noOftcs = 0;
           //Console.WriteLine("自定义生成Testcase:");
           //noOftcs = UInt32.Parse(Console.ReadLine());
           ART_New art_ins = new ART_New((uint)(rtseed.Next()));
           uint[] arrOftcs = new uint[3];
           arrOftcs[0] = 100;
           arrOftcs[1] = 1000;
           arrOftcs[2] = 10000;
           for (int i = 0; i < 3; i++)
           {
               if (salg == "rt")
               {
                   fresult = art_ins.ARTs(dim, failure_rates(dim, fr), null, art_ins.doRT_NDS, arrOftcs[i]);

               }
               else if (salg == "cos")
               {
                  // fresult = art_ins.ARTs(dim, failure_rates(dim, fr), null, art_ins.doARTCOS_NDs, arrOftcs[i]);
               }
               else if (salg == "x2")
               {
                 fresult = art_ins.ARTs(dim, failure_rates(dim, fr), null, art_ins.doARTX2_NDs_V1, arrOftcs[i]);
                  //fresult = art_ins.ARTs(dim, failure_rates(dim, fr), null, art_ins.doARTX2_NDs, arrOftcs[i]);
               }
               //Console.WriteLine("执行noOftcs=" + arrOftcs[i].ToString() + ";fcount=" + fresult.ToString());
           }

       }
       
    }
}
