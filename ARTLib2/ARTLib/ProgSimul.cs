﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace ARTLib
{
   public class ProgSimul
    {
        public int dimension;
        public ArrayList range_list;//每一维度的范围

        public int input_size;//输入域的大小，即点的个数
        public float fail_rate;//失效率
        public ArrayList failregion_list;//失效域列表
        public ArrayList random_list;//每一维的随机序列列表
        private int failregion_num;
       private Random global_random;
        public ProgSimul(int dim, float fail_r,int[] rangeMax)
        {
            dimension = dim;
            fail_rate = fail_r;//-1表示real-life programs
            input_size=1;
            range_list = new ArrayList(dim);
            random_list = new ArrayList(dim);
            failregion_list = new ArrayList();
            for (int i = 0; i < dim; i++)
            {
                //range_list[i] = (object)rangeMax[i];
                range_list.Add(rangeMax[i]);
                input_size=input_size*(rangeMax[i]+1);
                random_list.Add(new Random(i + DateTime.Now.Second + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Millisecond));
            }
            failregion_num = 0;
            global_random = new Random(DateTime.Now.Second + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Millisecond);

        }
        /// <summary>
        /// 生成失效域
        /// equal为T表示各维的范围相同，若为二维，表示为平方；否则，表示为矩形或超矩形
        /// </summary>
        /// <param name="fail_r"></param>
        /// <param name="dim_rate"></param>
        /// <param name="equal"></param>
        public void GenerateFailregion(float fail_r, int[] dim_rate, bool equal)
        {
            int range = 0;
            FailRegion fr = new FailRegion();

            fr.Dimension = dimension;
            fr.Fail_rate = fail_r;
            fr.Size = (int)(input_size * fail_r);
            fr.range_list = new ArrayList(dimension);
            fr.starpos_list = new ArrayList(dimension);
            fr.dim_rates = new ArrayList(dimension);
            if (equal)
            {
                range = (int)(Math.Exp(Math.Log(fr.Size, Math.E) / dimension));
                for (int i = 0; i < dimension; i++)
                {
                   
                    fr.dim_rates.Add(1);//各维之间的比例
                }

            }
            else
            {
                int r = 1;
                for (int i = 0; i < dimension; i++)
                {
                    r *= dim_rate[i];
                    fr.dim_rates.Add(dim_rate[i]);
                }
                range = (int)(Math.Exp(Math.Log((((double)fr.Size) / r), Math.E) / dimension));

               // range =( int)(((double)fr.Size) / r);
            }
            for (int i = 0; i < dimension; i++)
            {
                if (equal)
                {
                    //fr.range_list[i] = range;
                    fr.range_list.Add(range);
                }
                else
                {
                   // fr.range_list[i] = range * dim_rate[i];
                    fr.range_list.Add( range * dim_rate[i]);
                }
            }
            failregion_list.Add(fr);
            failregion_num++;

        }
       /// <summary>
       /// 产生多个失效域，size_rates为失效域之间的大小之比
       /// </summary>
       /// <param name="size_rates"></param>
       public void generateMultiplefailregions(float[] size_rates)
       {
           
           float size=0;
           for (int i = 0; i < size_rates.Length; i++)
           {
               size += size_rates[i];
           }
           for (int i = 0; i <size_rates.Length; i++)
           {
               GenerateFailregion(size_rates[i]/ size*fail_rate, null, true);
           }

       }
       /// <summary>
       /// 随机定位一个失效域，有个缺陷，失效域的方向还没考虑清楚
       /// </summary>
       /// <param name="fr"></param>
       public void randomLocation(FailRegion fr)
        {
            Random r = new Random((DateTime.Now.Second + DateTime.Now.Second + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Millisecond + global_random.Next(0, 10000)));
           fr.starpos_list.Clear();
            for (int i = 0; i < dimension; i++)
            {
                //fr.starpos_list[i] = r.Next(0, (int)range_list[i] - (int)fr.range_list[i]);//在失效域的范围<=输入域范围的情况下是成立的
               fr.starpos_list.Add(r.Next(0, (int)range_list[i] - (int)fr.range_list[i]));
               //if (i == 0)
               //{
               //    fr.starpos_list.Add(30);
               //}
               //else
               //{
               //    fr.starpos_list.Add(25);
               //}
            }
            fr.startp = new Input(dimension);
            for (int i = 0; i < dimension; i++)
            {
                fr.startp.item_list.Add(fr.starpos_list[i]);
            }

        }
        /// <summary>
        /// 随机生成一个输入
        /// </summary>
        /// <returns></returns>
        public Input randomInput()
        {
           
            Input input_exp = new Input(dimension);
            for (int i = 0; i < dimension; i++)
            {
                input_exp.item_list.Add(((Random)random_list[i]).Next(0, (int)range_list[i]));
            }
            return input_exp;
        }
        /// <summary>
        /// 判断测试输入是否失效
        /// </summary>
        /// <param name="testcase"></param>
        /// <returns></returns>
        public bool Failinput(Input testcase)
        {
            int i = 0;
            bool fail = false;
            foreach (FailRegion fr in failregion_list)
            {
                for (i = 0; i < dimension; i++)
                {
                    if (((int)testcase.item_list[i]<(int)fr.starpos_list[i])||((int)testcase.item_list[i] - (int)fr.starpos_list[i]) > (int)fr.range_list[i])
                    {
                        break;
                    }
                }
                if (i == dimension)
                {
                    fail = true;
                    break;
                }
            }
            return fail;
        }
        /// <summary>
        /// 判断t2是否在t1的range/2范围内，以t1为中心所定义的相近距离
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public bool IsAdjacent(Input t1, Input t2, int range)
        {
            bool close = false;
            int i = 0;
            for (i = 0; i < dimension; i++)
            {
                if (Math.Abs((int)t1.item_list[i] - (int)t2.item_list[i]) * 2 > range)
                {
                    break;
                }
            }
            if (i == dimension)
                close = true;
            return close;
        }
        /// <summary>
        /// 重置随机序列
        /// </summary>
        public void resetRandom()
        {
            if (random_list != null)
            {
                random_list.Clear();
                for (int i = 0; i < dimension; i++)
                {
                    random_list.Add(new Random(i + DateTime.Now.Second+DateTime.Now.Hour+DateTime.Now.Minute+DateTime.Now.Millisecond+global_random.Next(0,10000)));
                }
            }
        }
    }
}
