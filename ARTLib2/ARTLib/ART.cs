﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using Microsoft.Office;
using RNG;

using System.Reflection;
namespace ARTLib
{
   public class ART
    {
        private string currentPath;
        private const string filename = "testinfo.txt";
       private string excelname = "testcase.xls";
       
        public ART()
        {
            currentPath = Directory.GetCurrentDirectory();
            if (File.Exists(currentPath + @"\" + filename))
            {
                File.Delete(currentPath + @"\" + filename);
            }
            //File.Create(currentPath + @"\" + filename);

        }
       public void excelApp()
       {
          
           
       }
       /// <summary>
       /// RT算法
       /// </summary>
       /// <param name="prog"></param>
       /// <param name="fail_rate"></param>
       /// <param name="Max"></param>
       public void RT(ProgSimul prog, float fail_rate, int Max)
       {
           int s = 0;
           Input testcase = null;
           Input next_testcase = null;
           int fcount = 0;
           bool reveal = false;
           ArrayList testcases = new ArrayList();
           ArrayList fcount_list = new ArrayList();
          // ArrayList count_list = new ArrayList();
           // ArrayList r_testcases = new ArrayList();//实际的随机序列
           int F_measure = -1;

           int xM = (int)prog.range_list[0] + 1;
           int yM = (int)prog.range_list[1] + 1;
           float[,] freq = new float[xM, yM];//针对二维输入

           

           double s_mean = 0;
           double s_variance = 0;
           int Times = Max;

           Excel.Application excel = new Excel.Application();
           Excel.Workbooks workbooks = excel.Workbooks;
           object miss = Missing.Value;

           workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           Excel.Sheets sheets = excel.Worksheets;
           Excel._Worksheet sheet = (Excel._Worksheet)sheets.get_Item(1);
           excel.DisplayAlerts = false;
           //int cols = 1;
           //int rows = 1;
           //int sheetcnt = 1;


           for (int i = 0; i < xM; i++)
           {
               for (int j = 0; j < yM; j++)
               {
                   freq[i, j] = 0;
               }
           }
           int cur_x, cur_y;

           FileStream file_start = new FileStream(currentPath + @"\" + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
           StreamWriter sw_start = new StreamWriter(file_start);
           sw_start.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "开始RT实验：θ=" + prog.fail_rate.ToString() + "\r\n输入域范围为");
           for (int i = 0; i < prog.dimension; i++)
           {
               sw_start.Write(((int)prog.range_list[i]).ToString() + " ");
           }
           sw_start.Close();
           file_start.Close();

           FileStream file_loc;
           StreamWriter sw_loc;
           foreach (FailRegion fr in prog.failregion_list)
           {
               prog.randomLocation(fr);
               //if (n % 10 == 0)
               //    sw_loc.Write("\r\n");
               //sw_loc.Write(n.ToString() + "-" + fr.startp.ToString() + " 各维比率为" + fr.dimRatesTostring() + " ");
               //n++;
           }
           while (s < Times && s <2000)//重复Max次试验
           {


               file_loc = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);


               sw_loc = new StreamWriter(file_loc);
               sw_loc.Write("\r\n第" + s.ToString() + "次实验\r\n失效域位置：\r\n");

               int n = 1;
               foreach (FailRegion fr in prog.failregion_list)
               {
                   //prog.randomLocation(fr);
                   if (n % 10 == 0)
                       sw_loc.Write("\r\n");
                   sw_loc.Write(n.ToString() + "-" + fr.startp.ToString() + " 各维比率为" + fr.dimRatesTostring() + " ");
                   n++;
               }
               // sw_loc.Write("\r\n随机序列：\r\n");
               sw_loc.Close();
               file_loc.Close();

               //将测试结果序列写入文件
               FileStream file_local = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.Append, FileAccess.Write);
               StreamWriter sw = new StreamWriter(file_local);
               int curcnt = 0;

               //for (int i = 0; i < r_testcases.Count; i++)
               //{
               //    sw.Write(((Input)r_testcases[i]).ToString());
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
               //}
               //sw.Write("\r\n随机步数序列：\r\n");
               //for (int i = 0; i < fcount; i++)
               //{

               //    sw.Write(((int)count_list[i]).ToString() + " ");
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
               //}
               sw.Write("\r\n测试用例序列：\r\n");

               fcount = 0;
               reveal = false;
               testcase = prog.randomInput();
               //count_list.Add(1);
               while (!reveal)//可能要加一个时间限制
               {
                   //if (r_testcases.Count == 10)
                   //{
                   //    FileStream file_input0 = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
                   //    StreamWriter sw_input0 = new StreamWriter(file_input0);
                   //    for (int i = 0; i < r_testcases.Count; i++)
                   //    {
                   //        sw_input0.Write(((Input)r_testcases[i]).ToString()+" ");
                   //    }
                   //    sw_input0.Write("\r\n");
                   //    sw_input0.Close();
                   //    file_input0.Close();
                   //    r_testcases.Clear();

                   //}
                   fcount++;

                   testcases.Add(testcase);
                   cur_x = (int)testcase.item_list[0];
                   cur_y = (int)testcase.item_list[1];
                   freq[cur_x, cur_y] = freq[cur_x, cur_y] + 1;
                  // freq[cur_x, cur_y]++;

                   curcnt = testcases.Count;
                   if (curcnt == 100)
                   {
                       for (int i = 0; i < curcnt; i++)
                       {
                           sw.Write(((Input)testcases[i]).ToString());
                           if ((i + 1) % 10 == 0)
                               sw.Write("\r\n");
                           //sheet.Cells[rows, cols] = ((Input)testcases[i]).item_list[0].ToString();
                           //sheet.Cells[rows, cols + 1] = ((Input)testcases[i]).item_list[1].ToString();
                           //rows++;
                       }
                       testcases.Clear();
                       curcnt = 0;
                   }

                   // r_testcases.Add(testcase);
                   reveal = prog.Failinput(testcase);
                   if (!reveal)
                   {
                       next_testcase = prog.randomInput();
                       testcase = next_testcase;
                      


                   }

               }
               //完成一次run，记录F值
               fcount_list.Add(fcount);
               
               //rows = 1;
               
               //cols = cols + 2;
               //if (cols > 242)
               //{
               //    sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               //    if (sheets.Count == sheetcnt)
               //    {
               //        sheets.Add(miss, miss, miss, miss);

               //    }
               //        sheetcnt++;
               //        cols = 1;
               //        sheet = (Excel._Worksheet)sheets.get_Item("Sheet"+sheetcnt.ToString());

               //        sheet.Activate();
               //        Console.WriteLine("工作表个数" + sheets.Count.ToString() + ";当前工作表：" + sheet.Name);


               //}
               for (int i = 0; i < curcnt; i++)
               {
                   sw.Write(((Input)testcases[i]).ToString());
                   if ((i + 1) % 10 == 0)
                       sw.Write("\r\n");
                   //sheet.Cells[rows, cols] = ((Input)testcases[i]).item_list[0].ToString();
                   //sheet.Cells[rows, cols + 1] = ((Input)testcases[i]).item_list[1].ToString();
                   //rows++;
               }
               if (curcnt > 0)
               {
                   testcases.Clear();
                   curcnt = 0;
               }
               sw.Write("\r\nF值：" + fcount.ToString()+"\r\n");
               sw.Close();
               file_local.Close();
               s++;
               
               //r_testcases.Clear();
               //count_list.Clear();
               prog.resetRandom();

               //计算实验次数
               if (s == 1) continue;
               s_mean = 0;
               s_variance = 0;
               for (int i = 0; i < s; i++)
               {
                   s_mean += ((int)fcount_list[i]);
               }
               s_mean = s_mean / s;
               for (int i = 0; i < s; i++)
               {
                   s_variance += (double.Parse(((int)fcount_list[i]).ToString()) - s_mean) * (double.Parse(((int)fcount_list[i]).ToString()) - s_mean);
               }
               Times = (int)Math.Ceiling((1536.64 * s_variance / (s_mean * s_mean)));//Times=((196/5)^2)*s_variance/(s_mean^2)

               //if ((s >= 1000 || s > Times)&&cols>1)//最后一次且还有未写入excel的testcase
               //{

               //    sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               //    Console.WriteLine("工作表个数" + sheets.Count.ToString() + ";当前工作表：" + sheet.Name);
               //}

           }
           int sum = 0;
           for (int i = 0; i < s; i++)
           {
               sum += ((int)fcount_list[i]);

           }
           F_measure = sum / s;
           FileStream file_end = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
           StreamWriter sw_end = new StreamWriter(file_end);
           sw_end.Write("\r\n平均的F度量值为" + F_measure.ToString() + "\r\n实验次数为" + s.ToString());
           sw_end.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "结束RT实验\r\n");
           sw_end.Close();
           file_end.Close();

           float x_count = 0;
           float[] y_count = new float[yM];
           for (int i = 0; i < yM; i++)
           {
               y_count[i] = 0;
           }
           for (int i = 0; i < xM; i++)
           {
               sheet.Cells[1, i + 2] = i;
               sheet.Cells[i + 2, 1] = i;
           }
           for (int i = 0; i < xM; i++)
           {
               x_count = 0;
               for (int j = 0; j < yM; j++)
               {
                   sheet.Cells[i + 2, j + 2] = freq[i, j]/s;
                   x_count += freq[i, j];
                   y_count[j] += freq[i, j];
               }
               sheet.Cells[i + 2, yM + 3] = i;
               sheet.Cells[i + 2, yM + 4] = x_count / s;
           }
           for (int i = 0; i < yM; i++)
           {
               sheet.Cells[i + 2, yM + 5] = i;
               sheet.Cells[i + 2, yM + 6] = y_count[i] / s;
           }
           sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           excel.Workbooks.Close();
           // excel.Quit();
           System.GC.Collect();

       }
       /// <summary>
       /// RRT算法
       /// </summary>
       /// <param name="R"></param>
       /// <param name="prog"></param>
       /// <param name="fail_rate"></param>
       /// <param name="Max"></param>
        public void RRT(float R, ProgSimul prog,float fail_rate, int Max)
        {
            int s = 0;
            Input testcase = null;
            Input next_testcase = null;
            int fcount=0;
            bool reveal = false;
            ArrayList testcases = new ArrayList();
            ArrayList fcount_list = new ArrayList();
            ArrayList count_list = new ArrayList();
           // ArrayList r_testcases = new ArrayList();//实际的随机序列
            double F_measure = -1;

            int xM=(int)prog.range_list[0]+1;
            int yM=(int)prog.range_list[1]+1;
            float[,] freq = new float[xM,yM];//针对二维输入
           
            int exclusion = -1;
            int range = -1;//每一维可取的值个数
            bool next_find = false;
            bool far = true;
            int local_tries = 0;

            double s_mean=0;
            double s_variance=0;
            int Times = Max;

            //Excel.Application excel = new Excel.Application();
            //Excel.Workbooks workbooks= excel.Workbooks;
            //object miss=Missing.Value;
            
            //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
            //Excel.Sheets sheets = excel.Worksheets;
            //Excel._Worksheet sheet =(Excel._Worksheet) sheets.get_Item(1);
            //excel.DisplayAlerts = false;
            //int cols = 1;
            //int rows = 1;
            //int sheetcnt = 1;


            //for (int i = 0; i < xM; i++)
            //{
            //    for (int j = 0; j < yM; j++)
            //    {
            //        freq[i,j] = 0;
            //    }
            //}
            int cur_x, cur_y;

            //FileStream file_start = new FileStream(currentPath + @"\"+ filename, FileMode.OpenOrCreate,FileAccess.ReadWrite);
            //StreamWriter sw_start = new StreamWriter(file_start);
            //sw_start.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "开始RRT实验：θ=" + prog.fail_rate.ToString() + "\r\n输入域范围为");
            //for (int i = 0; i < prog.dimension; i++)
            //{
            //    sw_start.Write(((int)prog.range_list[i]).ToString() + " ");
            //}
            //sw_start.Close();
            //file_start.Close();

            FileStream file_loc;
            StreamWriter sw_loc;

            
            //foreach (FailRegion fr in prog.failregion_list)
            //{
            //    prog.randomLocation(fr);
            //    //if (n % 10 == 0)
            //    //    sw_loc.Write("\r\n");
            //    //sw_loc.Write(n.ToString() + "-" + fr.startp.ToString() + " 各维比率为" + fr.dimRatesTostring() + " ");
            //    //n++;
            //}
            while (s <Times&&s<2000)//重复Max次试验
            {
                
                
                file_loc = new FileStream(currentPath + @"\" +"n"+s.ToString()+filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
               
                
                sw_loc = new StreamWriter(file_loc);
                sw_loc.Write("\r\n第"+s.ToString()+"次实验\r\n失效域位置：\r\n");
            
                int n=1;
            foreach (FailRegion fr in prog.failregion_list)
            {
                prog.randomLocation(fr);
                //if (n % 10 == 0)
                //    sw_loc.Write("\r\n");
                //sw_loc.Write(n.ToString() + "-" + fr.startp.ToString() + " 各维比率为"+fr.dimRatesTostring()+" ");
                //n++;
            }
           // sw_loc.Write("\r\n随机序列：\r\n");
            //sw_loc.Close();
            //file_loc.Close();
 
                fcount = 0;
                reveal = false;
                testcase = prog.randomInput();
                //count_list.Add(1);
                while (!reveal)//可能要加一个时间限制
                {
                    //if (r_testcases.Count == 10)
                    //{
                    //    FileStream file_input0 = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
                    //    StreamWriter sw_input0 = new StreamWriter(file_input0);
                    //    for (int i = 0; i < r_testcases.Count; i++)
                    //    {
                    //        sw_input0.Write(((Input)r_testcases[i]).ToString()+" ");
                    //    }
                    //    sw_input0.Write("\r\n");
                    //    sw_input0.Close();
                    //    file_input0.Close();
                    //    r_testcases.Clear();

                    //}
                    fcount++;

                    testcases.Add(testcase);
                    //cur_x = (int)testcase.item_list[0];
                    //cur_y = (int)testcase.item_list[1];
                    //freq[cur_x,cur_y]++;
                   // freq[cur_x, cur_y] = freq[cur_x, cur_y] + 1;
                   // r_testcases.Add(testcase);
                    reveal = prog.Failinput(testcase);
                    if (!reveal)
                    {
                        exclusion =(int)( R * prog.input_size / fcount);
                        range=(int)(Math.Exp(Math.Log(exclusion, Math.E) / prog.dimension));//每一维可取的值个数
                        next_find = false;
                        local_tries = 0;
                        while (!next_find)//寻找下一个test case
                        {
                            next_testcase = prog.randomInput();
                           
                            local_tries++;
                            far = true;
                            foreach (Input t in testcases)
                            {
                                if (prog.IsAdjacent(t, next_testcase,range))
                                {
                                    far = false;
                                    break;
                                }
                            }
                            if (far)
                            {
                                next_find = true;
                                testcase = next_testcase;
                            }
                            else
                            {
                                //if (r_testcases.Count == 10)
                                //{
                                //    FileStream file_input = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
                                //    StreamWriter sw_input = new StreamWriter(file_input);
                                //    for (int i = 0; i < r_testcases.Count; i++)
                                //    {
                                //        sw_input.Write(((Input)r_testcases[i]).ToString() + " ");
                                //    }
                                //    sw_input.Write("\r\n");
                                //    sw_input.Close();
                                //    file_input.Close();
                                //    r_testcases.Clear();

                                //}
                               // r_testcases.Add(next_testcase);
                                //if (local_tries == 50)//控制runtime
                                //{
                                //    range--;
                                //}
                            }
                            

                        }
                        fcount += (local_tries - 1);//将排除点计数
                       // count_list.Add(local_tries);//表示找到每一个测试用例随机进行的次数


                    }

                }
                //完成一次run，记录F值
                fcount_list.Add(fcount);
                //将测试结果序列写入文件
                //FileStream file_local = new FileStream(currentPath + @"\" +"n"+s.ToString()+filename, FileMode.Append, FileAccess.Write);
                //StreamWriter sw= new StreamWriter(file_local);
                
                //for (int i = 0; i < r_testcases.Count; i++)
                //{
                //    sw.Write(((Input)r_testcases[i]).ToString());
                //    if ((i + 1) % 10 == 0)
                //        sw.Write("\r\n");
                //}
                //sw.Write("\r\n随机步数序列：\r\n");
                //for (int i = 0; i < fcount; i++)
                //{

                //    sw.Write(((int)count_list[i]).ToString()+" ");
                //    if ((i + 1) % 10 == 0)
                //        sw.Write("\r\n");
                //}
                //sw.Write("\r\nF值：" + fcount.ToString()+"\r\n测试用例序列：\r\n");
                //rows = 1;
                //for (int i = 0; i < fcount; i++)
                //{
                //    sw.Write(((Input)testcases[i]).ToString());
                //    if ((i + 1) % 10 == 0)
                //        sw.Write("\r\n");
                //    //sheet.Cells[rows, cols] = ((Input)testcases[i]).item_list[0].ToString();
                //    //sheet.Cells[rows, cols + 1] = ((Input)testcases[i]).item_list[1].ToString();
                //    //rows++;
                //}
                //cols = cols + 2;
                //if (cols > 242)
                //{
                //    sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
                //    if (sheets.Count == sheetcnt)
                //    {
                //        sheets.Add(miss, miss, miss, miss);
                       
                //    }
                //        sheetcnt++;
                //        cols = 1;
                //        sheet = (Excel._Worksheet)sheets.get_Item("Sheet"+sheetcnt.ToString());
                    
                //        sheet.Activate();
                //        Console.WriteLine("工作表个数" + sheets.Count.ToString() + ";当前工作表：" + sheet.Name);
                   
                   
                //}
                
                //sw.Close();
                //file_local.Close();
                s++;
                testcases.Clear();
                //r_testcases.Clear();
               // count_list.Clear();
                prog.resetRandom();

                //计算实验次数
                if (s == 1) continue;
                s_mean = 0;
                s_variance = 0;
                for (int i = 0; i < s; i++)
                {
                    s_mean += ((int)fcount_list[i]);
                }
                s_mean = s_mean / s;
                for (int i = 0; i < s; i++)
                {
                    s_variance += (double.Parse(((int)fcount_list[i]).ToString()) - s_mean) * (double.Parse(((int)fcount_list[i]).ToString() )- s_mean);
                }
                Times =(int)Math.Ceiling((1536.64 * s_variance / (s_mean * s_mean)));//Times=((196/5)^2)*s_variance/(s_mean^2)

                //if ((s >= 1000 || s > Times)&&cols>1)//最后一次且还有未写入excel的testcase
                //{

                //    sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
                //    Console.WriteLine("工作表个数" + sheets.Count.ToString() + ";当前工作表：" + sheet.Name);
                //}

            }
            int sum = 0;
            for (int i = 0; i < s; i++)
            {
                sum += ((int)fcount_list[i]);

            }
            F_measure =double.Parse(sum.ToString()) / s;
            Console.WriteLine("theta="+fail_rate.ToString()+";fcount=" + F_measure.ToString() + ";实验次数为" + s.ToString());
            //FileStream file_end = new FileStream(currentPath + @"\"+filename, FileMode.Append, FileAccess.Write);
            //StreamWriter sw_end = new StreamWriter(file_end);
            //sw_end.Write("\r\n平均的F度量值为" + F_measure.ToString() + "\r\n实验次数为"+s.ToString());
            //sw_end.Write("\r\n" +DateTime.Now.ToShortDateString()+" " +DateTime.Now.ToLongTimeString() + "结束RRT实验\r\n");
            //sw_end.Close();
            //file_end.Close();

            //float x_count = 0;
            //float[] y_count = new float[yM];
            //for (int i = 0; i < yM; i++)
            //{
            //    y_count[i] = 0;
            //}
           
            //for (int i = 0; i < xM; i++)
            //{
            //    sheet.Cells[1, i + 2] = i;
            //    sheet.Cells[i + 2, 1] = i;
            //}
            //for (int i = 0; i < xM; i++)
            //{
            //    x_count = 0;
            //    for (int j = 0; j < yM; j++)
            //    {
            //        sheet.Cells[i + 2, j + 2] = freq[i,j]/s;
            //        x_count += freq[i, j];
            //        y_count[j] += freq[i, j];
            //    }
            //    sheet.Cells[i + 2, yM + 3] = i;
            //    sheet.Cells[i + 2, yM + 4] = x_count / s;
            //}
            //for (int i = 0; i < yM; i++)
            //{
            //    sheet.Cells[i + 2, yM + 5] = i;
            //    sheet.Cells[i + 2, yM + 6 ]= y_count[i] /s;
            //}
            //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           
            //excel.Workbooks.Close();
            // excel.Quit();
            System.GC.Collect();
           


        }
       public void RTforRealProg(ProgSimul prog,int Max,FaultyProgram fp)
       {
           int s = 0;
           Input testcase = null;
           Input next_testcase = null;
           int fcount = 0;
           bool reveal = false;
           ArrayList testcases = new ArrayList();
           ArrayList fcount_list = new ArrayList();
           // ArrayList count_list = new ArrayList();
           // ArrayList r_testcases = new ArrayList();//实际的随机序列
           int F_measure = -1;

           int xM = (int)prog.range_list[0] + 1;
           int yM = (int)prog.range_list[1] + 1;
           float[,] freq = new float[xM, yM];//针对二维输入

           

           double s_mean = 0;
           double s_variance = 0;
           int Times = Max;

           Excel.Application excel = new Excel.Application();
           Excel.Workbooks workbooks = excel.Workbooks;
           object miss = Missing.Value;

           workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           Excel.Sheets sheets = excel.Worksheets;
           Excel._Worksheet sheet = (Excel._Worksheet)sheets.get_Item(1);
           excel.DisplayAlerts = false;
           //int cols = 1;
           //int rows = 1;
           //int sheetcnt = 1;


           for (int i = 0; i < xM; i++)
           {
               for (int j = 0; j < yM; j++)
               {
                   freq[i, j] = 0;
               }
           }
           int cur_x, cur_y;

           FileStream file_start = new FileStream(currentPath + @"\" + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
           StreamWriter sw_start = new StreamWriter(file_start);
           sw_start.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "开始RT实验：失效类型=" + fp.PatternType.ToString() + "\r\n输入域范围为");
           for (int i = 0; i < prog.dimension; i++)
           {
               sw_start.Write(((int)prog.range_list[i]).ToString() + " ");
           }
           sw_start.Close();
           file_start.Close();

           FileStream file_loc;
           StreamWriter sw_loc;
          
           while (s < Times && s < 5000)//重复Max次试验
           {


               file_loc = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);


               sw_loc = new StreamWriter(file_loc);
               sw_loc.Write("\r\n第" + s.ToString() + "次实验\r\n");

               
               sw_loc.Close();
               file_loc.Close();

               //将测试结果序列写入文件
               FileStream file_local = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.Append, FileAccess.Write);
               StreamWriter sw = new StreamWriter(file_local);
               int curcnt = 0;

               //for (int i = 0; i < r_testcases.Count; i++)
               //{
               //    sw.Write(((Input)r_testcases[i]).ToString());
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
               //}
               //sw.Write("\r\n随机步数序列：\r\n");
               //for (int i = 0; i < fcount; i++)
               //{

               //    sw.Write(((int)count_list[i]).ToString() + " ");
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
               //}
               sw.Write("\r\n测试用例序列：\r\n");

               fcount = 0;
               reveal = false;
               testcase = prog.randomInput();
               //count_list.Add(1);
               while (!reveal)//可能要加一个时间限制
               {
                   //if (r_testcases.Count == 10)
                   //{
                   //    FileStream file_input0 = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
                   //    StreamWriter sw_input0 = new StreamWriter(file_input0);
                   //    for (int i = 0; i < r_testcases.Count; i++)
                   //    {
                   //        sw_input0.Write(((Input)r_testcases[i]).ToString()+" ");
                   //    }
                   //    sw_input0.Write("\r\n");
                   //    sw_input0.Close();
                   //    file_input0.Close();
                   //    r_testcases.Clear();

                   //}
                   fcount++;

                   testcases.Add(testcase);
                   cur_x = (int)testcase.item_list[0];
                   cur_y = (int)testcase.item_list[1];
                   freq[cur_x, cur_y] = freq[cur_x, cur_y] + 1;
                   // freq[cur_x, cur_y]++;

                   curcnt = testcases.Count;
                   if (curcnt == 100)
                   {
                       for (int i = 0; i < curcnt; i++)
                       {
                           sw.Write(((Input)testcases[i]).ToString());
                           if ((i + 1) % 10 == 0)
                               sw.Write("\r\n");
                           //sheet.Cells[rows, cols] = ((Input)testcases[i]).item_list[0].ToString();
                           //sheet.Cells[rows, cols + 1] = ((Input)testcases[i]).item_list[1].ToString();
                           //rows++;
                       }
                       testcases.Clear();
                       curcnt = 0;
                   }

                   // r_testcases.Add(testcase);
                   reveal = !(fp.callProg(cur_x, cur_y));
                   if (!reveal)
                   {
                       next_testcase = prog.randomInput();
                       testcase = next_testcase;



                   }

               }
               //完成一次run，记录F值
               fcount_list.Add(fcount);

               //rows = 1;

               //cols = cols + 2;
               //if (cols > 242)
               //{
               //    sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               //    if (sheets.Count == sheetcnt)
               //    {
               //        sheets.Add(miss, miss, miss, miss);

               //    }
               //        sheetcnt++;
               //        cols = 1;
               //        sheet = (Excel._Worksheet)sheets.get_Item("Sheet"+sheetcnt.ToString());

               //        sheet.Activate();
               //        Console.WriteLine("工作表个数" + sheets.Count.ToString() + ";当前工作表：" + sheet.Name);


               //}
               for (int i = 0; i < curcnt; i++)
               {
                   sw.Write(((Input)testcases[i]).ToString());
                   if ((i + 1) % 10 == 0)
                       sw.Write("\r\n");
                   //sheet.Cells[rows, cols] = ((Input)testcases[i]).item_list[0].ToString();
                   //sheet.Cells[rows, cols + 1] = ((Input)testcases[i]).item_list[1].ToString();
                   //rows++;
               }
               if (curcnt > 0)
               {
                   testcases.Clear();
                   curcnt = 0;
               }
               sw.Write("\r\nF值：" + fcount.ToString() + "\r\n");
               sw.Close();
               file_local.Close();
               s++;

               //r_testcases.Clear();
               //count_list.Clear();
               prog.resetRandom();

               //计算实验次数
               if (s == 1) continue;
               s_mean = 0;
               s_variance = 0;
               for (int i = 0; i < s; i++)
               {
                   s_mean += ((int)fcount_list[i]);
               }
               s_mean = s_mean / s;
               for (int i = 0; i < s; i++)
               {
                   s_variance += (double.Parse(((int)fcount_list[i]).ToString()) - s_mean) * (double.Parse(((int)fcount_list[i]).ToString()) - s_mean);
               }
               Times = (int)Math.Ceiling((1536.64 * s_variance / (s_mean * s_mean)));//Times=((196/5)^2)*s_variance/(s_mean^2)

               //if ((s >= 1000 || s > Times)&&cols>1)//最后一次且还有未写入excel的testcase
               //{

               //    sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               //    Console.WriteLine("工作表个数" + sheets.Count.ToString() + ";当前工作表：" + sheet.Name);
               //}

           }
           int sum = 0;
           for (int i = 0; i < s; i++)
           {
               sum += ((int)fcount_list[i]);

           }
           F_measure = sum / s;
           FileStream file_end = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
           StreamWriter sw_end = new StreamWriter(file_end);
           sw_end.Write("\r\n平均的F度量值为" + F_measure.ToString() + "\r\n实验次数为" + s.ToString());
           sw_end.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "结束RT实验\r\n");
           sw_end.Close();
           file_end.Close();

           float x_count = 0;
           float[] y_count = new float[yM];
           for (int i = 0; i < yM; i++)
           {
               y_count[i] = 0;
           }
           for (int i = 0; i < xM; i++)
           {
               sheet.Cells[1, i + 2] = i;
               sheet.Cells[i + 2, 1] = i;
           }
           for (int i = 0; i < xM; i++)
           {
               x_count = 0;
               for (int j = 0; j < yM; j++)
               {
                   sheet.Cells[i + 2, j + 2] = freq[i, j] / s;
                   x_count += freq[i, j];
                   y_count[j] += freq[i, j];
               }
               sheet.Cells[i + 2, yM + 3] = i;
               sheet.Cells[i + 2, yM + 4] = x_count / s;
           }
           for (int i = 0; i < yM; i++)
           {
               sheet.Cells[i + 2, yM + 5] = i;
               sheet.Cells[i + 2, yM + 6] = y_count[i] / s;
           }
           sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           excel.Workbooks.Close();
           // excel.Quit();
           System.GC.Collect();

       }
       public void RTSequence(ProgSimul prog, int Max, FaultyProgram fp)
       {
           int s = 0;
           Input testcase = null;
           Input next_testcase = null;
           int fcount = 0;
           bool reveal = false;
           ArrayList testcases = new ArrayList();
           ArrayList fcount_list = new ArrayList();
          
           int F_measure = -1;

           int xM = (int)prog.range_list[0] + 1;
           int yM = (int)prog.range_list[1] + 1;
           float[,] freq = new float[xM, yM];//针对二维输入



           double s_mean = 0;
           double s_variance = 0;
           int Times = Max;

           Excel.Application excel = new Excel.Application();
           Excel.Workbooks workbooks = excel.Workbooks;
           object miss = Missing.Value;

           workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           Excel.Sheets sheets = excel.Worksheets;
           Excel._Worksheet sheet = (Excel._Worksheet)sheets.get_Item(1);
           excel.DisplayAlerts = false;
           //int cols = 1;
           //int rows = 1;
           //int sheetcnt = 1;


           for (int i = 0; i < xM; i++)
           {
               for (int j = 0; j < yM; j++)
               {
                   freq[i, j] = 0;
               }
           }
           int cur_x, cur_y;

           FileStream file_start = new FileStream(currentPath + @"\" + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
           StreamWriter sw_start = new StreamWriter(file_start);
           sw_start.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "开始RT实验：失效类型=" + fp.PatternType.ToString() + "\r\n输入域范围为");
           for (int i = 0; i < prog.dimension; i++)
           {
               sw_start.Write(((int)prog.range_list[i]).ToString() + " ");
           }
           sw_start.Close();
           file_start.Close();

           FileStream file_loc;
           StreamWriter sw_loc;

           while (s <1)//重复Max次试验
           {


               file_loc = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);


               sw_loc = new StreamWriter(file_loc);
               sw_loc.Write("\r\n第" + s.ToString() + "次实验\r\n");


               sw_loc.Close();
               file_loc.Close();

               //将测试结果序列写入文件
               FileStream file_local = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.Append, FileAccess.Write);
               StreamWriter sw = new StreamWriter(file_local);
               int curcnt = 0;

               //for (int i = 0; i < r_testcases.Count; i++)
               //{
               //    sw.Write(((Input)r_testcases[i]).ToString());
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
               //}
               //sw.Write("\r\n随机步数序列：\r\n");
               //for (int i = 0; i < fcount; i++)
               //{

               //    sw.Write(((int)count_list[i]).ToString() + " ");
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
               //}
               sw.Write("\r\n测试用例序列：\r\n");

               fcount = 0;
               reveal = false;
               testcase = prog.randomInput();
               //count_list.Add(1);
               while (fcount<1500000)//可能要加一个时间限制
               {
                   //if (r_testcases.Count == 10)
                   //{
                   //    FileStream file_input0 = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
                   //    StreamWriter sw_input0 = new StreamWriter(file_input0);
                   //    for (int i = 0; i < r_testcases.Count; i++)
                   //    {
                   //        sw_input0.Write(((Input)r_testcases[i]).ToString()+" ");
                   //    }
                   //    sw_input0.Write("\r\n");
                   //    sw_input0.Close();
                   //    file_input0.Close();
                   //    r_testcases.Clear();

                   //}
                   fcount++;

                   testcases.Add(testcase);
                   cur_x = (int)testcase.item_list[0];
                   cur_y = (int)testcase.item_list[1];
                   freq[cur_x, cur_y] = freq[cur_x, cur_y] + 1;
                   // freq[cur_x, cur_y]++;

                   curcnt = testcases.Count;
                   if (curcnt == 100)
                   {
                       for (int i = 0; i < curcnt; i++)
                       {
                           sw.Write(((Input)testcases[i]).ToString());
                           if ((i + 1) % 10 == 0)
                               sw.Write("\r\n");
                           //sheet.Cells[rows, cols] = ((Input)testcases[i]).item_list[0].ToString();
                           //sheet.Cells[rows, cols + 1] = ((Input)testcases[i]).item_list[1].ToString();
                           //rows++;
                       }
                       testcases.Clear();
                       curcnt = 0;
                   }

                   // r_testcases.Add(testcase);
                   reveal = !(fp.callProg(cur_x, cur_y));
                   if (fcount<1500000)
                   {
                       next_testcase = prog.randomInput();
                       testcase = next_testcase;



                   }

               }
               //完成一次run，记录F值
               fcount_list.Add(fcount);

               //rows = 1;

               //cols = cols + 2;
               //if (cols > 242)
               //{
               //    sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               //    if (sheets.Count == sheetcnt)
               //    {
               //        sheets.Add(miss, miss, miss, miss);

               //    }
               //        sheetcnt++;
               //        cols = 1;
               //        sheet = (Excel._Worksheet)sheets.get_Item("Sheet"+sheetcnt.ToString());

               //        sheet.Activate();
               //        Console.WriteLine("工作表个数" + sheets.Count.ToString() + ";当前工作表：" + sheet.Name);


               //}
               for (int i = 0; i < curcnt; i++)
               {
                   sw.Write(((Input)testcases[i]).ToString());
                   if ((i + 1) % 10 == 0)
                       sw.Write("\r\n");
                   //sheet.Cells[rows, cols] = ((Input)testcases[i]).item_list[0].ToString();
                   //sheet.Cells[rows, cols + 1] = ((Input)testcases[i]).item_list[1].ToString();
                   //rows++;
               }
               if (curcnt > 0)
               {
                   testcases.Clear();
                   curcnt = 0;
               }
               sw.Write("\r\nF值：" + fcount.ToString() + "\r\n");
               sw.Close();
               file_local.Close();
               s++;

               //r_testcases.Clear();
               //count_list.Clear();
               prog.resetRandom();

               //计算实验次数
               if (s == 1) continue;
               s_mean = 0;
               s_variance = 0;
               for (int i = 0; i < s; i++)
               {
                   s_mean += ((int)fcount_list[i]);
               }
               s_mean = s_mean / s;
               for (int i = 0; i < s; i++)
               {
                   s_variance += (double.Parse(((int)fcount_list[i]).ToString()) - s_mean) * (double.Parse(((int)fcount_list[i]).ToString()) - s_mean);
               }
               Times = (int)Math.Ceiling((1536.64 * s_variance / (s_mean * s_mean)));//Times=((196/5)^2)*s_variance/(s_mean^2)

               //if ((s >= 1000 || s > Times)&&cols>1)//最后一次且还有未写入excel的testcase
               //{

               //    sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
               //    Console.WriteLine("工作表个数" + sheets.Count.ToString() + ";当前工作表：" + sheet.Name);
               //}

           }
           int sum = 0;
           for (int i = 0; i < s; i++)
           {
               sum += ((int)fcount_list[i]);

           }
           F_measure = sum / s;
           FileStream file_end = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
           StreamWriter sw_end = new StreamWriter(file_end);
           sw_end.Write("\r\n平均的F度量值为" + F_measure.ToString() + "\r\n实验次数为" + s.ToString());
           sw_end.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "结束RT实验\r\n");
           sw_end.Close();
           file_end.Close();

           float x_count = 0;
           float[] y_count = new float[yM];
           for (int i = 0; i < yM; i++)
           {
               y_count[i] = 0;
           }
           for (int i = 0; i < xM; i++)
           {
               sheet.Cells[1, i + 2] = i;
               sheet.Cells[i + 2, 1] = i;
           }
           for (int i = 0; i < xM; i++)
           {
               x_count = 0;
               for (int j = 0; j < yM; j++)
               {
                   sheet.Cells[i + 2, j + 2] = freq[i, j] / fcount;
                   x_count += freq[i, j];
                   y_count[j] += freq[i, j];
               }
               sheet.Cells[i + 2, yM + 3] = i;
               sheet.Cells[i + 2, yM + 4] = x_count / fcount;
           }
           for (int i = 0; i < yM; i++)
           {
               sheet.Cells[i + 2, yM + 5] = i;
               sheet.Cells[i + 2, yM + 6] = y_count[i] / fcount;
           }
           sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           excel.Workbooks.Close();
           // excel.Quit();
           System.GC.Collect();

       }
       public void RRTforRealProg(float R, int Max,FaultyProgram fp,ProgSimul prog)
       {

           int s = 0;
           Input testcase = null;
           Input next_testcase = null;
           int fcount = 0;
           bool reveal = false;
           ArrayList testcases = new ArrayList();
           ArrayList fcount_list = new ArrayList();
           ArrayList count_list = new ArrayList();
           // ArrayList r_testcases = new ArrayList();//实际的随机序列
           double F_measure = -1;

           int xM = (int)prog.range_list[0] + 1;
           int yM = (int)prog.range_list[1] + 1;
           float[,] freq = new float[xM, yM];//针对二维输入

           int exclusion = -1;
           int range = -1;//每一维可取的值个数
           bool next_find = false;
           bool far = true;
           int local_tries = 0;

           double s_mean = 0;
           double s_variance = 0;
           int Times = Max;

           //Excel.Application excel = new Excel.Application();
           //Excel.Workbooks workbooks = excel.Workbooks;
           //object miss = Missing.Value;

           //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           //Excel.Sheets sheets = excel.Worksheets;
           //Excel._Worksheet sheet = (Excel._Worksheet)sheets.get_Item(1);
           //excel.DisplayAlerts = false;
           //int cols = 1;
           //int rows = 1;
           //int sheetcnt = 1;


           //for (int i = 0; i < xM; i++)
           //{
           //    for (int j = 0; j < yM; j++)
           //    {
           //        freq[i, j] = 0;
           //    }
           //}
           int cur_x, cur_y;

           FileStream file_start = new FileStream(currentPath + @"\" + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
           StreamWriter sw_start = new StreamWriter(file_start);
           sw_start.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "开始RRT实验：失效类型=" + fp.PatternType.ToString() + "\r\n输入域范围为");
           for (int i = 0; i < prog.dimension; i++)
           {
               sw_start.Write(((int)prog.range_list[i]).ToString() + " ");
           }
           sw_start.Close();
           file_start.Close();

           FileStream file_loc;
           StreamWriter sw_loc;


           
           while (s < Times && s < 5000)//重复Max次试验
           {


               //file_loc = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);


               //sw_loc = new StreamWriter(file_loc);
               //sw_loc.Write("\r\n第" + s.ToString() + "次实验\r\n");

               
               //sw_loc.Close();
               //file_loc.Close();

               fcount = 0;
               reveal = false;
               testcase = prog.randomInput();
              // count_list.Add(1);
               while (!reveal)//可能要加一个时间限制
               {
                   //if (r_testcases.Count == 10)
                   //{
                   //    FileStream file_input0 = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
                   //    StreamWriter sw_input0 = new StreamWriter(file_input0);
                   //    for (int i = 0; i < r_testcases.Count; i++)
                   //    {
                   //        sw_input0.Write(((Input)r_testcases[i]).ToString()+" ");
                   //    }
                   //    sw_input0.Write("\r\n");
                   //    sw_input0.Close();
                   //    file_input0.Close();
                   //    r_testcases.Clear();

                   //}
                   fcount++;

                   testcases.Add(testcase);
                   cur_x = (int)testcase.item_list[0];
                   cur_y = (int)testcase.item_list[1];
                   //freq[cur_x,cur_y]++;
                    //freq[cur_x, cur_y] = freq[cur_x, cur_y] + 1;
                   // r_testcases.Add(testcase);
                   reveal = !(fp.callProg(cur_x, cur_y));
                   if (!reveal)
                   {
                       exclusion = (int)(R * prog.input_size / fcount);
                       range = (int)(Math.Exp(Math.Log(exclusion, Math.E) / prog.dimension));//每一维可取的值个数
                       //if (range = 0)
                       //{
                       //    range = 1;
                       //}
                       next_find = false;
                       local_tries = 0;
                       while (!next_find)//寻找下一个test case
                       {
                           next_testcase = prog.randomInput();

                           local_tries++;
                           far = true;
                           foreach (Input t in testcases)
                           {
                               if (prog.IsAdjacent(t, next_testcase, range))
                               {
                                   far = false;
                                   break;
                               }
                           }
                           if (far)
                           {
                               next_find = true;
                               testcase = next_testcase;
                           }
                           else
                           {
                               
                           }


                       }
                       fcount += (local_tries-1);//将排除点计数
                       //count_list.Add(local_tries);//表示找到每一个测试用例随机进行的次数


                   }

               }
               //完成一次run，记录F值
               fcount_list.Add(fcount);
               //将测试结果序列写入文件
               //FileStream file_local = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.Append, FileAccess.Write);
               //StreamWriter sw = new StreamWriter(file_local);

               
               //sw.Write("\r\n随机步数序列：\r\n");
               //for (int i = 0; i < fcount; i++)
               //{

               //    sw.Write(((int)count_list[i]).ToString() + " ");
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
               //}
               //sw.Write("\r\nF值：" + fcount.ToString() + "\r\n测试用例序列：\r\n");
               //rows = 1;
               //for (int i = 0; i < fcount; i++)
               //{
               //    sw.Write(((Input)testcases[i]).ToString());
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
                   
               //}
               

               //sw.Close();
               //file_local.Close();
               s++;
               testcases.Clear();
               //r_testcases.Clear();
               //count_list.Clear();
               prog.resetRandom();

               //计算实验次数
               if (s == 1) continue;
               s_mean = 0;
               s_variance = 0;
               for (int i = 0; i < s; i++)
               {
                   s_mean += ((int)fcount_list[i]);
               }
               s_mean = s_mean / s;
               for (int i = 0; i < s; i++)
               {
                   s_variance += (double.Parse(((int)fcount_list[i]).ToString()) - s_mean) * (double.Parse(((int)fcount_list[i]).ToString()) - s_mean);
               }
               Times = (int)Math.Ceiling((1536.64 * s_variance / (s_mean * s_mean)));//Times=((196/5)^2)*s_variance/(s_mean^2)

               

           }
           int sum = 0;
           for (int i = 0; i < s; i++)
           {
               sum += ((int)fcount_list[i]);

           }
           F_measure = double.Parse(sum.ToString()) / s;
           FileStream file_end = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
           StreamWriter sw_end = new StreamWriter(file_end);
           sw_end.Write("\r\n平均的F度量值为" + F_measure.ToString() + "\r\n实验次数为" + s.ToString());
           sw_end.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "结束RRT实验\r\n");
           sw_end.Close();
           file_end.Close();

           //float x_count = 0;
           //float[] y_count = new float[yM];
           //for (int i = 0; i < yM; i++)
           //{
           //    y_count[i] = 0;
           //}

           //for (int i = 0; i < xM; i++)
           //{
           //    sheet.Cells[1, i + 2] = i;
           //    sheet.Cells[i + 2, 1] = i;
           //}
           //for (int i = 0; i < xM; i++)
           //{
           //    x_count = 0;
           //    for (int j = 0; j < yM; j++)
           //    {
           //        sheet.Cells[i + 2, j + 2] = freq[i, j] / s;
           //        x_count += freq[i, j];
           //        y_count[j] += freq[i, j];
           //    }
           //    sheet.Cells[i + 2, yM + 3] = i;
           //    sheet.Cells[i + 2, yM + 4] = x_count / s;
           //}
           //for (int i = 0; i < yM; i++)
           //{
           //    sheet.Cells[i + 2, yM + 5] = i;
           //    sheet.Cells[i + 2, yM + 6] = y_count[i] / s;
           //}
           //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           //excel.Workbooks.Close();
           // excel.Quit();
           System.GC.Collect();
       }
       public void RRTSequence(float R, int Max, FaultyProgram fp, ProgSimul prog)
       {

           int s = 0;
           Input testcase = null;
           Input next_testcase = null;
           int fcount = 0;
           bool reveal = false;
           ArrayList testcases = new ArrayList();
          // ArrayList fcount_list = new ArrayList();
          // ArrayList count_list = new ArrayList();
           // ArrayList r_testcases = new ArrayList();//实际的随机序列
           int F_measure = -1;

           int xM = (int)prog.range_list[0] + 1;
           int yM = (int)prog.range_list[1] + 1;
           float[,] freq = new float[xM, yM];//针对二维输入

           int exclusion = -1;
           int range = -1;//每一维可取的值个数
           bool next_find = false;
           bool far = true;
           int local_tries = 0;

           //double s_mean = 0;
           //double s_variance = 0;
           int Times = Max;

           Excel.Application excel = new Excel.Application();
           Excel.Workbooks workbooks = excel.Workbooks;
           object miss = Missing.Value;

           workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
           Excel.Sheets sheets = excel.Worksheets;
           Excel._Worksheet sheet = (Excel._Worksheet)sheets.get_Item(1);
           excel.DisplayAlerts = false;
           //int cols = 1;
           //int rows = 1;
           //int sheetcnt = 1;


           for (int i = 0; i < xM; i++)
           {
               for (int j = 0; j < yM; j++)
               {
                   freq[i, j] = 0;
               }
           }
           int cur_x, cur_y;

           FileStream file_start = new FileStream(currentPath + @"\" + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
           StreamWriter sw_start = new StreamWriter(file_start);
           sw_start.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "开始RRT实验：失效类型=" + fp.PatternType.ToString() + "\r\n输入域范围为");
           for (int i = 0; i < prog.dimension; i++)
           {
               sw_start.Write(((int)prog.range_list[i]).ToString() + " ");
           }
           sw_start.Close();
           file_start.Close();

           FileStream file_loc;
           StreamWriter sw_loc;



           while (s<1)//重复Max次试验
           {


               file_loc = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);


               sw_loc = new StreamWriter(file_loc);
               sw_loc.Write("\r\n第" + s.ToString() + "次实验\r\n");


               sw_loc.Close();
               file_loc.Close();

               fcount = 0;
               reveal = false;
               testcase = prog.randomInput();
              // count_list.Add(1);
               while (fcount<5000)//可能要加一个时间限制
               {
                   //if (r_testcases.Count == 10)
                   //{
                   //    FileStream file_input0 = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
                   //    StreamWriter sw_input0 = new StreamWriter(file_input0);
                   //    for (int i = 0; i < r_testcases.Count; i++)
                   //    {
                   //        sw_input0.Write(((Input)r_testcases[i]).ToString()+" ");
                   //    }
                   //    sw_input0.Write("\r\n");
                   //    sw_input0.Close();
                   //    file_input0.Close();
                   //    r_testcases.Clear();

                   //}
                   fcount++;

                   testcases.Add(testcase);
                   cur_x = (int)testcase.item_list[0];
                   cur_y = (int)testcase.item_list[1];
                   //freq[cur_x,cur_y]++;
                   freq[cur_x, cur_y] = freq[cur_x, cur_y] + 1;
                   // r_testcases.Add(testcase);
                   reveal = !(fp.callProg(cur_x, cur_y));
                   if (fcount<5000)
                   {
                       exclusion = (int)(R * prog.input_size / fcount);
                       range = (int)(Math.Exp(Math.Log(exclusion, Math.E) / prog.dimension));//每一维可取的值个数
                       //if (range = 0)
                       //{
                       //    range = 1;
                       //}
                       next_find = false;
                       local_tries = 0;
                       while (!next_find)//寻找下一个test case
                       {
                           next_testcase = prog.randomInput();

                           local_tries++;
                           far = true;
                           foreach (Input t in testcases)
                           {
                               if (prog.IsAdjacent(t, next_testcase, range))
                               {
                                   far = false;
                                   break;
                               }
                           }
                           if (far)
                           {
                               next_find = true;
                               testcase = next_testcase;
                           }
                           else
                           {

                           }


                       }
                      // count_list.Add(local_tries);//表示找到每一个测试用例随机进行的次数


                   }

               }
               //完成一次run，记录F值
              // fcount_list.Add(fcount);
               //将测试结果序列写入文件
               FileStream file_local = new FileStream(currentPath + @"\" + "n" + s.ToString() + filename, FileMode.Append, FileAccess.Write);
               StreamWriter sw = new StreamWriter(file_local);


               //sw.Write("\r\n随机步数序列：\r\n");
               //for (int i = 0; i < fcount; i++)
               //{

               //    sw.Write(((int)count_list[i]).ToString() + " ");
               //    if ((i + 1) % 10 == 0)
               //        sw.Write("\r\n");
               //}
               sw.Write("\r\nF值：" + fcount.ToString() + "\r\n测试用例序列：\r\n");
               //rows = 1;
               for (int i = 0; i < fcount; i++)
               {
                   sw.Write(((Input)testcases[i]).ToString());
                   if ((i + 1) % 10 == 0)
                       sw.Write("\r\n");

               }


               sw.Close();
               file_local.Close();
               s++;
               testcases.Clear();
               //r_testcases.Clear();
               //count_list.Clear();
               prog.resetRandom();

               //计算实验次数
               //if (s == 1) continue;
               //s_mean = 0;
               //s_variance = 0;
               //for (int i = 0; i < s; i++)
               //{
               //    s_mean += ((int)fcount_list[i]);
               //}
               //s_mean = s_mean / s;
               //for (int i = 0; i < s; i++)
               //{
               //    s_variance += (double.Parse(((int)fcount_list[i]).ToString()) - s_mean) * (double.Parse(((int)fcount_list[i]).ToString()) - s_mean);
               //}
               //Times = (int)Math.Ceiling((1536.64 * s_variance / (s_mean * s_mean)));//Times=((196/5)^2)*s_variance/(s_mean^2)



           }
           int sum = 0;
           //for (int i = 0; i < s; i++)
           //{
           //    sum += ((int)fcount_list[i]);

           //}
           F_measure = sum / s;
           FileStream file_end = new FileStream(currentPath + @"\" + filename, FileMode.Append, FileAccess.Write);
           StreamWriter sw_end = new StreamWriter(file_end);
           sw_end.Write("\r\n平均的F度量值为" + F_measure.ToString() + "\r\n实验次数为" + s.ToString());
           sw_end.Write("\r\n" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "结束RRT实验\r\n");
           sw_end.Close();
           file_end.Close();

           float x_count = 0;
           float[] y_count = new float[yM];
           for (int i = 0; i < yM; i++)
           {
               y_count[i] = 0;
           }

           for (int i = 0; i < xM; i++)
           {
               sheet.Cells[1, i + 2] = i;
               sheet.Cells[i + 2, 1] = i;
           }
           for (int i = 0; i < xM; i++)
           {
               x_count = 0;
               for (int j = 0; j < yM; j++)
               {
                   sheet.Cells[i + 2, j + 2] = freq[i, j] /fcount;
                   x_count += freq[i, j];
                   y_count[j] += freq[i, j];
               }
               sheet.Cells[i + 2, yM + 3] = i;
               sheet.Cells[i + 2, yM + 4] = x_count /fcount;
           }
           for (int i = 0; i < yM; i++)
           {
               sheet.Cells[i + 2, yM + 5] = i;
               sheet.Cells[i + 2, yM + 6] = y_count[i] /fcount;
           }
           sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

           excel.Workbooks.Close();
           // excel.Quit();
           System.GC.Collect();
       }

    }
}
