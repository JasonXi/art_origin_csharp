﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using RNG;
using System.IO;
using Microsoft.Office;
using System.Reflection;


namespace ARTLib
{
    public delegate uint executeGTC( uint dim, double[] frs, SingleFault[] faults, uint noOftcs, ref double maxDiscrep );
    public delegate int computeFails( uint dim, double[] frs, SingleFault[] faults, uint noOftcs );
    public class ART_New
    {
        SingleTClist curtclist;//sorted test sequence
        ArrayList tclist;//original test sequence
        // mtrand rng;//多次ART共享同一个seed
        Fx instanceOfFx;

        Excel.Application excel=new Excel.Application();

        /// <summary>
        /// seed为随机数种子
        /// </summary>
        /// <param name="seed"></param>
        public ART_New( uint seed )
        {
            // rng =new mtrand();
            mtrand.sgenrand( seed );

            excel = null;

        }
        public double ARTs( uint dim, double[] frs_arg, SingleFault[] faults, executeGTC gtc_alg, uint noOfTcs )
        {
            double sum = 0;
            int i = 0;
            uint cnt = 0;
            double[] frs = new double[ dim ];
            frs[ 0 ] = 0.001;
            double discrep = -1;
            double sumDiscrep = 0;
            // frs[1] = 0.01;
            while( i < 2000 )
            {
                //cnt=doART(dim);
                //cnt = doARTX2(dim);
                double x = 0.3;
                 cnt = doARTX2_NDs(dim,frs,null,0,ref x);
                //cnt = doARTCOS_NDs(dim, frs);
                // cnt = doRT_NDS(dim, frs);
                //cnt = gtc_alg( dim, frs_arg, faults, noOfTcs, ref discrep );
                sum += cnt;
                sumDiscrep += discrep;
                // sum +=doART(dim);
                i++;

                // Console.WriteLine("第" + i.ToString() + "次：" +"平均discrep为"+((double)sumDiscrep/i).ToString());

            }
            sumDiscrep = sumDiscrep / i;
            Console.WriteLine( "disper=" + sumDiscrep.ToString() + ";noOftcs=" + noOfTcs.ToString() );
            return ( ( double ) sum ) / i;

        }

        public uint doART( uint dim )
        {
            double exclusion_b = -1;
            double pdfU = -1;
            double shape_a = 8;
            double min_dist = 1;//the minimum distance between two ajacent test cases
            double theta = 0.001;
            bool fail = false;
            uint fcount = 0;
            TCNode curtc = null;

            double x0 = mtrand.e0e1genrand();
            double cur_val = -1;
            if( curtclist == null )
            {
                curtclist = new SingleTClist( dim );
                tclist = new ArrayList();
            }
            else
            {
                curtclist.tclist.Clear();
                tclist.Clear();
            }
            curtclist.tclist.Add( new TCNode( 0, 0, 0 ) );
            curtclist.tclist.Add( new TCNode( 1, 1, 1 ) );
            while( !fail )
            {
                curtc = genNext( curtclist, exclusion_b, shape_a, pdfU, fcount );//目前是针对一维的
                cur_val = ( double ) curtc.t_value;

                if( Double.IsNaN( cur_val ) || cur_val >= 1 )
                    continue;//防止误差引起的问题

                tclist.Add( cur_val );
                if( cur_val >= x0 && cur_val <= x0 + theta )
                {
                    fail = true;
                    continue;
                }
                fcount++;
                insertTCL( curtclist, curtc, ref exclusion_b, ref shape_a, ref min_dist );//insert and compute b
                pdfU = ( 1.0 - 2 * fcount * shape_a * Math.Pow( exclusion_b, 3.0 ) ) / ( 1.0 - 2 * fcount * exclusion_b );//compute θn
                computeFx( curtclist, pdfU, exclusion_b, shape_a );
            }
            return fcount;
        }
        public uint doARTX2( uint dim )
        {
            bool fail = false;
            TCNode curtc = null;
            double cur_val = -1;
            double theta = 0.001;
            uint fcount = 0;
            double x0 = mtrand.e0e1genrand();
            if( instanceOfFx == null )
            {
                instanceOfFx = new Fx( "a(x-x1)(x-x2)" );
            }
            if( curtclist == null )
            {
                curtclist = new SingleTClist( dim );
                tclist = new ArrayList();
            }
            else
            {
                curtclist.tclist.Clear();
                tclist.Clear();
            }
            curtclist.tclist.Add( new TCNode( 1, 1, 1 ) );
            int insPos = -1;
            while( !fail )
            {
                curtc = genNextX2( curtclist, fcount, null, ref insPos );//目前是针对一维的
                cur_val = ( double ) curtc.t_value;

                if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                    continue;//防止误差引起的问题

                tclist.Add( cur_val );
                fcount++;
                if( cur_val >= x0 && cur_val <= x0 + theta )
                {
                    fail = true;
                    continue;
                }

                insertTCLX2( curtclist, curtc );

                computeFxX2( curtclist );
            }
            return fcount;

        }
        /// <summary>
        /// RT方法，适用于多维
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="frs"></param>
        /// <param name="faults"></param>
        /// <param name="noOftcs"></param>
        /// <param name="maxDiscrep"></param>
        /// <returns></returns>
        public uint doRT_NDS( uint dim, double[] frs, SingleFault[] faults, uint noOftcs, ref double maxDiscrep )
        {
            bool fail = false;
            SingleTClist[] ordered_tcOfDs = new SingleTClist[ dim ];
            ArrayList[] orig_tcOfDs = new ArrayList[ dim ];
            double[] init_posOfDs = new double[ dim ];
            uint fcount = 0;
            TCNode[] curtcs = new TCNode[ dim ];
            double[] cur_vals = new double[ dim ];
            TCNode curtc = null;
            double cur_val = -1;
            // excel = new Excel.Application();
            if( faults == null )
            {
                for( int i = 0; i < dim; i++ )
                {

                    init_posOfDs[ i ] = ( 1.0 - frs[ i ] ) * mtrand.e0e1genrand();



                }
            }
            else
            {
                initPos( dim, faults );
            }
            for( int i = 0; i < dim; i++ )
            {




                if( orig_tcOfDs[ i ] == null )
                {

                    orig_tcOfDs[ i ] = new ArrayList();
                    ordered_tcOfDs[ i ] = new SingleTClist( dim );
                }
                else
                {

                    orig_tcOfDs[ i ].Clear();
                    ordered_tcOfDs[ i ].tclist.Clear();

                }

            }


            int flag = 0;
            while( !fail )
            {

                for( int i = 0; i < dim; i++ )
                {
                    curtc = genRT();//目前是针对一维的
                    cur_val = ( double ) curtc.t_value;


                    curtcs[ i ] = curtc;
                    cur_vals[ i ] = cur_val;
                    orig_tcOfDs[ i ].Add( cur_val );
                    insertTCLX2( ordered_tcOfDs[ i ], new TCNode( cur_val ) );


                }
                fcount++;

                if( noOftcs == -1 )
                {
                    if( faults == null )
                    {
                        flag = 0;
                        for( int i = 0; i < dim; i++ )
                        {
                            if( cur_vals[ i ] >= double.Parse( init_posOfDs[ i ].ToString() ) && cur_vals[ i ] <= ( double.Parse( init_posOfDs[ i ].ToString() ) + frs[ i ] ) )
                            {
                                flag++;
                            }

                        }

                        if( flag == dim && noOftcs == 0 )
                        {

                            fail = true;
                            continue;
                        }
                        flag = 0;
                    }
                    else
                    {
                        fail = executeProg( dim, cur_vals, faults );
                        if( fail && noOftcs == 0 )
                            continue;
                    }
                }
                else
                {
                    if( noOftcs > 0 && fcount >= noOftcs )
                        break;
                }



            }
            if( noOftcs != -1 )
            {
                // testcase2excel(dim, ordered_tcOfDs, frs, noOftcs);

            }
            if( noOftcs > 0 )
            {
                //  computeDiscrepancy(ordered_tcOfDs, dim, noOftcs, 0.001, ref maxDiscrep);
                // computeDiscrepancy_V1(ordered_tcOfDs, dim, noOftcs,  ref maxDiscrep);
                computeDispersion( ordered_tcOfDs, dim, noOftcs, ref maxDiscrep );
            }
            // excel.Quit();
            return fcount;



        }
        /// <summary>
        /// FSCS-ART k=10
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="frs"></param>
        /// <param name="faults"></param>
        /// <param name="noOftcs"></param>
        /// <returns></returns>
        public uint doFSCS_NDS( uint dim, double[] frs, SingleFault[] faults, uint noOftcs )
        {
            bool fail = false;
            //  SingleTClist[] ordered_tcOfDs = new SingleTClist[dim];
            ArrayList[] orig_tcOfDs = new ArrayList[ dim ];
            double[] init_posOfDs = new double[ dim ];
            uint fcount = 0;
            TCNode[] curtcs = new TCNode[ dim ];
            double[] cur_vals = new double[ dim ];
            TCNode curtc = null;
            double cur_val = -1;
            //excel = new Excel.Application();
            if( faults == null )
            {
                for( int i = 0; i < dim; i++ )
                {

                    init_posOfDs[ i ] = ( 1.0 - frs[ i ] ) * mtrand.e0e1genrand();



                }
            }
            else
            {
                initPos( dim, faults );
            }
            for( int i = 0; i < dim; i++ )
            {




                if( orig_tcOfDs[ i ] == null )
                {

                    orig_tcOfDs[ i ] = new ArrayList();
                    //ordered_tcOfDs[i] = new SingleTClist(dim);
                }
                else
                {

                    orig_tcOfDs[ i ].Clear();
                    //ordered_tcOfDs[i].tclist.Clear();

                }

            }


            int flag = 0;
            while( !fail )
            {

                //for (int i = 0; i < dim; i++)
                //{
                //    curtc = genRT();//目前是针对一维的
                //    cur_val = (double)curtc.t_value;


                //    curtcs[i] = curtc;
                //    cur_vals[i] = cur_val;
                //    orig_tcOfDs[i].Add(cur_val);
                //    insertTCLX2(ordered_tcOfDs[i], new TCNode(cur_val));


                //}
                genNextK_FSCS( cur_vals, curtcs, orig_tcOfDs, fcount, 10, dim );
                //genNext_RRT(cur_vals, curtcs, orig_tcOfDs, fcount, 0.75, dim);
                for( int i = 0; i < dim; i++ )
                {

                    orig_tcOfDs[ i ].Add( cur_vals[ i ] );



                }
                fcount++;

                if( noOftcs <= 0 )
                {
                    if( faults == null )
                    {
                        flag = 0;
                        for( int i = 0; i < dim; i++ )
                        {
                            if( cur_vals[ i ] >= double.Parse( init_posOfDs[ i ].ToString() ) && cur_vals[ i ] <= ( double.Parse( init_posOfDs[ i ].ToString() ) + frs[ i ] ) )
                            {
                                flag++;
                            }

                        }

                        if( flag == dim )
                        {

                            fail = true;
                            continue;
                        }
                        flag = 0;
                    }
                    else
                    {
                        fail = executeProg( dim, cur_vals, faults );
                        if( fail )
                            continue;
                    }
                }
                else
                {
                    if( fcount >= noOftcs )
                        break;
                }



            }
            //if (noOftcs>0)
            //{
            //    testcase2excel(dim, ordered_tcOfDs, frs, noOftcs);

            //}
            //excel.Quit();
            return fcount;



        }
        public void genNextK_FSCS( double[] curvals, TCNode[] tcOfNDs, ArrayList[] tcsOfNDs, uint fcount, int K, uint dim )
        {
            int i = 0;
            TCNode curtc = null;
            double cur_val;
            double[] newVals = new double[ dim ];
            TCNode[] newTcs = new TCNode[ dim ];
            double[] preVals = new double[ dim ];
            TCNode[] preTcs = new TCNode[ dim ];
            double[] NextVals = new double[ dim ];
            TCNode[] NextTcs = new TCNode[ dim ];
            if( fcount == 0 )
            {
                for( int j = 0; j < dim; j++ )
                {
                    curtc = genRT();//目前是针对一维的
                    cur_val = ( double ) curtc.t_value;


                    tcOfNDs[ j ] = curtc;
                    curvals[ j ] = cur_val;
                    //orig_tcOfDs[i].Add(cur_val);
                    //insertTCLX2(ordered_tcOfDs[i], new TCNode(cur_val));


                }
                return;
            }

            double minOfOne = 0;
            double maxOfK = -1;
            double dist = 0;
            while( i < K )
            {

                for( int j = 0; j < dim; j++ )
                {
                    newTcs[ j ] = genRT();//目前是针对一维的
                    newVals[ j ] = ( double ) ( newTcs[ j ].t_value );


                }

                //求newTc与PreTc的最小距离
                minOfOne = -1;
                for( int l = 0; l < fcount; l++ )
                {
                    for( int h = 0; h < dim; h++ )
                    {
                        preVals[ h ] = double.Parse( tcsOfNDs[ h ][ l ].ToString() );
                    }

                    dist = 0;
                    for( int h = 0; h < dim; h++ )
                    {
                        dist += ( preVals[ h ] - newVals[ h ] ) * ( preVals[ h ] - newVals[ h ] );
                    }
                    if( minOfOne == -1 || dist < minOfOne )
                    {
                        minOfOne = dist;
                    }

                }
                if( minOfOne > maxOfK )
                {
                    for( int j = 0; j < dim; j++ )
                    {
                        NextTcs[ j ] = newTcs[ j ];
                        NextVals[ j ] = newVals[ j ];
                        maxOfK = minOfOne;


                    }


                }



                i++;
            }
            for( int j = 0; j < dim; j++ )
            {
                tcOfNDs[ j ] = NextTcs[ j ];
                curvals[ j ] = NextVals[ j ];
                // tcsOfNDs[j].Add(NextVals[j]);


            }



        }
        /// <summary>
        /// exclusion作为排除域的半径
        /// </summary>
        /// <param name="curvals"></param>
        /// <param name="tcOfNDs"></param>
        /// <param name="tcsOfNDs"></param>
        /// <param name="fcount"></param>
        /// <param name="exclusion"></param>
        /// <param name="dim"></param>
        public void genNext_RRT( double[] curvals, TCNode[] tcOfNDs, ArrayList[] tcsOfNDs, uint fcount, double exclusion, uint dim )
        {
            int i = 0;
            TCNode curtc = null;
            double cur_val;
            double[] newVals = new double[ dim ];
            TCNode[] newTcs = new TCNode[ dim ];
            double[] preVals = new double[ dim ];
            TCNode[] preTcs = new TCNode[ dim ];
            double[] NextVals = new double[ dim ];
            TCNode[] NextTcs = new TCNode[ dim ];

            if( fcount == 0 )
            {
                for( int j = 0; j < dim; j++ )
                {
                    curtc = genRT();//目前是针对一维的
                    cur_val = ( double ) curtc.t_value;


                    tcOfNDs[ j ] = curtc;
                    curvals[ j ] = cur_val;
                    //orig_tcOfDs[i].Add(cur_val);
                    //insertTCLX2(ordered_tcOfDs[i], new TCNode(cur_val));


                }
                return;
            }

            double ex2 = exclusion * exclusion / fcount;
            double dist = 0;
            bool far = false;
            while( !far )
            {

                for( int j = 0; j < dim; j++ )
                {
                    newTcs[ j ] = genRT();//目前是针对一维的
                    newVals[ j ] = ( double ) ( newTcs[ j ].t_value );


                }


                far = true;
                for( int l = 0; l < fcount; l++ )
                {
                    for( int h = 0; h < dim; h++ )
                    {
                        preVals[ h ] = double.Parse( tcsOfNDs[ h ][ l ].ToString() );
                    }

                    dist = 0;
                    for( int h = 0; h < dim; h++ )
                    {
                        dist += ( preVals[ h ] - newVals[ h ] ) * ( preVals[ h ] - newVals[ h ] );
                    }
                    if( dist < ex2 )
                    {
                        far = false;
                        break;
                    }

                }
                if( far )
                {
                    for( int j = 0; j < dim; j++ )
                    {
                        NextTcs[ j ] = newTcs[ j ];
                        NextVals[ j ] = newVals[ j ];
                        //maxOfK = minOfOne;


                    }
                    break;

                }

            }
            for( int j = 0; j < dim; j++ )
            {
                tcOfNDs[ j ] = NextTcs[ j ];
                curvals[ j ] = NextVals[ j ];
                // tcsOfNDs[j].Add(NextVals[j]);


            }



        }
        public TCNode genRT()
        {
            double tu = mtrand.e0e1genrand();//产生不包括0,1的随机数
            return new TCNode( tu );

        }
        /// <summary>
        /// 一维的X2
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="frs"></param>
        /// <param name="faults"></param>
        /// <param name="noOftcs"></param>
        /// <returns></returns>
        public uint doARTX2_NDs( uint dim, double[] frs, SingleFault[] faults, uint noOftcs, ref double maxDiscrep )
        {
            bool fail = false;
            SingleTClist[] ordered_tcOfDs = new SingleTClist[ dim ];
            ArrayList[] orig_tcOfDs = new ArrayList[ dim ];
            Fx[] FxOfDs = new Fx[ dim ];
            double[] init_posOfDs = new double[ dim ];
            TCNode[] curtcs = new TCNode[ dim ];
            double[] cur_vals = new double[ dim ];
            TCNode curtc = null;
            double cur_val = -1;

            uint fcount = 0;

            // excel = new Excel.Application();
            if( faults == null )
            {
                for( int i = 0; i < dim; i++ )
                {
                    init_posOfDs[ i ] = ( 1.0 - frs[ i ] ) * mtrand.e0e1genrand();

                }
            }
            else
            {
                initPos( dim, faults );

            }
            for( int i = 0; i < dim; i++ )
            {

                if( ordered_tcOfDs[ i ] == null )
                {
                    ordered_tcOfDs[ i ] = new SingleTClist( ( uint ) i );
                    orig_tcOfDs[ i ] = new ArrayList();
                }
                else
                {
                    ordered_tcOfDs[ i ].tclist.Clear();
                    orig_tcOfDs[ i ].Clear();

                }
                if( FxOfDs[ i ] == null )
                {
                    FxOfDs[ i ] = new Fx( "a(x-x1)(x-x2)" );
                }
            }

            for( int i = 0; i < dim; i++ )
            {
                ordered_tcOfDs[ i ].tclist.Add( new TCNode( 1, 1, 1 ) );
            }
            int flag = 0;
            int inserPos = -1;
            while( !fail )
            {

                for( int i = 0; i < dim; i++ )
                {
                    curtc = genNextX2( ordered_tcOfDs[ i ], fcount, FxOfDs[ i ], ref inserPos );//目前是针对一维的
                    //  curtc = genNextX2_V1(ordered_tcOfDs[i], fcount, FxOfDs[i]);
                    cur_val = ( double ) curtc.t_value;

                    if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                    {
                        i--;//原地不动
                        continue;
                    }//防止误差引起的问题
                    curtcs[ i ] = curtc;
                    cur_vals[ i ] = cur_val;
                    orig_tcOfDs[ i ].Add( cur_val );

                }
                fcount++;
                if( faults == null )
                {
                    flag = 0;
                    for( int i = 0; i < dim; i++ )
                    {
                        if( cur_vals[ i ] >= double.Parse( init_posOfDs[ i ].ToString() ) && cur_vals[ i ] <= ( double.Parse( init_posOfDs[ i ].ToString() ) + frs[ i ] ) )
                        {
                            flag++;
                        }

                    }

                    if( flag == dim && noOftcs == 0 )
                    {

                        fail = true;
                        continue;

                    }
                    flag = 0;
                }
                else
                {
                    fail = executeProg( dim, cur_vals, faults );
                    if( fail && noOftcs == 0 )
                    {
                        continue;

                    }


                }

                // int insPos = -1;
                for( int i = 0; i < dim; i++ )
                {
                    // insPos=insertTCLX2(ordered_tcOfDs[i], curtcs[i]);
                    insertTCLX2_V2( ordered_tcOfDs[ i ], curtcs[ i ], inserPos );
                    //computeFxX2(ordered_tcOfDs[i],FxOfDs[i],insPos);
                    // computeFxX2_V1(ordered_tcOfDs[i], FxOfDs[i], insPos);
                    computeFxX2_V3( ordered_tcOfDs[ i ], FxOfDs[ i ], inserPos );
                }
                if( noOftcs > 0 && fcount == noOftcs )
                {
                    break;
                }


            }
            if( noOftcs > 0 )
            {
                //computeDiscrepancy(ordered_tcOfDs, dim, noOftcs, 0.001,ref maxDiscrep);
                computeDispersion( ordered_tcOfDs, dim, noOftcs, ref maxDiscrep );
                // computeDiscrepancy_V1(ordered_tcOfDs, dim, noOftcs, ref maxDiscrep);
            }
            // excel.Quit();
            return fcount;

        }
        public void computeDiscrepancy( SingleTClist[] ordered_tcOfDs, uint dim, uint noOftcs, double interval, ref double max_Discrep )
        {

            int noOfintervals = ( int ) ( 1.0 / interval );
            int[] freqs1 = null;
            int[ , ] freqs2 = null;
            int[ , , ] freqs3 = null;
            TCNode tc = null;
            double maxDiscrep = -1;
            if( dim == 1 )
            {
                freqs1 = new int[ noOfintervals ];
                for( int i = 0; i < noOfintervals; i++ )
                    freqs1[ i ] = 0;
                int xIndex = -1;
                for( int i = 0; i < noOftcs; i++ )
                {
                    tc = ( TCNode ) ordered_tcOfDs[ 0 ].tclist[ i ];
                    xIndex = ( int ) ( double.Parse( tc.t_value.ToString() ) / interval );
                    freqs1[ xIndex ]++;

                }
                double diff = 0;
                for( int i = 0; i < noOfintervals; i++ )
                {
                    diff = Math.Abs( double.Parse( freqs1[ i ].ToString() ) / noOftcs - interval );
                    if( diff > maxDiscrep )
                    {
                        maxDiscrep = diff;
                    }
                }
            }
            else if( dim == 2 )
            {
                freqs2 = new int[ noOfintervals, noOfintervals ];
                for( int i = 0; i < noOfintervals; i++ )
                {
                    for( int j = 0; j < noOfintervals; j++ )
                    {
                        freqs2[ i, j ] = 0;
                    }
                }
                int xIndex = -1;
                int yIndex = -1;
                for( int i = 0; i < noOftcs; i++ )
                {
                    tc = ( TCNode ) ordered_tcOfDs[ 0 ].tclist[ i ];
                    xIndex = ( int ) ( double.Parse( tc.t_value.ToString() ) / interval );
                    tc = ( TCNode ) ordered_tcOfDs[ 1 ].tclist[ i ];
                    yIndex = ( int ) ( double.Parse( tc.t_value.ToString() ) / interval );
                    freqs2[ xIndex, yIndex ] = freqs2[ xIndex, yIndex ] + 1;

                }
                double diff = 0;
                for( int i = 0; i < noOfintervals; i++ )
                {
                    for( int j = 0; j < noOfintervals; j++ )
                    {
                        diff = Math.Abs( double.Parse( freqs2[ i, j ].ToString() ) / noOftcs - interval * interval );
                        if( diff > maxDiscrep )
                        {
                            maxDiscrep = diff;
                        }

                    }
                }
            }
            else if( dim == 3 )
            {
                freqs3 = new int[ noOfintervals, noOfintervals, noOfintervals ];
                for( int i = 0; i < noOfintervals; i++ )
                {
                    for( int j = 0; j < noOfintervals; j++ )
                    {
                        for( int k = 0; k < noOfintervals; k++ )
                        {
                            freqs3[ i, j, k ] = 0;
                        }
                    }
                }
                int xIndex = -1;
                int yIndex = -1;
                int zIndex = -1;
                for( int i = 0; i < noOftcs; i++ )
                {
                    tc = ( TCNode ) ordered_tcOfDs[ 0 ].tclist[ i ];
                    xIndex = ( int ) ( double.Parse( tc.t_value.ToString() ) / interval );
                    tc = ( TCNode ) ordered_tcOfDs[ 1 ].tclist[ i ];
                    yIndex = ( int ) ( double.Parse( tc.t_value.ToString() ) / interval );
                    tc = ( TCNode ) ordered_tcOfDs[ 2 ].tclist[ i ];
                    zIndex = ( int ) ( double.Parse( tc.t_value.ToString() ) / interval );
                    freqs3[ xIndex, yIndex, zIndex ] = freqs3[ xIndex, yIndex, zIndex ] + 1;

                }
                double diff = 0;
                for( int i = 0; i < noOfintervals; i++ )
                {
                    for( int j = 0; j < noOfintervals; j++ )
                    {
                        for( int k = 0; k < noOfintervals; k++ )
                        {
                            diff = Math.Abs( double.Parse( freqs3[ i, j, k ].ToString() ) / noOftcs - interval * interval * interval );
                            if( diff > maxDiscrep )
                            {
                                maxDiscrep = diff;
                            }
                        }

                    }
                }


            }
            max_Discrep = maxDiscrep;
            // Console.WriteLine("discrep=" + maxDiscrep.ToString() + ";noOftcs=" + noOftcs.ToString());



        }
        public void computeDispersion( SingleTClist[] ordered_tclist, uint dim, uint noOftcs, ref double disperion )
        {
            double maxdisper = -1;

            double minDist = -1;
            TCNode[] curtcOfNds = new TCNode[ dim ];
            TCNode[] tmptcOfNds = new TCNode[ dim ];
            double[] curOfvals = new double[ dim ];
            double[] tmpOfvals = new double[ dim ];
            double tmpDist = 0;
            for( int i = 0; i < noOftcs; i++ )
            {
                minDist = -1;
                for( int k = 0; k < dim; k++ )
                {
                    curtcOfNds[ k ] = ( TCNode ) ordered_tclist[ k ].tclist[ i ];
                    curOfvals[ k ] = double.Parse( ( curtcOfNds[ k ].t_value ).ToString() );

                }
                for( int j = 0; j < noOftcs; j++ )
                {
                    if( j == i )
                        continue;
                    else
                    {
                        tmpDist = 0;
                        for( int k = 0; k < dim; k++ )
                        {
                            tmptcOfNds[ k ] = ( TCNode ) ordered_tclist[ k ].tclist[ j ];
                            tmpOfvals[ k ] = double.Parse( ( tmptcOfNds[ k ].t_value ).ToString() );
                            tmpDist += ( tmpOfvals[ k ] - curOfvals[ k ] ) * ( tmpOfvals[ k ] - curOfvals[ k ] );

                        }
                        if( minDist == -1 || tmpDist < minDist )
                        {
                            minDist = tmpDist;
                        }

                    }
                }
                if( minDist > maxdisper )
                {
                    maxdisper = minDist;
                }




            }
            disperion = Math.Sqrt( maxdisper );
        }
        /// <summary>
        /// 随机生成1000个任意大小的子域
        /// </summary>
        /// <param name="ordered_tcOfDs"></param>
        /// <param name="dim"></param>
        /// <param name="noOftcs"></param>
        /// <param name="max_Discrep"></param>
        public void computeDiscrepancy_V1( SingleTClist[] ordered_tcOfDs, uint dim, uint noOftcs, ref double max_Discrep )
        {
            SingleFault[] faults = new SingleFault[ 1000 ];
            double[] discreps = new double[ 1000 ];
            double[] curvals = new double[ ( int ) dim ];
            TCNode curtc = null;
            for( int i = 0; i < 1000; i++ )
            {
                faults[ i ] = new SingleFault( dim, mtrand.e0e1genrand() );
                discreps[ i ] = 0;
            }
            initPos( dim, faults );
            for( int i = 0; i < noOftcs; i++ )
            {
                for( int j = 0; j < dim; j++ )
                {
                    curtc = ( TCNode ) ordered_tcOfDs[ j ].tclist[ i ];
                    curvals[ j ] = double.Parse( curtc.t_value.ToString() );

                }
                SingleFault fault;
                bool fail = false;
                double fr_sval;
                int flag;
                for( int k = 0; k < faults.Length; k++ )
                {
                    fault = ( SingleFault ) faults[ k ];

                    flag = 0;
                    for( int j = 0; j < dim; j++ )
                    {
                        fr_sval = double.Parse( ( fault.init_posOfNDs[ j ] ).ToString() );
                        if( curvals[ j ] >= fr_sval && curvals[ j ] <= ( fr_sval + ( double ) ( fault.fr_dimensions[ j ] ) ) )
                        {
                            flag++;

                        }
                    }
                    if( flag == dim )
                    {
                        discreps[ k ] = discreps[ k ] + 1;

                    }

                }


            }
            double maxdiscrep = -1;
            double eachdiscrep = -1;
            for( int i = 0; i < faults.Length; i++ )
            {
                eachdiscrep = Math.Abs( discreps[ i ] / noOftcs - faults[ i ].failr );
                if( eachdiscrep > maxdiscrep )
                {
                    maxdiscrep = eachdiscrep;
                }


            }
            max_Discrep = maxdiscrep;


        }
        /// <summary>
        /// 利用Hilbert Curve降维，二维或二维以上
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="frs"></param>
        /// <param name="faults"></param>
        /// <param name="noOftcs"></param>
        /// <returns></returns>
        public uint doARTX2_NDs_V1( uint dim, double[] frs, SingleFault[] faults, uint noOftcs, ref double discrep )
        {
            bool fail = false;
            SingleTClist[] ordered_tcOfDs = new SingleTClist[ dim ];
            ArrayList[] orig_tcOfDs = new ArrayList[ dim ];
            // Fx[] FxOfDs = new Fx[dim];
            Fx FxOfCommon = new Fx( "a(x-x1)(x-x2)" );
            SingleTClist tcsOfHibert = new SingleTClist( 1 );
            double[] init_posOfDs = new double[ dim ];
            TCNode[] curtcs = new TCNode[ dim ];
            double[] cur_vals = new double[ dim ];
            TCNode curtc = null;
            double cur_val = -1;

            uint fcount = 0;
            // excel = new Excel.Application();
            if( faults == null )
            {
                for( int i = 0; i < dim; i++ )
                {
                    init_posOfDs[ i ] = ( 1.0 - frs[ i ] ) * mtrand.e0e1genrand();

                }
            }
            else
            {
                initPos( dim, faults );

            }
            for( int i = 0; i < dim; i++ )
            {

                if( ordered_tcOfDs[ i ] == null )
                {
                    ordered_tcOfDs[ i ] = new SingleTClist( ( uint ) i );
                    orig_tcOfDs[ i ] = new ArrayList();
                }
                else
                {
                    ordered_tcOfDs[ i ].tclist.Clear();
                    orig_tcOfDs[ i ].Clear();

                }
                curtcs[ i ] = new TCNode( null );
                //if (FxOfDs[i] == null)
                //{
                //    FxOfDs[i] = new Fx("a(x-x1)(x-x2)");
                //}
            }

            //for (int i = 0; i < dim; i++)
            //{
            //    ordered_tcOfDs[i].tclist.Add(new TCNode(1, 1, 1));
            //}

            tcsOfHibert.tclist.Add( new TCNode( 1, 1, 1 ) );
            int flag = 0;
            int inserPos = -1;
            while( !fail )
            {

                // curtc = genNextK_Hilbert(fcount, FxOfCommon, tcsOfHibert, 10);//产生10个候选的tc

                curtc = genNext_Hilber( fcount, FxOfCommon, tcsOfHibert, ref inserPos );
                cur_val = ( double ) curtc.t_value;

                if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                {
                    //i--;//原地不动
                    continue;
                }//防止误差引起的问题
                // genNextFromHilber(cur_val, cur_vals, curtcs, orig_tcOfDs, dim,ordered_tcOfDs);
                genNextFromHilber_V1( cur_val, cur_vals, curtcs, orig_tcOfDs, dim, ordered_tcOfDs );
                //for (int i = 0; i < dim; i++)
                //{



                //    curtcs[i] = curtc;
                //    cur_vals[i] = cur_val;
                //    orig_tcOfDs[i].Add(cur_val);

                //}
                // curtc= genNextK_Hilbert_V2(fcount, FxOfCommon, tcsOfHibert, 10, dim, orig_tcOfDs, cur_vals, curtcs);
                // cur_val = (double)curtc.t_value;
                fcount++;
                if( faults == null )
                {
                    flag = 0;
                    for( int i = 0; i < dim; i++ )
                    {
                        if( cur_vals[ i ] >= double.Parse( init_posOfDs[ i ].ToString() ) && cur_vals[ i ] <= ( double.Parse( init_posOfDs[ i ].ToString() ) + frs[ i ] ) )
                        {
                            flag++;
                        }

                    }

                    if( flag == dim && noOftcs == 0 )
                    {

                        fail = true;
                        continue;
                    }
                    flag = 0;
                }
                else
                {
                    fail = executeProg( dim, cur_vals, faults );
                    if( fail && noOftcs == 0 )
                    {
                        continue;
                    }


                }
                int insPos = -1;
                //insPos = insertTCLX2(tcsOfHibert, curtc);
                insertTCLX2_V2( tcsOfHibert, curtc, inserPos );
                computeFxX2_V3( tcsOfHibert, FxOfCommon, inserPos );
                if( noOftcs > 0 && fcount == noOftcs )
                {
                    break;
                }
                //for (int i = 0; i < dim; i++)
                //{
                //    insPos = insertTCLX2(ordered_tcOfDs[i], curtcs[i]);
                //    //computeFxX2(ordered_tcOfDs[i],FxOfDs[i],insPos);
                //    // computeFxX2_V1(ordered_tcOfDs[i], FxOfDs[i], insPos);
                //    //computeFxX2_V2(ordered_tcOfDs[i], FxOfDs[i], insPos);
                //    computeFxX2_V3(ordered_tcOfDs[i], FxOfDs[i], insPos);
                //}


            }
            if( noOftcs > 0 )
            {
                //computeDiscrepancy(ordered_tcOfDs, dim, noOftcs, 0.01, ref discrep);
                // computeDispersion(ordered_tcOfDs, dim, noOftcs, ref discrep);
                computeDiscrepancy_V1( ordered_tcOfDs, dim, noOftcs, ref discrep );
                //testcase2excel(dim, ordered_tcOfDs, frs, fcount);
            }
            //excel.Quit();
            return fcount;

        }
        /// <summary>
        /// 产生k个候选的tc，并选择fx最大的
        /// </summary>
        /// <param name="fcount"></param>
        /// <param name="FxOfHilbert"></param>
        /// <param name="tcsOfHibert"></param>
        /// <param name="K"></param>
        /// <returns></returns>
        public TCNode genNextK_Hilbert( uint fcount, Fx FxOfHilbert, SingleTClist tcsOfHibert, int K )
        {
            int i = 0;
            TCNode nextTC = null;
            TCNode curtc;
            double maxfx = -1;
            double cur_val;
            int inserPos = -1;
            if( fcount == 0 )
            {
                return genNext_Hilber( fcount, FxOfHilbert, tcsOfHibert, ref inserPos );
            }
            while( i < K )
            {
                curtc = genNext_Hilber( fcount, FxOfHilbert, tcsOfHibert, ref inserPos );
                cur_val = ( double ) curtc.t_value;

                if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                {
                    //i--;//原地不动
                    continue;
                }//防止误差引起的问题
                if( curtc.fx_val > maxfx )
                {
                    nextTC = curtc;
                    maxfx = curtc.fx_val;
                }
                i++;
            }
            return nextTC;

        }
        public int computeFails_RT( uint dim, double[] frs, SingleFault[] faults, uint noOftcs )
        {
            bool fail = false;
            SingleTClist[] ordered_tcOfDs = new SingleTClist[ dim ];
            ArrayList[] orig_tcOfDs = new ArrayList[ dim ];
            double[] init_posOfDs = new double[ dim ];
            uint fcount = 0;
            TCNode[] curtcs = new TCNode[ dim ];
            double[] cur_vals = new double[ dim ];
            TCNode curtc = null;
            double cur_val = -1;
            // excel = new Excel.Application();
            if( faults == null )
            {
                for( int i = 0; i < dim; i++ )
                {

                    init_posOfDs[ i ] = ( 1.0 - frs[ i ] ) * mtrand.e0e1genrand();



                }
            }
            else
            {
                initPos( dim, faults );
            }
            for( int i = 0; i < dim; i++ )
            {




                if( orig_tcOfDs[ i ] == null )
                {

                    orig_tcOfDs[ i ] = new ArrayList();
                    ordered_tcOfDs[ i ] = new SingleTClist( dim );
                }
                else
                {

                    orig_tcOfDs[ i ].Clear();
                    ordered_tcOfDs[ i ].tclist.Clear();

                }

            }


            int flag = 0;
            int noOffails = 0;
            while( fcount < noOftcs )
            {

                for( int i = 0; i < dim; i++ )
                {
                    curtc = genRT();//目前是针对一维的
                    cur_val = ( double ) curtc.t_value;


                    curtcs[ i ] = curtc;
                    cur_vals[ i ] = cur_val;
                    orig_tcOfDs[ i ].Add( cur_val );
                    insertTCLX2( ordered_tcOfDs[ i ], new TCNode( cur_val ) );


                }
                fcount++;


                if( faults == null )
                {
                    flag = 0;
                    for( int i = 0; i < dim; i++ )
                    {
                        if( cur_vals[ i ] >= double.Parse( init_posOfDs[ i ].ToString() ) && cur_vals[ i ] <= ( double.Parse( init_posOfDs[ i ].ToString() ) + frs[ i ] ) )
                        {
                            flag++;
                        }

                    }

                    if( flag == dim )
                    {

                        noOffails++;
                    }
                    flag = 0;
                }
                else
                {
                    fail = executeProg( dim, cur_vals, faults );
                    if( fail )
                        noOffails++;
                }





            }
            return noOffails;
        }
        public int computeFails_1D( uint dim, double[] frs, SingleFault[] faults, uint noOftcs )
        {
           // Console.Out.WriteLine("start computeFails_1D!!!");
            bool fail = false;
            SingleTClist[] ordered_tcOfDs = new SingleTClist[ dim ];
            ArrayList[] orig_tcOfDs = new ArrayList[ dim ];
            Fx[] FxOfDs = new Fx[ dim ];
            double[] init_posOfDs = new double[ dim ];
            TCNode[] curtcs = new TCNode[ dim ];
            double[] cur_vals = new double[ dim ];
            TCNode curtc = null;
            double cur_val = -1;

            uint fcount = 0;

            // excel = new Excel.Application();
            if( faults == null )
            {
                for( int i = 0; i < dim; i++ )
                {
                    init_posOfDs[ i ] = ( 1.0 - frs[ i ] ) * mtrand.e0e1genrand();

                }
            }
            else
            {
                initPos( dim, faults );

            }
            for( int i = 0; i < dim; i++ )
            {

                if( ordered_tcOfDs[ i ] == null )
                {
                    ordered_tcOfDs[ i ] = new SingleTClist( ( uint ) i );
                    orig_tcOfDs[ i ] = new ArrayList();
                }
                else
                {
                    ordered_tcOfDs[ i ].tclist.Clear();
                    orig_tcOfDs[ i ].Clear();

                }
                if( FxOfDs[ i ] == null )
                {
                    FxOfDs[ i ] = new Fx( "a(x-x1)(x-x2)" );
                }
            }

            for( int i = 0; i < dim; i++ )
            {
                ordered_tcOfDs[ i ].tclist.Add( new TCNode( 1, 1, 1 ) );
            }
            int flag = 0;
            int inserPos = -1;
            int noOffails = 0;
            while( fcount < noOftcs )
            {

                for( int i = 0; i < dim; i++ )
                {
                    curtc = genNextX2( ordered_tcOfDs[ i ], fcount, FxOfDs[ i ], ref inserPos );//目前是针对一维的
                    //  curtc = genNextX2_V1(ordered_tcOfDs[i], fcount, FxOfDs[i]);
                    cur_val = ( double ) curtc.t_value;

                    if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                    {
                        i--;//原地不动
                        continue;
                    }//防止误差引起的问题
                    curtcs[ i ] = curtc;
                    cur_vals[ i ] = cur_val;
                    orig_tcOfDs[ i ].Add( cur_val );

                }
                fcount++;
                if( faults == null )
                {
                    flag = 0;
                    for( int i = 0; i < dim; i++ )
                    {
                        if( cur_vals[ i ] >= double.Parse( init_posOfDs[ i ].ToString() ) && cur_vals[ i ] <= ( double.Parse( init_posOfDs[ i ].ToString() ) + frs[ i ] ) )
                        {
                            flag++;
                        }

                    }

                    if( flag == dim )
                    {

                        noOffails++;

                    }
                    flag = 0;
                }
                else
                {
                    fail = executeProg( dim, cur_vals, faults );
                    if( fail )
                    {
                        noOffails++;

                    }


                }

                // int insPos = -1;
                for( int i = 0; i < dim; i++ )
                {
                    // insPos=insertTCLX2(ordered_tcOfDs[i], curtcs[i]);
                    insertTCLX2_V2( ordered_tcOfDs[ i ], curtcs[ i ], inserPos );
                    //computeFxX2(ordered_tcOfDs[i],FxOfDs[i],insPos);
                    // computeFxX2_V1(ordered_tcOfDs[i], FxOfDs[i], insPos);
                    computeFxX2_V3( ordered_tcOfDs[ i ], FxOfDs[ i ], inserPos );
                }



            }
            return noOffails;

        }
        public int computeFails_Hilbert( uint dim, double[] frs, SingleFault[] faults, uint noOftcs )
        {
            bool fail = false;
            SingleTClist[] ordered_tcOfDs = new SingleTClist[ dim ];
            ArrayList[] orig_tcOfDs = new ArrayList[ dim ];
            // Fx[] FxOfDs = new Fx[dim];
            Fx FxOfCommon = new Fx( "a(x-x1)(x-x2)" );
            SingleTClist tcsOfHibert = new SingleTClist( 1 );
            double[] init_posOfDs = new double[ dim ];
            TCNode[] curtcs = new TCNode[ dim ];
            double[] cur_vals = new double[ dim ];
            TCNode curtc = null;
            double cur_val = -1;

            uint fcount = 0;
            // excel = new Excel.Application();
            if( faults == null )
            {
                for( int i = 0; i < dim; i++ )
                {
                    init_posOfDs[ i ] = ( 1.0 - frs[ i ] ) * mtrand.e0e1genrand();

                }
            }
            else
            {
                initPos( dim, faults );

            }
            for( int i = 0; i < dim; i++ )
            {

                if( ordered_tcOfDs[ i ] == null )
                {
                    ordered_tcOfDs[ i ] = new SingleTClist( ( uint ) i );
                    orig_tcOfDs[ i ] = new ArrayList();
                }
                else
                {
                    ordered_tcOfDs[ i ].tclist.Clear();
                    orig_tcOfDs[ i ].Clear();

                }
                curtcs[ i ] = new TCNode( null );
                //if (FxOfDs[i] == null)
                //{
                //    FxOfDs[i] = new Fx("a(x-x1)(x-x2)");
                //}
            }

            for( int i = 0; i < dim; i++ )
            {
                ordered_tcOfDs[ i ].tclist.Add( new TCNode( 1, 1, 1 ) );
            }

            tcsOfHibert.tclist.Add( new TCNode( 1, 1, 1 ) );
            int flag = 0;
            int inserPos = -1;
            int noOffails = 0;
            while( fcount < noOftcs )
            {

                // curtc = genNextK_Hilbert(fcount, FxOfCommon, tcsOfHibert, 10);//产生10个候选的tc

                curtc = genNext_Hilber( fcount, FxOfCommon, tcsOfHibert, ref inserPos );
                cur_val = ( double ) curtc.t_value;

                if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                {
                    //i--;//原地不动
                    continue;
                }//防止误差引起的问题
                genNextFromHilber( cur_val, cur_vals, curtcs, orig_tcOfDs, dim, ordered_tcOfDs );
                //for (int i = 0; i < dim; i++)
                //{



                //    curtcs[i] = curtc;
                //    cur_vals[i] = cur_val;
                //    orig_tcOfDs[i].Add(cur_val);

                //}
                // curtc= genNextK_Hilbert_V2(fcount, FxOfCommon, tcsOfHibert, 10, dim, orig_tcOfDs, cur_vals, curtcs);
                // cur_val = (double)curtc.t_value;
                fcount++;
                if( faults == null )
                {
                    flag = 0;
                    for( int i = 0; i < dim; i++ )
                    {
                        if( cur_vals[ i ] >= double.Parse( init_posOfDs[ i ].ToString() ) && cur_vals[ i ] <= ( double.Parse( init_posOfDs[ i ].ToString() ) + frs[ i ] ) )
                        {
                            flag++;
                        }

                    }

                    if( flag == dim )
                    {

                        noOffails++;

                    }
                    flag = 0;
                }
                else
                {
                    fail = executeProg( dim, cur_vals, faults );
                    if( fail )
                    {
                        noOffails++;
                    }


                }
                int insPos = -1;
                //insPos = insertTCLX2(tcsOfHibert, curtc);
                insertTCLX2_V2( tcsOfHibert, curtc, inserPos );
                computeFxX2_V3( tcsOfHibert, FxOfCommon, inserPos );




            }

            return noOffails;
        }
        public double computeP( uint dim, double[] frs_arg, SingleFault[] faults, computeFails computeF, uint noOfTcs )
        {
            double sumOffails = 0;
            int i = 0;
            uint cnt = 0;

            int noOffails = 0;
            // frs[1] = 0.01;
            while( i < 2000 )
            {
                //cnt=doART(dim);
                //cnt = doARTX2(dim);
                // cnt = doARTX2_NDs(dim,frs);
                //cnt = doARTCOS_NDs(dim, frs);
                // cnt = doRT_NDS(dim, frs);
                noOffails = computeF( dim, frs_arg, faults, noOfTcs );

                if( noOffails > 0 )
                {
                    sumOffails++;
                }

                i++;
                //  Console.WriteLine("第" + i.ToString() + "次：" + cnt.ToString()+";平均次数为"+((double)sum/i).ToString());

            }
            sumOffails = sumOffails / i;
            Console.WriteLine( "P=" + sumOffails.ToString() + ";noOftcs=" + noOfTcs.ToString() );
            return sumOffails;


        }
        public double computeE( uint dim, double[] frs_arg, SingleFault[] faults, computeFails computeF, uint noOfTcs )
        {
           // Console.Out.WriteLine("start computeE!!!");
            double sumOffails = 0;
            int i = 0;
            uint cnt = 0;

            int noOffails = 0;
            // frs[1] = 0.01;
            while( i < 5 )  //原来是2000
            {

                noOffails = computeF( dim, frs_arg, faults, noOfTcs );

                if( noOffails > 0 )
                {
                    sumOffails += noOffails;
                }

                i++;
                  Console.WriteLine("第" + i.ToString() + "次：" + cnt.ToString() + ";平均次数为" + ((double)sumOffails / i).ToString());


            }
            sumOffails = sumOffails / i;
            Console.WriteLine( "E=" + sumOffails.ToString() + ";noOftcs=" + noOfTcs.ToString() );
            return sumOffails;


        }
        public TCNode genNextK_Hilbert_V2( uint fcount, Fx FxOfHilbert, SingleTClist tcsOfHibert, int K, uint dim, ArrayList[] orig_tcsOfNDs, double[] curvals, TCNode[] curtcsOfNds )
        {
            int i = 0;//候选用例的计数变量

            TCNode curtc;
            double maxfx = -1;
            double cur_val;
            TCNode[] newTcs = new TCNode[ dim ];
            double[] newVals = new double[ dim ];
            TCNode[] adjTcs = new TCNode[ dim ];
            double[] adjVals = new double[ dim ];
            TCNode[] nextTcs = new TCNode[ dim ];
            double[] nextVals = new double[ dim ];
            TCNode nextTc = null;
            double dist1 = 0;
            double dist2 = 0;
            double valD = 0;
            double minDist = -1;
            double maxDist = -1;
            int inserPos = -1;
            for( int l = 0; l < dim; l++ )
            {
                newTcs[ l ] = new TCNode( -1 );
                nextTcs[ l ] = new TCNode( -1 );
            }
            if( fcount == 0 )
            {
                curtc = genNext_Hilber( fcount, FxOfHilbert, tcsOfHibert, ref inserPos );
                cur_val = ( double ) curtc.t_value;
                genNextFromHilber( cur_val, nextVals, nextTcs, orig_tcsOfNDs, dim, null );
                nextTc = curtc;
            }
            else
            {
                while( i < K )
                {
                    curtc = genNext_Hilber( fcount, FxOfHilbert, tcsOfHibert, ref inserPos );
                    cur_val = ( double ) curtc.t_value;

                    if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                    {
                        //i--;//原地不动
                        continue;
                    }//防止误差引起的问题

                    genNextFromHilber( cur_val, newVals, newTcs, orig_tcsOfNDs, dim, null );
                    dist1 = 0;
                    if( curtc.fx_index == 0 )
                    {
                        for( int l = 0; l < dim; l++ )
                        {
                            dist1 += newVals[ l ] * newVals[ l ];
                        }
                    }
                    else
                    {
                        for( int l = 0; l < dim; l++ )
                        {
                            valD = double.Parse( orig_tcsOfNDs[ l ][ curtc.fx_index - 1 ].ToString() );
                            dist1 += ( newVals[ l ] - valD ) * ( newVals[ l ] - valD );
                        }

                    }
                    minDist = dist1;
                    dist2 = 0;
                    if( curtc.fx_index == fcount )
                    {
                        dist2 += ( newVals[ 0 ] - 1 ) * ( newVals[ 0 ] - 1 );
                        for( int l = 1; l < dim; l++ )
                        {
                            dist2 += newVals[ l ] * newVals[ l ];
                        }


                    }
                    else
                    {
                        for( int l = 0; l < dim; l++ )
                        {
                            valD = double.Parse( orig_tcsOfNDs[ l ][ curtc.fx_index ].ToString() );
                            dist2 += ( newVals[ l ] - valD ) * ( newVals[ l ] - valD );
                        }

                    }
                    if( dist2 < dist1 )
                    {
                        minDist = dist2;
                    }
                    if( minDist > maxDist )
                    {
                        maxDist = minDist;
                        nextTc = curtc;
                        for( int l = 0; l < dim; l++ )
                        {
                            nextVals[ l ] = newVals[ l ];
                            nextTcs[ l ] = newTcs[ l ];
                        }


                    }

                    i++;
                }
            }
            for( int l = 0; l < dim; l++ )
            {
                curvals[ l ] = nextVals[ l ];
                curtcsOfNds[ l ] = nextTcs[ l ];
                orig_tcsOfNDs[ l ].Add( nextVals[ l ] );
            }
            return nextTc;

        }
        public TCNode genNext_Hilber( uint fcount, Fx FxOfHilbert, SingleTClist tcsOfHibert, ref int insPos )
        {
            double tu = mtrand.e0e1genrand();//产生不包括0,1的随机数
            double tval = tu;
            double tval2 = tu;

            TCNode tc = null;
            bool success = false;
            if( FxOfHilbert == null )
            {
                FxOfHilbert = instanceOfFx;
            }
            if( fcount == 0 )
            {
                tc = new TCNode( tval );
                insPos = 0;
                success = true;
            }
            else//按fn分布产生下一个test case
            {
                //int noOfTc = tcs.tclist.Count;
                double tc_val;
                TCNode tc_temp;
                fx2 fx_tmp;
                double coefficient = 0;
                double lr = 0;
                double rr = 1;
                double bcof;
                double[] roots;
                double[] roots2;
                int i;
                for( i = 0; i < fcount + 1; i++ )
                {
                    tc_temp = ( TCNode ) tcsOfHibert.tclist[ i ];
                    tc_val = double.Parse( tc_temp.t_value.ToString() );
                    if( tu > tc_temp.f_high )
                    {
                        fx_tmp = ( fx2 ) FxOfHilbert.fxs[ i ];
                        if( i == 0 )
                        {
                            coefficient += ( fx_tmp.a ) * Math.Pow( fx_tmp.x2, 2.0 ) * ( fx_tmp.x1 / 2.0 - fx_tmp.x2 / 6.0 );

                        }
                        else
                        {
                            coefficient += ( ( -fx_tmp.a ) * Math.Pow( fx_tmp.x2 - fx_tmp.x1, 3.0 ) / 6 );
                        }
                        continue;
                    }
                    else if( tu >= tc_temp.f_low )
                    {
                        insPos = i;
                        fx_tmp = ( fx2 ) FxOfHilbert.fxs[ i ];
                        bcof = ( fx_tmp.a ) * ( fx_tmp.x1 - fx_tmp.x2 ) / 2;
                        double verifyTval;
                        double verify2;
                        if( i == 0 )
                        {
                            // lr = 0 - fx_tmp.x2;
                            lr = -1;
                            rr = 0;
                            bcof = -bcof;
                            coefficient += ( fx_tmp.a ) * Math.Pow( fx_tmp.x2, 2.0 ) * ( fx_tmp.x1 / 2.0 - fx_tmp.x2 / 6.0 );
                        }
                        //else if (i == fcount)
                        //{
                        //    lr = 0;
                        //    rr = 1-fx_tmp.x1;
                        //}
                        else
                        {
                            //lr = 0;
                            //rr = fx_tmp.x2-fx_tmp.x1;
                            lr = 0;
                            rr = 1;
                        }


                        //tval = computeRoot((fx_tmp.a) / 3, bcof, coefficient - tu,0, 1);//有误差问题
                        double x1_tmp = fx_tmp.x1;
                        string errmsg = ">" + x1_tmp.ToString() + "<" + fx_tmp.x2.ToString();
                        if( i == 0 )
                        {
                            x1_tmp = fx_tmp.x2;
                            errmsg = "<" + x1_tmp.ToString();
                        }
                        double a_tmp = ( fx_tmp.a ) / 3;
                        roots = shengjinFormula( a_tmp, bcof, 0, coefficient - tu );
                        // roots2 = shengjinFormula(a_tmp, bcof - 3 * x1_tmp * a_tmp, 3 * a_tmp * x1_tmp * x1_tmp - 2 * x1_tmp * bcof, x1_tmp * x1_tmp * (bcof - a_tmp * x1_tmp) + coefficient - tu);
                        for( int rn = 0; rn < roots.Length; rn++ )
                        {
                            if( i == 0 )
                            {
                                tval = roots[ rn ] + fx_tmp.x2;
                                if( tval >= 0 && tval <= fx_tmp.x2 )
                                {
                                    success = true;
                                    break;
                                }
                            }
                            else
                            {
                                tval = roots[ rn ] + fx_tmp.x1;
                                if( tval >= fx_tmp.x1 && tval <= fx_tmp.x2 && tval <= 1 )//去掉不合理的根
                                {
                                    success = true;
                                    break;

                                }
                                //if (tval >= 1)
                                //{
                                //    tval = 1;
                                //}
                            }



                        }
                        if( success )
                        {
                            tc = new TCNode( tval );
                            tc.fx_val = ( fx_tmp.a ) * ( tval - fx_tmp.x1 ) * ( tval - fx_tmp.x2 );
                            tc.fx_index = i;
                        }


                        break;

                    }



                }



            }
            if( !success )
            {
                tval = -1;//无效值
                tc = new TCNode( tval );
            }

            //tclist.Add(tval);
            return tc;

        }
        /// <summary>
        /// 目前实验的是二维的
        /// </summary>
        /// <param name="hilberVal"></param>
        /// <param name="tcOfNDs"></param>
        /// <param name="origOfNDs"></param>
        /// <param name="dim"></param>
        public void genNextFromHilber( double hilberVal, double[] curvals, TCNode[] tcOfNDs, ArrayList[] origOfNDs, uint dim, SingleTClist[] order_tclist )
        {
            HilbertCurve hilber_curve = new HilbertCurve();
            string bin_str = hilber_curve.Double2Bin( hilberVal );
            double xx = -1;
            double yy = -1;
            int numOfbits = bin_str.Length - 2;
            string hiber_key_valid = null;
            if( numOfbits > 32 )
            {
                numOfbits = 32;
            }
            uint hilber_key = Convert.ToUInt32( bin_str.Substring( 2, numOfbits ), 2 );
            ArrayList valOfnds = hilber_curve.convert_hilbert_key_V1( hilber_key, numOfbits, ref xx, ref yy );
            for( int i = 0; i < dim; i++ )
            {
                curvals[ i ] = double.Parse( valOfnds[ i ].ToString() );
                tcOfNDs[ i ].t_value = curvals[ i ];
                origOfNDs[ i ].Add( curvals[ i ] );
                if( order_tclist != null )
                {
                    order_tclist[ i ].tclist.Add( new TCNode( curvals[ i ] ) );
                }
            }
        }
        /// <summary>
        /// 产生对应于Hilber曲线的一个N维点
        /// </summary>
        /// <param name="hilberVal"></param>
        /// <param name="curvals"></param>
        /// <param name="tcOfNDs"></param>
        /// <param name="origOfNDs"></param>
        /// <param name="dim"></param>
        /// <param name="order_tclist"></param>
        public void genNextFromHilber_V1( double hilberVal, double[] curvals, TCNode[] tcOfNDs, ArrayList[] origOfNDs, uint dim, SingleTClist[] order_tclist )
        {
            HilbertCurve hilber_curve = new HilbertCurve();
            string bin_str = hilber_curve.Double2Bin( hilberVal );

            int numOfbits = bin_str.Length - 2;

            if( numOfbits > 32 )
            {
                numOfbits = 32;
            }
            string[] pointOfstrs = hilber_curve.HilbertCode2Coordinates( bin_str.Substring( 2, numOfbits ), dim );

            for( int i = 0; i < dim; i++ )
            {
                curvals[ i ] = hilber_curve.Bin2Double( pointOfstrs[ i ] );
                tcOfNDs[ i ].t_value = curvals[ i ];
                origOfNDs[ i ].Add( curvals[ i ] );
                if( order_tclist != null )
                {
                    order_tclist[ i ].tclist.Add( new TCNode( curvals[ i ] ) );
                }
            }
        }
        public uint doARTCOS_NDs( uint dim, double[] frs, SingleFault[] faults, uint noOftest )
        {
            bool fail = false;
            SingleTClist[] ordered_tcOfDs = new SingleTClist[ dim ];
            ArrayList[] orig_tcOfDs = new ArrayList[ dim ];
            Fx[] FxOfDs = new Fx[ dim ];
            double[] init_posOfDs = new double[ dim ];
            TCNode[] curtcs = new TCNode[ dim ];
            double[] cur_vals = new double[ dim ];
            TCNode curtc = null;
            double cur_val = -1;

            uint fcount = 0;
            excel = new Excel.Application();
            if( faults == null )
            {
                for( int i = 0; i < dim; i++ )
                {
                    init_posOfDs[ i ] = ( 1.0 - frs[ i ] ) * mtrand.e0e1genrand();

                }
            }
            else
            {
                initPos( dim, faults );
            }
            for( int i = 0; i < dim; i++ )
            {

                if( ordered_tcOfDs[ i ] == null )
                {
                    ordered_tcOfDs[ i ] = new SingleTClist( ( uint ) i );
                    orig_tcOfDs[ i ] = new ArrayList();
                }
                else
                {
                    ordered_tcOfDs[ i ].tclist.Clear();
                    orig_tcOfDs[ i ].Clear();

                }
                if( FxOfDs[ i ] == null )
                {
                    FxOfDs[ i ] = new Fx( "acos(ax+b)" );
                }
            }

            for( int i = 0; i < dim; i++ )
            {
                ordered_tcOfDs[ i ].tclist.Add( new TCNode( 1, 1, 1 ) );
            }
            int flag = 0;
            while( !fail )
            {
                //1.generate the next tc
                for( int i = 0; i < dim; i++ )
                {
                    curtc = genNextCos( ordered_tcOfDs[ i ], fcount, FxOfDs[ i ] );//目前是针对一维的
                    cur_val = ( double ) curtc.t_value;

                    if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                    {
                        i--;//原地不动
                        continue;
                    }//防止误差引起的问题
                    curtcs[ i ] = curtc;
                    cur_vals[ i ] = cur_val;
                    orig_tcOfDs[ i ].Add( cur_val );

                }
                //2.execute the tc
                fcount++;
                if( noOftest == -1 )
                {
                    if( faults == null )
                    {
                        flag = 0;
                        for( int i = 0; i < dim; i++ )
                        {
                            if( cur_vals[ i ] >= double.Parse( init_posOfDs[ i ].ToString() ) && cur_vals[ i ] <= ( double.Parse( init_posOfDs[ i ].ToString() ) + frs[ i ] ) )
                            {
                                flag++;
                            }

                        }

                        if( flag == dim )
                        {

                            fail = true;
                            continue;
                        }
                        flag = 0;
                    }
                    else
                    {
                        fail = executeProg( dim, cur_vals, faults );
                        if( fail )
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    if( fcount >= noOftest )
                        break;
                }
                //3. update the pdfs
                int insertPos = -1;
                for( int i = 0; i < dim; i++ )
                {
                    insertPos = insertTCLX2( ordered_tcOfDs[ i ], curtcs[ i ] );
                    computeFxCos( ordered_tcOfDs[ i ], FxOfDs[ i ], insertPos );
                }


            }
            if( noOftest != -1 )
            {
                testcase2excel( dim, ordered_tcOfDs, frs, fcount );
            }
            //excel.Quit();
            return fcount;

        }
        /// <summary>
        /// 将test case 输出到excel
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="orderTces"></param>
        /// <param name="failurerate"></param>
        /// <param name="fcnt"></param>
        public void testcase2excel( uint dim, SingleTClist[] orderTces, double[] failurerates, uint fcnt )
        {
            string excelname = "tcs.xls";

            Excel.Workbooks workbooks;
            object miss = Missing.Value;
            Excel.Sheets sheets;
            Excel._Worksheet sheet = null;
            workbooks = excel.Workbooks;
            workbooks.Open( Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss );
            sheets = excel.Worksheets;
            sheet = ( Excel._Worksheet ) sheets.get_Item( 1 );
            excel.DisplayAlerts = false;

            double init_s = 0;
            double init_end = 0;
            double sub_cnt;
            double tval;
            int noOfrange = 0;
            for( int i = 0; i < dim; i++ )
            {
                sheet.Cells[ 1, i + 2 ] = i + 1;
                init_s = 0;
                init_end = init_end + failurerates[ i ];
                sub_cnt = 0;
                noOfrange = 0;
                for( int j = 0; j < fcnt; )
                {
                    tval = double.Parse( ( ( TCNode ) ( orderTces[ i ].tclist[ j ] ) ).t_value.ToString() );
                    if( tval >= init_s && tval <= init_end )
                    {
                        sub_cnt = sub_cnt + 1;
                        j++;
                    }
                    else
                    {
                        sheet.Cells[ noOfrange + 2, i + 1 ] = init_end.ToString();
                        sheet.Cells[ noOfrange + 2, i + 2 ] = sub_cnt / ( double ) fcnt;
                        noOfrange++;
                        //sub_cnt = 1;

                        sub_cnt = 0;
                        init_s = init_end;
                        init_end = init_end + failurerates[ i ];
                    }
                }
                sheet.Cells[ noOfrange + 2, i + 1 ] = init_end.ToString();
                sheet.Cells[ noOfrange + 2, i + 2 ] = sub_cnt / ( double ) fcnt;
            }
            sheet.SaveAs( Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss );
            excel.Workbooks.Close();



        }
        /// <summary>
        /// 以二次函数作为pdf,inspos为插入位置
        /// </summary>
        /// <param name="tcs"></param>
        /// <returns></returns>
        public TCNode genNextX2( SingleTClist tcs, uint fcount, Fx insofFx, ref int inspos )
        {
            //Console.Out.WriteLine("start genNextX2!!!");
            double tu = mtrand.e0e1genrand();//产生不包括0,1的随机数
            double tval = tu;
            double tval2 = tu;

            TCNode tc = null;
            bool success = false;
            if( insofFx == null )
            {
                insofFx = instanceOfFx;
            }
            if( fcount == 0 )
            {
                tc = new TCNode( tval );
                inspos = 0;
                success = true;

            }
            else//按fn分布产生下一个test case
            {
                //int noOfTc = tcs.tclist.Count;
                double tc_val;
                TCNode tc_temp;
                fx2 fx_tmp;
                double coefficient = 0;
                double lr = 0;
                double rr = 1;
                double bcof;
                double[] roots;
                double[] roots2;
                int i;
                for( i = 0; i < fcount + 1; i++ )
                {
                    tc_temp = ( TCNode ) tcs.tclist[ i ];
                    tc_val = double.Parse( tc_temp.t_value.ToString() );
                    if( tu > tc_temp.f_high )
                    {
                        fx_tmp = ( fx2 ) insofFx.fxs[ i ];
                        if( i == 0 )
                        {
                            coefficient += ( fx_tmp.a ) * Math.Pow( fx_tmp.x2, 2.0 ) * ( fx_tmp.x1 / 2.0 - fx_tmp.x2 / 6.0 );

                        }
                        else
                        {
                            coefficient += ( ( -fx_tmp.a ) * Math.Pow( fx_tmp.x2 - fx_tmp.x1, 3.0 ) / 6 );
                        }
                        continue;
                    }
                    else if( tu >= tc_temp.f_low )
                    {

                        inspos = i;
                        fx_tmp = ( fx2 ) insofFx.fxs[ i ];
                        bcof = ( fx_tmp.a ) * ( fx_tmp.x1 - fx_tmp.x2 ) / 2;
                        double verifyTval;
                        double verify2;
                        if( i == 0 )
                        {
                            // lr = 0 - fx_tmp.x2;
                            lr = -1;
                            rr = 0;
                            bcof = -bcof;
                            coefficient += ( fx_tmp.a ) * Math.Pow( fx_tmp.x2, 2.0 ) * ( fx_tmp.x1 / 2.0 - fx_tmp.x2 / 6.0 );
                        }
                        //else if (i == fcount)
                        //{
                        //    lr = 0;
                        //    rr = 1-fx_tmp.x1;
                        //}
                        else
                        {
                            //lr = 0;
                            //rr = fx_tmp.x2-fx_tmp.x1;
                            lr = 0;
                            rr = 1;
                        }


                        //tval = computeRoot((fx_tmp.a) / 3, bcof, coefficient - tu,0, 1);//有误差问题
                        double x1_tmp = fx_tmp.x1;
                        string errmsg = ">" + x1_tmp.ToString() + "<" + fx_tmp.x2.ToString();
                        if( i == 0 )
                        {
                            x1_tmp = fx_tmp.x2;
                            errmsg = "<" + x1_tmp.ToString();
                        }
                        double a_tmp = ( fx_tmp.a ) / 3;
                        roots = shengjinFormula( a_tmp, bcof, 0, coefficient - tu );

                        for( int rn = 0; rn < roots.Length; rn++ )
                        {
                            if( i == 0 )
                            {
                                tval = roots[ rn ] + fx_tmp.x2;
                                if( tval >= 0 && tval <= fx_tmp.x2 )
                                {
                                    success = true;
                                    break;
                                }
                            }
                            else
                            {
                                tval = roots[ rn ] + fx_tmp.x1;
                                if( tval >= fx_tmp.x1 && tval <= fx_tmp.x2 && tval <= 1 )//去掉不合理的根
                                {
                                    success = true;
                                    break;

                                }
                                //if (tval >= 1)
                                //{
                                //    tval = 1;
                                //}
                            }



                        }
                        if( success )
                        {
                            tc = new TCNode( tval );
                            tc.fx_val = ( fx_tmp.a ) * ( tval - fx_tmp.x1 ) * ( tval - fx_tmp.x2 );
                        }


                        break;

                    }



                }
                //if (i == fcount+2)
                //    tval = (tu - 2 * shape_a * Math.Pow(ex_b, 3) * fcount / 3) / pdfU + 2 * ex_b * fcount;
                //tc = new TCNode(tval);


            }
            if( !success )
            {
                tval = -1;//无效值
                tc = new TCNode( tval );

            }

            //tclist.Add(tval);
            return tc;

        }
        public TCNode genNextK_X2( SingleTClist tcs, uint fcount, Fx insofFx, int K )
        {
            int i = 0;
            TCNode nextTC = null;
            TCNode curtc;
            double maxfx = -1;
            double cur_val;
            int insPos = -1;
            if( fcount == 0 )
            {
                return genNextX2( tcs, fcount, insofFx, ref insPos );
            }
            int inserPos = -1;
            while( i < K )
            {
                curtc = genNextX2( tcs, fcount, insofFx, ref inserPos );
                cur_val = ( double ) curtc.t_value;

                if( Double.IsNaN( cur_val ) || cur_val >= 1 || cur_val <= 0 )
                {
                    //i--;//原地不动
                    continue;
                }//防止误差引起的问题
                if( curtc.fx_val > maxfx )
                {
                    nextTC = curtc;
                    maxfx = curtc.fx_val;
                }
                i++;
            }
            return nextTC;


        }
        public TCNode genNextX2_V1( SingleTClist tcs, uint fcount, Fx insofFx )
        {
            double tu = mtrand.e0e1genrand();//产生不包括0,1的随机数
            double tval = tu;
            double tval2 = tu;

            TCNode tc;
            bool success = false;
            if( insofFx == null )
            {
                insofFx = instanceOfFx;
            }
            if( fcount == 0 )
            {
                //tc = new TCNode(tval);
                success = true;
            }
            else//按fn分布产生下一个test case
            {
                //int noOfTc = tcs.tclist.Count;
                double tc_val;
                TCNode tc_temp;
                fx2 fx_tmp;
                double coefficient = 0;
                double lr = 0;
                double rr = 1;

                double[] roots;
                double[] roots2;
                int i;
                for( i = 0; i < fcount + 1; i++ )
                {
                    tc_temp = ( TCNode ) tcs.tclist[ i ];
                    tc_val = double.Parse( tc_temp.t_value.ToString() );
                    if( tu > tc_temp.f_high )
                    {
                        fx_tmp = ( fx2 ) insofFx.fxs[ i ];
                        coefficient += fx_tmp.s;

                        continue;
                    }
                    else if( tu >= tc_temp.f_low )
                    {
                        fx_tmp = ( fx2 ) insofFx.fxs[ i ];
                        coefficient = coefficient + ( fx_tmp.a / 6.0 ) * ( Math.Pow( fx_tmp.x1, 2 ) ) * ( fx_tmp.x1 - 3 * fx_tmp.x2 );

                        double a_tmp = ( fx_tmp.a ) / 3;
                        double bcof = -( fx_tmp.a ) * ( fx_tmp.x1 + fx_tmp.x2 ) / 2;
                        double ccof = ( fx_tmp.a ) * ( fx_tmp.x1 ) * ( fx_tmp.x2 );
                        roots = shengjinFormula( a_tmp, bcof, ccof, coefficient - tu );

                        for( int rn = 0; rn < roots.Length; rn++ )
                        {
                            tval = roots[ rn ];


                            if( tval >= fx_tmp.x1 && tval <= fx_tmp.x2 && tval <= 1 )//去掉不合理的根
                            {
                                success = true;
                                break;

                            }




                        }



                        break;

                    }



                }
                //if (i == fcount+2)
                //    tval = (tu - 2 * shape_a * Math.Pow(ex_b, 3) * fcount / 3) / pdfU + 2 * ex_b * fcount;
                //tc = new TCNode(tval);


            }
            if( !success )
            {
                tval = -1;//无效值
            }
            tc = new TCNode( tval );
            //tclist.Add(tval);
            return tc;

        }
        public bool computeFxX2_V3( SingleTClist tcl, Fx insOfFx, int insPos )
        {
          //  Console.Out.WriteLine("start computeFxX2_V3!!!");
            int noOfTcs = tcl.tclist.Count;//最后一个是1，不是tc
            bool enExcel = true;
            string excelname = "pdfsX2_V3.xls";

            Excel.Workbooks workbooks=null;
            object miss = Missing.Value;
            Excel.Sheets sheets=null;
            Excel._Worksheet sheet = null;
            if( noOfTcs > 120 )
                enExcel = false;
            if( enExcel )
            {

                //workbooks = excel.Workbooks;
                //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
                //sheets = excel.Worksheets;
                //sheet = (Excel._Worksheet)sheets.get_Item(1);
                //excel.DisplayAlerts = false;
                //sheet.Cells[1, noOfTcs - 1] = noOfTcs;

            }

            if( insOfFx.fxs == null )
            {
                insOfFx.fxs = new ArrayList();
                // insOfFx.fxs.Add(new fx2(0, 1));
            }
            else
            {
                // insOfFx.fxs.Clear();

            }
            fx2 fx2tmp;
            TCNode tccur;
            double sumOfdenomi = 0;

            Random randOfroot = new Random();
            tccur = ( TCNode ) tcl.tclist[ 0 ];  //第一个测试用例
            double x1 = double.Parse( tccur.t_value.ToString() );
            double r0 = -x1 * randOfroot.NextDouble();
            sumOfdenomi = Math.Pow( x1, 2.0 ) * ( -x1 / 6.0 + r0 / 2.0 );//0到x1 的积分

            tccur = ( TCNode ) tcl.tclist[ noOfTcs - 2 ];  //最后一个测试用例
            double xn = double.Parse( tccur.t_value.ToString() );
            double rn = ( 1 - xn ) * randOfroot.NextDouble() + 1;
            sumOfdenomi += Math.Pow( 1 - xn, 2.0 ) * ( ( 1 - xn ) / 3.0 + ( xn - rn ) / 2.0 );

            double x2 = -1;

            if( noOfTcs == 2 )
            {

           /*     for( int i = 1; i < noOfTcs - 1; i++ )
                {
                    tccur = ( TCNode ) tcl.tclist[ i ];
                    x2 = double.Parse( tccur.t_value.ToString() );
                    sumOfdenomi += -Math.Pow( x2 - x1, 3 ) / 6.0;
                    x1 = x2;


                }     */
            }
            else
            {
                double comput_denomi = insOfFx.sumsOfintervals;
                fx2 fx_tmp = null;
                if( insPos == 0 )
                {
                    fx_tmp = ( fx2 ) insOfFx.fxs[ 0 ];
                    comput_denomi -= Math.Pow( fx_tmp.x2, 2.0 ) * ( -fx_tmp.x2 / 6.0 + fx_tmp.x1 / 2.0 );
                    comput_denomi += Math.Pow( x1, 2.0 ) * ( -x1 / 6.0 + r0 / 2.0 );

                    tccur = ( TCNode ) tcl.tclist[ 1 ];
                    x2 = double.Parse( tccur.t_value.ToString() );
                    comput_denomi += -Math.Pow( x2 - x1, 3 ) / 6.0;

                    fx_tmp = ( fx2 ) insOfFx.fxs[ noOfTcs - 2 ];
                    comput_denomi -= Math.Pow( 1 - fx_tmp.x1, 2.0 ) * ( ( 1 - fx_tmp.x1 ) / 3.0 + ( fx_tmp.x1 - fx_tmp.x2 ) / 2.0 );
                    comput_denomi += Math.Pow( 1 - xn, 2.0 ) * ( ( 1 - xn ) / 3.0 + ( xn - rn ) / 2.0 );


                }
                else if( insPos > 0 && insPos < noOfTcs - 2 )
                {
                    fx_tmp = ( fx2 ) insOfFx.fxs[ 0 ];
                    comput_denomi -= Math.Pow( fx_tmp.x2, 2.0 ) * ( -fx_tmp.x2 / 6.0 + fx_tmp.x1 / 2.0 );
                    comput_denomi += Math.Pow( x1, 2.0 ) * ( -x1 / 6.0 + r0 / 2.0 );

                    fx_tmp = ( fx2 ) insOfFx.fxs[ insPos ];
                    comput_denomi -= -Math.Pow( fx_tmp.x2 - fx_tmp.x1, 3 ) / 6.0;
                    tccur = ( TCNode ) tcl.tclist[ insPos ];
                    x2 = double.Parse( tccur.t_value.ToString() );
                    comput_denomi += -Math.Pow( x2 - fx_tmp.x1, 3 ) / 6.0 - Math.Pow( fx_tmp.x2 - x2, 3 ) / 6.0;

                    fx_tmp = ( fx2 ) insOfFx.fxs[ noOfTcs - 2 ];
                    comput_denomi -= Math.Pow( 1 - fx_tmp.x1, 2.0 ) * ( ( 1 - fx_tmp.x1 ) / 3.0 + ( fx_tmp.x1 - fx_tmp.x2 ) / 2.0 );
                    comput_denomi += Math.Pow( 1 - xn, 2.0 ) * ( ( 1 - xn ) / 3.0 + ( xn - rn ) / 2.0 );




                }
                else if( insPos == noOfTcs - 2 )
                {
                    fx_tmp = ( fx2 ) insOfFx.fxs[ 0 ];
                    comput_denomi -= Math.Pow( fx_tmp.x2, 2.0 ) * ( -fx_tmp.x2 / 6.0 + fx_tmp.x1 / 2.0 );
                    comput_denomi += Math.Pow( x1, 2.0 ) * ( -x1 / 6.0 + r0 / 2.0 );

                    fx_tmp = ( fx2 ) insOfFx.fxs[ noOfTcs - 2 ];
                    comput_denomi -= Math.Pow( 1 - fx_tmp.x1, 2.0 ) * ( ( 1 - fx_tmp.x1 ) / 3.0 + ( fx_tmp.x1 - fx_tmp.x2 ) / 2.0 );

                    tccur = ( TCNode ) tcl.tclist[ insPos ];
                    x2 = double.Parse( tccur.t_value.ToString() );
                    comput_denomi += -Math.Pow( x2 - fx_tmp.x1, 3 ) / 6.0;
                    comput_denomi += Math.Pow( 1 - xn, 2.0 ) * ( ( 1 - xn ) / 3.0 + ( xn - rn ) / 2.0 );


                }
                sumOfdenomi = comput_denomi;

            }

            //tccur = (TCNode)tcl.tclist[noOfTcs - 2];
            //double xn = double.Parse(tccur.t_value.ToString());
            //double rn = (1 - xn) * randOfroot.NextDouble() + 1;
            //sumOfdenomi += Math.Pow(1 - xn, 2.0) * ((1 - xn) / 3.0 + (xn - rn) / 2.0);



            double acof = 1.0 / sumOfdenomi;//二次项系数
            double ss = -1;
            insOfFx.sumsOfintervals = sumOfdenomi;


            if( insOfFx.fxs == null )
            {
                insOfFx.fxs = new ArrayList();
                // insOfFx.fxs.Add(new fx2(0, 1));
            }
            else
            {
                insOfFx.fxs.Clear();

            }
            double flow = 0;
            //double fhig = 0;
            x1 = 0;
            for( int i = 0; i < noOfTcs; i++ )
            {
                tccur = ( TCNode ) tcl.tclist[ i ];

                x2 = double.Parse( tccur.t_value.ToString() );
                if( i == 0 )
                {
                    fx2tmp = new fx2( r0, x2 );
                    fx2tmp.a = acof;
                    fx2tmp.s = acof * Math.Pow( x2, 2.0 ) * ( -x2 / 6.0 + r0 / 2.0 );
                }
                else if( i == noOfTcs - 1 )
                {
                    fx2tmp = new fx2( x1, rn );
                    fx2tmp.a = acof;
                    fx2tmp.s = acof * Math.Pow( 1 - x1, 2.0 ) * ( ( 1 - x1 ) / 3.0 + ( x1 - rn ) / 2.0 );
                }
                else
                {
                    fx2tmp = new fx2( x1, x2 );
                    fx2tmp.a = acof;
                    //下面这句话原本放在括号外面，应该是个错误
                    fx2tmp.s = -acof / 6.0 * Math.Pow(x2 - x1, 3);
                }


               //这句话原本在这个位置
                //fx2tmp.s = -acof / 6.0 * Math.Pow(x2 - x1, 3);
                insOfFx.fxs.Add( fx2tmp );

                tccur.f_low = flow;
                tccur.f_high = flow + fx2tmp.s;
                flow = tccur.f_high;
                x1 = x2;


            }
            tccur = ( TCNode ) tcl.tclist[ noOfTcs - 1 ];
            tccur.f_high = 1;

            if( enExcel )
            {
                //double lrv = 0, rrv = 1;
                //for (int i = 0; i < insOfFx.fxs.Count; i++)
                //{
                //    fx2tmp = (fx2)insOfFx.fxs[i];

                //    lrv = fx2tmp.x1;
                //    rrv = fx2tmp.x2;

                //    sheet.Cells[i + 2, noOfTcs - 1] = "(" + lrv.ToString() + "," + rrv.ToString() + ");" + fx2tmp.a.ToString() + "*(x-(" + fx2tmp.x1.ToString() + "))*(x-(" + fx2tmp.x2.ToString() + "))";


                //}
                //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

                //excel.Workbooks.Close();
                //excel.Quit();

            }


            return true;


        }
        public TCNode genNextCos( SingleTClist tcs, uint fcount, Fx insofFx )
        {
            double tu = mtrand.e0e1genrand();//产生不包括0,1的随机数
            double tval = tu;
            double tval2 = tu;

            TCNode tc;
            bool success = false;
            if( insofFx == null )
            {
                insofFx = instanceOfFx;
            }
            if( fcount == 0 )
            {
                //tc = new TCNode(tval);
                success = true;
            }
            else//按fn分布产生下一个test case
            {
                //int noOfTc = tcs.tclist.Count;
                double tc_val;
                TCNode tc_temp;
                fcos fx_tmp;
                double coefficient = 0;
                //double lr = 0;
                //double rr = 1;

                int i;
                for( i = 0; i < fcount + 1; i++ )
                {
                    tc_temp = ( TCNode ) tcs.tclist[ i ];
                    tc_val = double.Parse( tc_temp.t_value.ToString() );
                    if( tu > tc_temp.f_high )
                    {
                        fx_tmp = ( fcos ) insofFx.fxs[ i ];
                        coefficient += fx_tmp.s;

                        continue;
                    }
                    else if( tu >= tc_temp.f_low )
                    {
                        fx_tmp = ( fcos ) insofFx.fxs[ i ];



                        coefficient = coefficient - fx_tmp.A / fx_tmp.acof * Math.Sin( fx_tmp.acof * fx_tmp.lr + fx_tmp.bcof );

                        double sin_atb = ( tu - coefficient ) * ( fx_tmp.acof ) / ( fx_tmp.A );
                        double atb = Math.Asin( sin_atb );
                        double kmin = Math.Floor( ( ( fx_tmp.acof ) * ( fx_tmp.lr ) + fx_tmp.bcof - atb ) / ( 2 * Math.PI ) );
                        double kmax = Math.Ceiling( ( ( fx_tmp.acof ) * ( fx_tmp.rr ) + fx_tmp.bcof - atb ) / ( 2 * Math.PI ) );
                        long min = long.Parse( kmin.ToString() );
                        long max = long.Parse( kmax.ToString() );
                        double tval_tmp;
                        //bool flag = false;
                        for( long j = min; j <= max; j++ )
                        {
                            tval_tmp = ( 2 * j * Math.PI + atb - fx_tmp.bcof ) / fx_tmp.acof;
                            if( tval_tmp >= fx_tmp.lr && tval_tmp <= fx_tmp.rr )
                            {
                                tval = tval_tmp;
                                success = true;
                                break;
                            }

                        }
                        if( !success )
                        {
                            kmin = Math.Floor( ( ( fx_tmp.acof ) * ( fx_tmp.lr ) + fx_tmp.bcof + atb - Math.PI ) / ( 2 * Math.PI ) );
                            kmax = Math.Ceiling( ( ( fx_tmp.acof ) * ( fx_tmp.rr ) + fx_tmp.bcof + atb - Math.PI ) / ( 2 * Math.PI ) );
                            min = long.Parse( kmin.ToString() );
                            max = long.Parse( kmax.ToString() );
                            for( long j = min; j <= max; j++ )
                            {
                                tval_tmp = ( 2 * j * Math.PI + Math.PI - atb - fx_tmp.bcof ) / fx_tmp.acof;
                                if( tval_tmp >= fx_tmp.lr && tval_tmp <= fx_tmp.rr )
                                {
                                    tval = tval_tmp;
                                    success = true;
                                    break;
                                }

                            }

                        }




                        break;

                    }



                }
                //if (i == fcount+2)
                //    tval = (tu - 2 * shape_a * Math.Pow(ex_b, 3) * fcount / 3) / pdfU + 2 * ex_b * fcount;
                //tc = new TCNode(tval);


            }
            if( !success )
            {
                tval = -1;//无效值
            }
            tc = new TCNode( tval );
            //tclist.Add(tval);
            return tc;

        }
        public double computeRoot( double acof, double bcof, double cof, double lrange, double rrange )
        {
            DHRT solve_root = new DHRT( lrange, rrange, 0.0000001, 0.01 );
            solve_root.dh_root( acof, bcof, cof );
            return double.Parse( ( solve_root.values[ 0 ] ).ToString() );

        }
        public double[] shengjinFormula( double acof, double bcof, double cof, double dof )
        {
            double A = bcof * bcof - 3 * acof * cof;//A=b^2-3ac
            double B = bcof * cof - 9 * acof * dof;// B=bc-9ad
            double C = cof * cof - 3 * bcof * dof;//C=c^2-3bd
            double delta = B * B - 4 * A * C;
            double root = 0;
            double r1 = 0;
            double r2 = 0;
            double[] roots = new double[ 3 ];
            if( delta > 0 )
            {
                double Y1 = A * bcof + 3.0 * acof * ( -B + Math.Sqrt( B * B - 4 * A * C ) ) / 2.0;
                double Y2 = A * bcof + 3.0 * acof * ( -B - Math.Sqrt( B * B - 4 * A * C ) ) / 2.0;
                double powY1;
                double powY2;
                if( Y1 < 0 )
                {
                    powY1 = -Math.Pow( -Y1, 1.0 / 3.0 );
                }
                else
                {
                    powY1 = Math.Pow( Y1, 1.0 / 3.0 );
                }
                if( Y2 < 0 )
                {
                    powY2 = -Math.Pow( -Y2, 1.0 / 3.0 );
                }
                else
                {
                    powY2 = Math.Pow( Y2, 1.0 / 3.0 );
                }
                root = ( -bcof - powY1 - powY2 ) / ( 3.0 * acof );
                r1 = root;
                r2 = root;
            }
            else if( delta == 0 )
            {
                root = -bcof / acof + B / A;
                r1 = -B / ( 2 * A );
                r2 = r1;

            }
            else if( delta < 0 )
            {
                double T = ( 2 * A * bcof - 3 * acof * B ) / ( 2 * Math.Pow( A, 3.0 / 2.0 ) );
                double theta = Math.Acos( T );
                root = ( -bcof - 2 * Math.Sqrt( A ) * Math.Cos( theta / 3 ) ) / ( 3.0 * acof );
                r1 = ( -bcof + Math.Sqrt( A ) * ( Math.Cos( theta / 3 ) + Math.Sqrt( 3 ) * Math.Sin( theta / 3 ) ) ) / ( 3 * acof );
                r2 = ( -bcof + Math.Sqrt( A ) * ( Math.Cos( theta / 3 ) - Math.Sqrt( 3 ) * Math.Sin( theta / 3 ) ) ) / ( 3 * acof );
            }
            roots[ 0 ] = root;
            roots[ 1 ] = r1;
            roots[ 2 ] = r2;
            return roots;
        }
        public int insertTCLX2( SingleTClist tcl, TCNode tc )
        {
            int noOfTc = tcl.tclist.Count;
            int insertPos = noOfTc;
            TCNode tmp_tc = null;
            for( int i = 0; i < noOfTc; i++ )
            {
                tmp_tc = ( TCNode ) tcl.tclist[ i ];
                if( double.Parse( tc.t_value.ToString() ) < double.Parse( tmp_tc.t_value.ToString() ) )
                {

                    insertPos = i;
                    break;
                }
            }
            tcl.tclist.Insert( insertPos, tc );
            return insertPos;


        }
        public void insertTCLX2_V2( SingleTClist tcl, TCNode tc, int inspos )
        {
            //int noOfTc = tcl.tclist.Count;
            //int insertPos = noOfTc;
            //TCNode tmp_tc = null;
            //for (int i = 0; i < noOfTc; i++)
            //{
            //    tmp_tc = (TCNode)tcl.tclist[i];
            //    if (double.Parse(tc.t_value.ToString()) < double.Parse(tmp_tc.t_value.ToString()))
            //    {

            //        insertPos = i;
            //        break;
            //    }
            //}
            tcl.tclist.Insert( inspos, tc );
            //  return insertPos;


        }

        public bool computeFxX2( SingleTClist tcl, Fx insOfFx, int insPos )
        {

            int noOfTcs = tcl.tclist.Count;//最后一个是1，不是tc
            bool enExcel = true;
            string excelname = "pdfs.xls";

            Excel.Workbooks workbooks;
            object miss = Missing.Value;
            Excel.Sheets sheets;
            Excel._Worksheet sheet = null;
            if( noOfTcs > 120 )
                enExcel = false;
            if( enExcel )
            {

                //workbooks = excel.Workbooks;
                //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
                //sheets = excel.Worksheets;
                //sheet = (Excel._Worksheet)sheets.get_Item(1);
                //excel.DisplayAlerts = false;
                //sheet.Cells[1, noOfTcs - 1] = noOfTcs;
                //sheet.Cells[0, 1] = "lr";
                //sheet.Cells[0, 2] = "rr";
                //sheet.Cells[0, 3] = "A";
                //sheet.Cells[0, 4] = "a";
                //sheet.Cells[0, 5] = "b";
            }

            if( insOfFx.fxs == null )
            {
                insOfFx.fxs = new ArrayList();
                insOfFx.fxs.Add( new fx2( 0, 1 ) );
            }
            else
            {
                //insOfFx.fxs.Clear();

            }

            fx2 fx2tmp;
            TCNode tccur;
            TCNode tcpre;
            TCNode insNew;

            if( noOfTcs == 0 )
                return false;
            /*1. set the area of each pdf*/
            //f0
            if( insPos != 0 )
            {
                tccur = ( TCNode ) tcl.tclist[ 0 ];
                fx2tmp = new fx2( 0.0, ( double ) tccur.t_value );
                fx2tmp.s = ( double ) tccur.t_value;
                //insOfFx.fxs.Add(fcostmp);
                insOfFx.fxs[ 0 ] = fx2tmp;
            }
            //tccur = (TCNode)tcl.tclist[0];
            //fx2tmp = new fx2(0, (double)tccur.t_value);
            //fx2tmp.s = fx2tmp.x2 - fx2tmp.x1;
            //insOfFx.fxs.Add(fx2tmp);
            //fi(1<=i<=n-1)
            insNew = ( TCNode ) tcl.tclist[ insPos ];

            if( insPos == 0 )
            {
                fx2tmp = new fx2( 0.0, ( double ) insNew.t_value );
                fx2tmp.s = ( double ) insNew.t_value;

            }
            else
            {
                tcpre = ( TCNode ) tcl.tclist[ insPos - 1 ];
                fx2tmp = new fx2( double.Parse( tcpre.t_value.ToString() ), double.Parse( insNew.t_value.ToString() ) );
                fx2tmp.s = fx2tmp.x2 - fx2tmp.x1;
            }
            insOfFx.fxs.Insert( insPos, fx2tmp );
            if( insPos < noOfTcs - 2 )
            {
                tccur = ( TCNode ) tcl.tclist[ insPos + 1 ];
                tcpre = ( TCNode ) tcl.tclist[ insPos ];
                fx2tmp = new fx2( double.Parse( tcpre.t_value.ToString() ), double.Parse( tccur.t_value.ToString() ) );
                fx2tmp.s = double.Parse( tccur.t_value.ToString() ) - double.Parse( tcpre.t_value.ToString() );
                insOfFx.fxs[ insPos + 1 ] = fx2tmp;
            }
            //for (int i = 1; i < noOfTcs - 1; i++)
            //{
            //    tccur = (TCNode)tcl.tclist[i];
            //    tcpre = (TCNode)tcl.tclist[i - 1];
            //    fx2tmp = new fx2((double)tcpre.t_value, (double)tccur.t_value);
            //    fx2tmp.s = fx2tmp.x2 - fx2tmp.x1;
            //    insOfFx.fxs.Add(fx2tmp);

            //}
            //fn
            tccur = ( TCNode ) tcl.tclist[ noOfTcs - 2 ];
            fx2tmp = new fx2( ( double ) tccur.t_value, 1 );
            fx2tmp.s = fx2tmp.x2 - fx2tmp.x1;
            //insOfFx.fxs.Add(fx2tmp);
            insOfFx.fxs[ noOfTcs - 1 ] = fx2tmp;

            /* 2.compute the parameters of each pdf*/
            Random randOfpdf = new Random();
            int noOffx = insOfFx.fxs.Count;
            //f0
            fx2tmp = ( fx2 ) insOfFx.fxs[ 0 ];
            double x1 = fx2tmp.x2;
            double r0 = -x1 * randOfpdf.NextDouble();
            double a0 = ( fx2tmp.s ) * 6 / ( 3 * r0 * Math.Pow( x1, 2.0 ) - Math.Pow( x1, 3.0 ) );
            fx2tmp.a = a0;
            fx2tmp.x1 = r0;
            //fi(1<=i<=n-1)
            if( insPos != 0 )
            {
                fx2tmp = ( fx2 ) insOfFx.fxs[ insPos ];
                fx2tmp.a = -6 * ( fx2tmp.s ) / Math.Pow( fx2tmp.s, 3.0 );
            }
            if( insPos < noOfTcs - 2 )
            {
                fx2tmp = ( fx2 ) insOfFx.fxs[ insPos + 1 ];
                fx2tmp.a = -6 * ( fx2tmp.s ) / Math.Pow( fx2tmp.s, 3.0 );
            }
            //for (int i = 1; i < noOffx - 1; i++)
            //{
            //    fx2tmp = (fx2)insOfFx.fxs[i];
            //    fx2tmp.a = -6 * (fx2tmp.s) / Math.Pow(fx2tmp.s, 3.0);
            //}
            //fn
            fx2tmp = ( fx2 ) insOfFx.fxs[ noOffx - 1 ];
            double xn = fx2tmp.x1;
            double rn = ( 1 - xn ) * randOfpdf.NextDouble() + 1;
            double an = 6 * ( fx2tmp.s ) / ( Math.Pow( 1 - xn, 3.0 ) - 3 * rn * Math.Pow( 1 - xn, 2 ) + 3 * xn * Math.Pow( 1 - xn, 2 ) );
            fx2tmp.x2 = rn;
            fx2tmp.a = an;

            if( enExcel )
            {
                //double lrv=0, rrv=1;
                //for (int i = 0; i < insOfFx.fxs.Count; i++)
                //{
                //    fx2tmp = (fx2)insOfFx.fxs[i];
                //    if (i != 0)
                //    {
                //        lrv = fx2tmp.x1;
                //    }
                //    else
                //    {
                //        lrv = 0;
                //    }
                //    if (i != noOfTcs - 1)
                //    {
                //        rrv = fx2tmp.x2;
                //    }
                //    else
                //    {
                //        rrv = 1;
                //    }
                //    sheet.Cells[i + 2, noOfTcs - 1] = "(" + lrv.ToString() + "," + rrv.ToString() + ");" + fx2tmp.a.ToString() + "*(x-(" + fx2tmp.x1.ToString()+ "))*(x-(" + fx2tmp.x2.ToString() + "))";


                //}
                //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

                //excel.Workbooks.Close();
                // excel.Quit();

            }

            /*3.compute F(low),F(high) */
            double tc_val;
            TCNode tc_temp;
            //f0
            if( insPos == 0 )
            {
                tc_temp = ( TCNode ) tcl.tclist[ 0 ];
                tc_val = ( double ) tc_temp.t_value;
                fx2tmp = ( fx2 ) insOfFx.fxs[ 0 ];
                tc_temp.f_low = 0;
                tc_temp.f_high = ( fx2tmp.a ) * Math.Pow( fx2tmp.x2, 2.0 ) * ( fx2tmp.x2 / 3 - ( fx2tmp.x2 - fx2tmp.x1 ) / 2 );
                insPos = insPos + 1;
            }
            else
            {
                tc_temp = ( TCNode ) tcl.tclist[ insPos - 1 ];
            }


            double prehigh = tc_temp.f_high;
            //fi
            for( int i = insPos; i < noOfTcs - 1; i++ )
            {

                tc_temp = ( TCNode ) tcl.tclist[ i ];
                tc_val = ( double ) tc_temp.t_value;
                fx2tmp = ( fx2 ) insOfFx.fxs[ i ];
                tc_temp.f_low = prehigh;
                // tc_temp.f_high = prehigh - (fx2tmp.a) * Math.Pow(fx2tmp.x2 - fx2tmp.x1, 3.0) / 6;
                tc_temp.f_high = prehigh + fx2tmp.s;
                prehigh = tc_temp.f_high;

            }

            //fn
            tc_temp = ( TCNode ) tcl.tclist[ noOfTcs - 1 ];
            tc_temp.f_low = prehigh;
            tc_temp.f_high = 1;
            return true;


        }
        public bool computeFxX2_V1( SingleTClist tcl, Fx insOfFx, int insPos )
        {

            int noOfTcs = tcl.tclist.Count;//最后一个是1，不是tc
            bool enExcel = true;
            string excelname = "pdfs.xls";

            Excel.Workbooks workbooks;
            object miss = Missing.Value;
            Excel.Sheets sheets;
            Excel._Worksheet sheet = null;
            if( noOfTcs > 120 )
                enExcel = false;
            if( enExcel )
            {

                //workbooks = excel.Workbooks;
                //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
                //sheets = excel.Worksheets;
                //sheet = (Excel._Worksheet)sheets.get_Item(1);
                //excel.DisplayAlerts = false;
                //sheet.Cells[1, noOfTcs - 1] = noOfTcs;
                //sheet.Cells[0, 1] = "lr";
                //sheet.Cells[0, 2] = "rr";
                //sheet.Cells[0, 3] = "A";
                //sheet.Cells[0, 4] = "a";
                //sheet.Cells[0, 5] = "b";
            }

            if( insOfFx.fxs == null )
            {
                insOfFx.fxs = new ArrayList();
                // insOfFx.fxs.Add(new fx2(0, 1));
            }
            else
            {
                insOfFx.fxs.Clear();

            }
            double x1 = 0;
            double x2 = -1;
            double sumOfdenomi = 0;
            TCNode tccur;
            for( int i = 0; i < noOfTcs; i++ )
            {
                tccur = ( TCNode ) tcl.tclist[ i ];
                x2 = double.Parse( tccur.t_value.ToString() );
                sumOfdenomi += Math.Pow( x2 - x1, 3 );
                x1 = x2;


            }
            double acof = -6.0 / sumOfdenomi;//二次项系数
            double ss = -1;
            fx2 fx2tmp;
            double flow = 0;
            //double fhig = 0;
            x1 = 0;
            for( int i = 0; i < noOfTcs; i++ )
            {
                tccur = ( TCNode ) tcl.tclist[ i ];

                x2 = double.Parse( tccur.t_value.ToString() );
                fx2tmp = new fx2( x1, x2 );
                fx2tmp.a = acof;
                fx2tmp.s = -acof / 6.0 * Math.Pow( x2 - x1, 3 );
                insOfFx.fxs.Add( fx2tmp );

                tccur.f_low = flow;
                tccur.f_high = flow + fx2tmp.s;
                flow = tccur.f_high;
                x1 = x2;


            }
            tccur = ( TCNode ) tcl.tclist[ noOfTcs - 1 ];
            tccur.f_high = 1;

            if( enExcel )
            {
                //double lrv = 0, rrv = 1;
                //for (int i = 0; i < insOfFx.fxs.Count; i++)
                //{
                //    fx2tmp = (fx2)insOfFx.fxs[i];

                //    lrv = fx2tmp.x1;
                //    rrv = fx2tmp.x2;

                //    sheet.Cells[i + 2, noOfTcs - 1] = "(" + lrv.ToString() + "," + rrv.ToString() + ");" + fx2tmp.a.ToString() + "*(x-(" + fx2tmp.x1.ToString() + "))*(x-(" + fx2tmp.x2.ToString() + "))";


                //}
                //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

                //excel.Workbooks.Close();
                // excel.Quit();

            }


            return true;


        }
        public bool computeFxCos( SingleTClist tcl, Fx insOfFx, int insPos )
        {
            int noOfTcs = tcl.tclist.Count;//最后一个是1，不是tc
            bool enExcel = true;
            string excelname = "pdfs.xls";

            Excel.Workbooks workbooks;
            object miss = Missing.Value;
            Excel.Sheets sheets;
            Excel._Worksheet sheet = null;
            if( noOfTcs > 120 )
                enExcel = false;
            if( enExcel )
            {

                //workbooks = excel.Workbooks;
                //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
                //sheets = excel.Worksheets;
                //sheet = (Excel._Worksheet)sheets.get_Item(1);
                //excel.DisplayAlerts = false;
                //sheet.Cells[1, noOfTcs - 1] = noOfTcs;

            }

            if( insOfFx.fxs == null )
            {
                insOfFx.fxs = new ArrayList();
                insOfFx.fxs.Add( new fcos( 0.0, 1.0 ) );
            }
            else
            {
                // insOfFx.fxs.Clear();

            }

            fcos fcostmp;
            TCNode tccur;
            TCNode tcpre;
            TCNode insNew;

            if( noOfTcs == 0 )
                return false;
            /*1. set the area of each pdf*/
            //f0
            if( insPos != 0 )
            {
                tccur = ( TCNode ) tcl.tclist[ 0 ];
                fcostmp = new fcos( 0.0, ( double ) tccur.t_value );
                fcostmp.s = ( double ) tccur.t_value;
                //insOfFx.fxs.Add(fcostmp);
                insOfFx.fxs[ 0 ] = fcostmp;
            }
            //fi(1<=i<=n-1)
            insNew = ( TCNode ) tcl.tclist[ insPos ];

            if( insPos == 0 )
            {
                fcostmp = new fcos( 0.0, ( double ) insNew.t_value );
                fcostmp.s = ( double ) insNew.t_value;

            }
            else
            {
                tcpre = ( TCNode ) tcl.tclist[ insPos - 1 ];
                fcostmp = new fcos( double.Parse( tcpre.t_value.ToString() ), double.Parse( insNew.t_value.ToString() ) );
                fcostmp.s = double.Parse( insNew.t_value.ToString() ) - double.Parse( tcpre.t_value.ToString() );
            }
            insOfFx.fxs.Insert( insPos, fcostmp );
            if( insPos < noOfTcs - 2 )
            {
                tccur = ( TCNode ) tcl.tclist[ insPos + 1 ];
                tcpre = ( TCNode ) tcl.tclist[ insPos ];
                fcostmp = new fcos( double.Parse( tcpre.t_value.ToString() ), double.Parse( tccur.t_value.ToString() ) );
                fcostmp.s = double.Parse( tccur.t_value.ToString() ) - double.Parse( tcpre.t_value.ToString() );
                insOfFx.fxs[ insPos + 1 ] = fcostmp;
            }

            //for (int i = 1; i < noOfTcs - 1; i++)
            //{
            //    tccur = (TCNode)tcl.tclist[i];
            //    tcpre = (TCNode)tcl.tclist[i - 1];
            //    fcostmp = new fcos(double.Parse(tcpre.t_value.ToString()),double.Parse(tccur.t_value.ToString()));
            //    fcostmp.s =double.Parse(tccur.t_value.ToString())-double.Parse(tcpre.t_value.ToString());
            //    insOfFx.fxs.Add(fcostmp);

            //}
            //fn
            tccur = ( TCNode ) tcl.tclist[ noOfTcs - 2 ];
            fcostmp = new fcos( double.Parse( tccur.t_value.ToString() ), 1.0 );
            fcostmp.s = 1 - double.Parse( tccur.t_value.ToString() );
            // insOfFx.fxs.Add(fcostmp);
            insOfFx.fxs[ noOfTcs - 1 ] = fcostmp;



            /* 2.compute the parameters of each pdf*/
            Random randOfpdf = new Random();
            int noOffx = insOfFx.fxs.Count;
            //f0
            fcostmp = ( fcos ) insOfFx.fxs[ 0 ];
            double x1 = fcostmp.rr;
            double a0 = Math.PI / x1 * randOfpdf.NextDouble();
            double b0 = Math.PI / 2.0 - a0 * x1;
            fcostmp.acof = a0;
            fcostmp.bcof = b0;
            fcostmp.A = a0 * ( fcostmp.s ) / ( Math.Sin( a0 * ( fcostmp.rr ) + b0 ) - Math.Sin( b0 ) );
            //fi(1<=i<=n-1)

            if( insPos != 0 )
            {
                fcostmp = ( fcos ) insOfFx.fxs[ insPos ];
                fcostmp.acof = Math.PI / ( fcostmp.rr - fcostmp.lr );
                fcostmp.bcof = Math.PI / 2.0 - Math.PI / ( fcostmp.rr - fcostmp.lr ) * ( fcostmp.lr );
                fcostmp.A = ( fcostmp.acof ) * ( fcostmp.s ) / ( Math.Sin( ( fcostmp.acof ) * ( fcostmp.rr ) + fcostmp.bcof ) - Math.Sin( ( fcostmp.acof ) * ( fcostmp.lr ) + fcostmp.bcof ) );
            }
            if( insPos < noOfTcs - 2 )
            {
                fcostmp = ( fcos ) insOfFx.fxs[ insPos + 1 ];
                fcostmp.acof = Math.PI / ( fcostmp.rr - fcostmp.lr );
                fcostmp.bcof = Math.PI / 2.0 - Math.PI / ( fcostmp.rr - fcostmp.lr ) * ( fcostmp.lr );
                fcostmp.A = ( fcostmp.acof ) * ( fcostmp.s ) / ( Math.Sin( ( fcostmp.acof ) * ( fcostmp.rr ) + fcostmp.bcof ) - Math.Sin( ( fcostmp.acof ) * ( fcostmp.lr ) + fcostmp.bcof ) );

            }
            //for (int i = 1; i < noOffx - 1; i++)
            //{
            //    fcostmp = (fcos)insOfFx.fxs[i];
            //    fcostmp.acof=Math.PI/(fcostmp.rr-fcostmp.lr);
            //    fcostmp.bcof=Math.PI/2.0-Math.PI/(fcostmp.rr-fcostmp.lr)*(fcostmp.lr);
            //    fcostmp.A = (fcostmp.acof) * (fcostmp.s) / (Math.Sin((fcostmp.acof) * (fcostmp.rr) + fcostmp.bcof) - Math.Sin((fcostmp.acof) * (fcostmp.lr) + fcostmp.bcof));
            //}
            //fn
            fcostmp = ( fcos ) insOfFx.fxs[ noOffx - 1 ];
            double xn = fcostmp.lr;
            double an = Math.PI / ( 1 - xn ) * randOfpdf.NextDouble();
            double bn = 1.5 * Math.PI - an * xn;
            fcostmp.acof = an;
            fcostmp.bcof = bn;
            fcostmp.A = ( fcostmp.acof ) * ( fcostmp.s ) / ( Math.Sin( ( fcostmp.acof ) * ( fcostmp.rr ) + fcostmp.bcof ) - Math.Sin( ( fcostmp.acof ) * ( fcostmp.lr ) + fcostmp.bcof ) );
            if( enExcel )
            {
                //for (int i = 0; i < insOfFx.fxs.Count; i++)
                //{
                //    fcostmp = (fcos)insOfFx.fxs[i];
                //    sheet.Cells[i+2, noOfTcs - 1] = "(" + fcostmp.lr.ToString() + "," + fcostmp.rr.ToString() + ");" + fcostmp.A.ToString() + "*cos(" + fcostmp.acof.ToString() + "*x+(" + fcostmp.bcof.ToString() + "))";


                //}
                //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);

                //excel.Workbooks.Close();
                // excel.Quit();

            }
            /*3.compute F(low),F(high) */
            double tc_val;
            TCNode tc_temp;
            //f0
            if( insPos == 0 )
            {
                tc_temp = ( TCNode ) tcl.tclist[ 0 ];
                tc_val = ( double ) tc_temp.t_value;
                fcostmp = ( fcos ) insOfFx.fxs[ 0 ];
                tc_temp.f_low = 0;
                tc_temp.f_high = fcostmp.s;
                insPos = insPos + 1;
            }
            else
            {
                tc_temp = ( TCNode ) tcl.tclist[ insPos - 1 ];

            }


            double prehigh = tc_temp.f_high;
            //fi
            for( int i = insPos; i < noOfTcs - 1; i++ )
            {

                tc_temp = ( TCNode ) tcl.tclist[ i ];
                tc_val = ( double ) tc_temp.t_value;
                fcostmp = ( fcos ) insOfFx.fxs[ i ];
                tc_temp.f_low = prehigh;
                tc_temp.f_high = prehigh + fcostmp.s;
                prehigh = tc_temp.f_high;

            }

            //fn
            tc_temp = ( TCNode ) tcl.tclist[ noOfTcs - 1 ];
            tc_temp.f_low = prehigh;
            tc_temp.f_high = 1;
            return true;


        }
        /// <summary>
        /// 计算新的一组fn(x)
        /// </summary>
        /// <param name="tcl"></param>
        /// <returns></returns>
        public bool computeFxX2( SingleTClist tcl )
        {
            if( instanceOfFx.fxs == null )
            {
                instanceOfFx.fxs = new ArrayList();
            }
            else
            {
                instanceOfFx.fxs.Clear();

            }
            int noOfTcs = tcl.tclist.Count;//最后一个是1，不是tc
            fx2 fx2tmp;
            TCNode tccur;
            TCNode tcpre;

            if( noOfTcs == 0 )
                return false;
            /*1. set the area of each pdf*/
            //f0
            tccur = ( TCNode ) tcl.tclist[ 0 ];
            fx2tmp = new fx2( 0, ( double ) tccur.t_value );
            fx2tmp.s = fx2tmp.x2 - fx2tmp.x1;
            instanceOfFx.fxs.Add( fx2tmp );
            //fi(1<=i<=n-1)
            for( int i = 1; i < noOfTcs - 1; i++ )
            {
                tccur = ( TCNode ) tcl.tclist[ i ];
                tcpre = ( TCNode ) tcl.tclist[ i - 1 ];
                fx2tmp = new fx2( ( double ) tcpre.t_value, ( double ) tccur.t_value );
                fx2tmp.s = fx2tmp.x2 - fx2tmp.x1;
                instanceOfFx.fxs.Add( fx2tmp );

            }
            //fn
            tccur = ( TCNode ) tcl.tclist[ noOfTcs - 2 ];
            fx2tmp = new fx2( ( double ) tccur.t_value, 1 );
            fx2tmp.s = fx2tmp.x2 - fx2tmp.x1;
            instanceOfFx.fxs.Add( fx2tmp );

            /* 2.compute the parameters of each pdf*/
            Random randOfpdf = new Random();
            int noOffx = instanceOfFx.fxs.Count;
            //f0
            fx2tmp = ( fx2 ) instanceOfFx.fxs[ 0 ];
            double x1 = fx2tmp.x2;
            double r0 = -x1 * randOfpdf.NextDouble();
            double a0 = ( fx2tmp.s ) * 6 / ( 3 * r0 * Math.Pow( x1, 2.0 ) - Math.Pow( x1, 3.0 ) );
            fx2tmp.a = a0;
            fx2tmp.x1 = r0;
            //fi(1<=i<=n-1)
            for( int i = 1; i < noOffx - 1; i++ )
            {
                fx2tmp = ( fx2 ) instanceOfFx.fxs[ i ];
                fx2tmp.a = -6 * ( fx2tmp.s ) / Math.Pow( fx2tmp.s, 3.0 );
            }
            //fn
            fx2tmp = ( fx2 ) instanceOfFx.fxs[ noOffx - 1 ];
            double xn = fx2tmp.x1;
            double rn = ( 1 - xn ) * randOfpdf.NextDouble() + 1;
            double an = 6 * ( fx2tmp.s ) / ( Math.Pow( 1 - xn, 3.0 ) - 3 * rn * Math.Pow( 1 - xn, 2 ) + 3 * xn * Math.Pow( 1 - xn, 2 ) );
            fx2tmp.x2 = rn;
            fx2tmp.a = an;

            /*3.compute F(low),F(high) */
            double tc_val;
            TCNode tc_temp;
            //f0
            tc_temp = ( TCNode ) tcl.tclist[ 0 ];
            tc_val = ( double ) tc_temp.t_value;
            fx2tmp = ( fx2 ) instanceOfFx.fxs[ 0 ];
            tc_temp.f_low = 0;
            tc_temp.f_high = ( fx2tmp.a ) * Math.Pow( fx2tmp.x2, 2.0 ) * ( fx2tmp.x2 / 3 - ( fx2tmp.x2 - fx2tmp.x1 ) / 2 );


            double prehigh = tc_temp.f_high;
            //fi
            for( int i = 1; i < noOfTcs - 1; i++ )
            {

                tc_temp = ( TCNode ) tcl.tclist[ i ];
                tc_val = ( double ) tc_temp.t_value;
                fx2tmp = ( fx2 ) instanceOfFx.fxs[ i ];
                tc_temp.f_low = prehigh;
                tc_temp.f_high = prehigh - ( fx2tmp.a ) * Math.Pow( fx2tmp.x2 - fx2tmp.x1, 3.0 ) / 6;
                prehigh = tc_temp.f_high;

            }

            //fn
            tc_temp = ( TCNode ) tcl.tclist[ noOfTcs - 1 ];
            tc_temp.f_low = prehigh;
            tc_temp.f_high = 1;
            return true;



        }
        public TCNode genNext( SingleTClist tcs, double ex_b, double shape_a, double pdfU, uint fcount )
        {

            double tu = mtrand.e0e1genrand();//产生不包括0,1的随机数
            double tval = tu;
            double tu_after;
            TCNode tc;
            if( fcount == 0 )
            {
                //tc = new TCNode(tval);
            }
            else//按fn分布产生下一个test case
            {
                //int noOfTc = tcs.tclist.Count;
                double tc_val;
                TCNode tc_temp;
                int i;
                for( i = 0; i < fcount + 2; i++ )
                {
                    tc_temp = ( TCNode ) tcs.tclist[ i ];
                    tc_val = double.Parse( tc_temp.t_value.ToString() );
                    if( tu > tc_temp.f_high )
                        continue;
                    else if( tu >= tc_temp.f_low )
                    {
                        tval = Math.Pow( ( tu - pdfU * ( tc_val - ( 2 * i - 1 ) * ex_b ) - 2 * shape_a * Math.Pow( ex_b, 3 ) * ( i - 1 ) / 3 ) * 3 / shape_a - Math.Pow( ex_b, 3 ), 1.0 / 3.0 ) + tc_val;
                        break;

                    }
                    else
                    {
                        tval = ( tu - 2 * shape_a * Math.Pow( ex_b, 3 ) * ( i - 1 ) / 3 ) / pdfU + 2 * ex_b * ( i - 1 );//存在误差，可能使tval>=1
                        tu_after = 2 * shape_a * Math.Pow( ex_b, 3 ) * ( i - 1 ) / 3 + pdfU * ( tval - 2 * ex_b * ( i - 1 ) );
                        break;
                    }


                }
                //if (i == fcount+2)
                //    tval = (tu - 2 * shape_a * Math.Pow(ex_b, 3) * fcount / 3) / pdfU + 2 * ex_b * fcount;
                //tc = new TCNode(tval);


            }
            tc = new TCNode( tval );
            //tclist.Add(tval);
            return tc;





        }
        public void insertTCL( SingleTClist tcs, TCNode tc, ref double ex_b, ref double shape_a, ref double min_d )
        {

            int noOfTc = tcs.tclist.Count;
            int insertPos = noOfTc;
            TCNode tmp_tc = null;
            for( int i = 0; i < noOfTc; i++ )
            {
                tmp_tc = ( TCNode ) tcs.tclist[ i ];
                if( double.Parse( tc.t_value.ToString() ) < double.Parse( tmp_tc.t_value.ToString() ) )
                {

                    insertPos = i;
                    break;
                }
            }
            tcs.tclist.Insert( insertPos, tc );
            tmp_tc = ( TCNode ) tcs.tclist[ insertPos - 1 ];
            double before_delta = double.Parse( tc.t_value.ToString() ) - double.Parse( tmp_tc.t_value.ToString() );
            tmp_tc = ( TCNode ) tcs.tclist[ insertPos + 1 ];
            double after_delta = double.Parse( tmp_tc.t_value.ToString() ) - double.Parse( tc.t_value.ToString() );
            if( min_d > before_delta )
                min_d = before_delta;
            if( min_d > after_delta )
                min_d = after_delta;
            ex_b = min_d > 1 / ( 2 * noOfTc + 2.0 ) ? 1 / ( 4 * noOfTc + 4.0 ) : min_d / 2;



        }
        public void computeFx( SingleTClist tcs, double pdfU, double ex_b, double shape_a )
        {
            int noOfTc = tcs.tclist.Count;
            double tc_val;
            TCNode tc_temp;
            for( int i = 1; i < noOfTc - 1; i++ )
            {
                tc_temp = ( TCNode ) tcs.tclist[ i ];
                tc_val = ( double ) tc_temp.t_value;
                tc_temp.f_low = pdfU * ( tc_val - ex_b - 2 * ex_b * ( i - 1 ) ) + 2 * shape_a * Math.Pow( ex_b, 3 ) * ( i - 1 ) / 3;
                tc_temp.f_high = pdfU * ( tc_val - ex_b - 2 * ex_b * ( i - 1 ) ) + 2 * shape_a * Math.Pow( ex_b, 3 ) * ( i - 1 ) / 3 + shape_a * Math.Pow( ex_b, 3 ) / 3;


            }

        }
        /// <summary>
        /// true表示fail
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="vals"></param>
        /// <param name="faults"></param>
        /// <returns></returns>
        public bool executeProg( uint dim, double[] vals, SingleFault[] faults )
        {
            SingleFault fault;
            bool fail = false;
            double fr_sval;
            int flag;
            for( int i = 0; i < faults.Length; i++ )
            {
                fault = ( SingleFault ) faults[ i ];

                flag = 0;
                for( int j = 0; j < dim; j++ )
                {
                    fr_sval = double.Parse( ( fault.init_posOfNDs[ j ] ).ToString() );
                    if( vals[ j ] >= fr_sval && vals[ j ] <= ( fr_sval + ( double ) ( fault.fr_dimensions[ j ] ) ) )
                    {
                        flag++;

                    }
                }
                if( flag == dim )
                {
                    fail = true;

                    break;
                }

            }
            return fail;
        }

        /// <summary>
        /// 确定faults的位置
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="faults"></param>
        public void initPos( uint dim, SingleFault[] faults )
        {
            SingleFault fault = null;
            double fr_dim;
            for( int i = 0; i < faults.Length; i++ )
            {
                fault = ( SingleFault ) faults[ i ];
                if( fault.init_posOfNDs == null )
                {
                    fault.init_posOfNDs = new ArrayList( Int32.Parse( dim.ToString() ) );
                }
                else
                {
                    fault.init_posOfNDs.Clear();
                }
                for( int j = 0; j < dim; j++ )
                {
                    fr_dim = ( double ) ( fault.fr_dimensions )[ j ];
                    //(fault.init_posOfNDs)[j] = (1.0 - fr_dim) * mtrand.e0e1genrand();
                    fault.init_posOfNDs.Add( ( 1.0 - fr_dim ) * mtrand.e0e1genrand() );

                }

            }

        }


    }
    public class TCNode
    {
        public object t_value;
        public double f_low;
        public double f_high;
        public double fx_val;
        public int fx_index;//位于第几个区间
        public TCNode( object val )
        {
            t_value = val;
            f_low = -1;
            f_high = -1;
        }
        public TCNode( object val, double fl, double fh )
        {
            t_value = val;
            f_low = fl;
            f_high = fh;
        }
    }
    public class SingleTClist
    {
        uint dimension;
        public ArrayList tclist;
        public SingleTClist( uint dim )
        {
            dimension = dim;
            tclist = new ArrayList();

        }

    }
    public class fx2//二次概率密度函数=a(x-x1)(x-x2)
    {
        public double a;//二次项系数
        public double x1;//第一个根
        public double x2;//第二个根
        public double s;//该fx所对应的积分值
        public fx2( double ax1, double ax2 )
        {

            x1 = ax1;
            x2 = ax2;
        }
    }
    public class fx1
    {
    }
    public class fcos//余弦pdf=Acos(ax+b)
    {
        public double A;//振幅
        public double acof;
        public double bcof;
        public double lr;//定义域(lr,rr)
        public double rr;
        public double s;//该fx所对应的积分值
        public fcos( double llr, double rrr )
        {
            lr = llr;
            rr = rrr;

        }
    }
    public class Fx//概率分布函数
    {
        string fexp;
        public ArrayList fxs;
        public double sumsOfintervals = 0;
        public Fx( string expname )
        {
            fexp = expname;

        }

    }
    public class SingleFault
    {
        public uint dim;
        public double failr;
        public ArrayList init_posOfNDs;
        public ArrayList fr_dimensions;
        public double coordinate_pros = 1;//默认为正方形
        public SingleFault( uint ddim, double fr_arg )
        {
            dim = ddim;
            failr = fr_arg;
            //init_posOfNDs = new ArrayList();
            fr_dimensions = new ArrayList( Int32.Parse( dim.ToString() ) );//各维的fr
            for( int i = 0; i < dim; i++ )
            {
                if( coordinate_pros == 1 )
                {

                    fr_dimensions.Add( Math.Pow( failr, 1.0 / ( double ) dim ) );
                }
                else
                {

                }
            }
        }

    }
}
