﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;
using System.IO;

namespace Bi_Testing_OneDimension
{
    class Program
    {
        double fail_start;
        double fail_rate=0.0001;
        int randomseed;
        ArrayList al = new ArrayList(); 
        public Program(int seed) 
        {
            randomseed = seed;
        }
        public int run()
        {
            Random random = new Random(randomseed);
            int count = 0;
            fail_start = random.NextDouble()*(1-fail_rate);
            Console.Out.WriteLine("fail:(" + fail_start + "," + (fail_start + fail_rate) + ")");
            double p = 0.5;
          //  Console.Out.WriteLine("p:"+p);
            int i=1,m=0;
            while (test(p)) 
            {
                Console.Out.WriteLine("p"+count+":" + p);
                count++;
                m =(int)(count+1 - Math.Pow(2, i));
                p = (Math.Pow(2, -(i + 1))) * (2 * m + 1);
                if (2 * m + 1 == (Math.Pow(2, i + 1)) - 1)
                {
                    i++;
                }
                
            }
            Console.Out.WriteLine("p" + count + ":" + p);
            return count;
        }
        public bool test(double p)
        {
            bool flag;
            if (p > fail_start && p < (fail_start + fail_rate))
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
            return flag;
        }
        static void Main(string[] args)
        {
            //////////////////////////////////
            //Excel.Application excel;
            //excel = new Excel.Application();
            //Excel.Workbooks workbooks;
            //object miss = Missing.Value;
            //Excel.Sheets sheets;
            //Excel._Worksheet sheet = null;
            //string excelname = "result.xls";
            //workbooks = excel.Workbooks;
            //workbooks.Open(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss, miss);
            //sheets = excel.Worksheets;
            //sheet = (Excel._Worksheet)sheets.get_Item(1);
            //excel.DisplayAlerts = false;
            //////////////////////////////////
            int cishu = 2000;
            long sumOfF = 0;
            MyTimer timer = new MyTimer();
            timer.Start();
            for (int i = 0; i < cishu; i++)
            {
                Program program = new Program((i + 3) * 17);
                int f_measure = program.run();
                sumOfF += f_measure;
            }
            timer.Stop();
            ////////////////////////////////////////////////
            //sheet.Cells[2, 1] = sumOfF / cishu;
            //sheet.SaveAs(Directory.GetCurrentDirectory() + @"\" + excelname, miss, miss, miss, miss, miss, miss, miss, miss, miss);
            //excel.Workbooks.Close();
            //excel.Quit(); 
            ////////////////////////////////////////////////
            Console.Out.WriteLine("F-measure平均值：" + ((double)sumOfF / (double)cishu) + " time:" + timer.Duration / cishu + " s");
            Console.ReadLine();
        }
    }
}
